
- `locate` vs `find`: locate searches based on an index  which is updated periodically by system and is usually faster than find (but uses index).
- `passwd`: changing password
- `chsh`: change the login shell.
- `clobbering`: means than `> file` will first clobber (delete) existing file before redirecting. In bash, `set -C` sets clobbering to off.
- **Segmentation fault, Bus error**  
    Segmentation falut: The program tried to access a part of memory that it was not allowed to touch, and the operating system killed it.  
     a bus error means that the program tried to access some memory in a particular way that it shouldn’t.
- Change file mode (modifying permissions)

    ```zsh
    chmod g+r file # g: group
    chmod o+r file # o: others
    chmod go+r file
    # AN ABSOLUTE CHANGE
    chmod 644 file
    ```

- create symbolic links: `ln -s target linkname`

- using `tar`:

    ```zsh
    # create moode (-c), verbose
    tar cvf archive.tar file1 file2 file3
    # extract mode (-x)
    tar xvf archive.tar
    # p for saving original permissions (defalut in sudo mode)
    tar xvpf archive.tar # doesnot remove .tar suffix
    # table of content
    tar tvf archive.tar
    ```

-  Compressed Archives (.tar.gz)  
     `.tgz` is saame as `tar.gz` and is used for FAT file system in windows.

    ```
    c – create a archive file.
    x – extract a archive file.
    v – show the progress of archive file.
    f – filename of archive file.
    t – viewing content of archive file.
    j – filter archive through bzip2.
    z – filter archive through gzip.
    r – append or update files or directories to existing archive file.
    W – Verify a archive file.
    wildcards – Specify patterns in unix tar command.
    ```

    examples:

    ```zsh
    # way  1
    gunzip file.tar.gz
    tar xvf file.tar
    # way 2
    zcat file.tar.gz | tar -xvf -
    # zcat is equivalent to gunzip -dc
    # way 3: z first invokes gunzip
     tar -tzvf file.tar.gz
    # a tar archive containing a big file and
    # don't want to store the file on disk before processing it
    tar -xOzf foo.tgz bigfile | process
    # if you want to process the concatenation of the files
    tar -xOzf foo.tgz bigfile1 bigfile2 | process
    # extract group of files using wildcard
    tar -xvf Phpfiles-org.tar --wildcards '*.php'
    # create archive and sent it to stdout (pipe)
    tar cf - `ls *.bed` | gzip - archive.tar.gz
    tar cf - | tar -C folder -xvf -
    # create compressed archives
    tar czf archive.tar.gz file1 file2
    # perform comand on regular files of an archive
    # command is executed once for each regular file extracted
    tar -xf archive.tar \
       --to-command='proc $TAR_FILENAME $TAR_SIZE'
    # calculate md5sum for all files within an archive
    tar xf some.tar \
        --to-command='md5sum'
    # to print also name of files; head -c 34: prints first 34 bytes (the md5sum values)
    tar xf some.tar \
        --to-command="sh -c $(printf '%q' 'md5sum | head -c 34 && printf "%s\n" "$TAR_FILENAME"')"

    # create bz2-compressed archive (<tar.tb2> | <tar.tbz> | <tar.bz2>) 
    tar cvjf archive.tar.bz2 file1 file2
    # vefiy a tar archive (not applicable to gz & bz2)
    tar tvfW tecmint-14-09-12.tar
    ```

- Other compression programs with similar syntax to `gzip`;
  - `bzip2` & `bunzip2`: you can use tar with `j` option instead.
  - `xz` & `unxz`
  - `zip` & `unzip` for compatibility to windows zip files.
  - `compress`: an old unix proggram that creates `.Z` files. Files can be decommpressed using `gunzip`

## FHS

- `/sbin`: system management utilities. Not in the path of regular users.
- `/var`: program's runtime information, System logging, user tracking, caches, and other files that system programs create and manage.
- `/var/tmp`: system doesn’t wipe it on boot (unlike `/tmp`).
- `/lib`: may only contain shared libraries but `/usr/lib` may contain shared or static libraries.
- `/usr`:  /usr is where most of the user-space programs and data reside.
- `/usr/include`: /include Holds header files used by the C compiler.
- `/usr/info` Contains GNU info manuals.
- `/usr/local` Is where administrators can install their own software. Its structure should look like that of / and /usr.
- `/usr/share`: **read-only** Architecture-independent data to be shred between diffrent architecture of a same OS (rarely used for this purpose). Any program or package which contains or requires data that doesn’t need to be modified should store that data in `/usr/share` (or `/usr/local/share`, if installed locally).

- Kernel Location: `/vmlinuz` or `/boot/vmlinuz`
- `/lib/modules`: loadable kernel modules

- `vipw` to edit `/etc/passwd`
- `visudo` to edit `/etc/sudoers`

