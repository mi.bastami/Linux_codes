## grub2

- You can access grub2 config during boot in grub menu using **c**

- the grub has its own kernel and root directory and can access and read devices.

- only the "root" as a kernel parameter defines the root filesystem `/` (in line `linux boot_image= .... root=UUID=`). Other "root" in other parts of config file indicate grub's root.

```grub
echo $root
# hd0,gpt2
# hd0,msdos1 (indicating mbr)
ls # list all devices known to grub
ls -l # detailed output
ls ($root)/ # list inside root
ls ($root)/boot
set # see a list of variables
```

## grub2 config

Grub2 config file are:

- `/boot/grub/grub.cfg`: kernel configurations. Not modified manually. Is based on templates in `/etc/grub.d` and seetings in `/etc/default/grub` and is made with `grub-mkconfig`. See `info -f grub -n 'Simple configuration'` for ducumentation of `/etc/defaults/grub` which controls the operation of
'grub-mkconfig' through a series of key=value pairs.

- numerous loadable modules `.mod` in directories like `/boot/grub/x86_64-efi/*.mod`

To add a menuentry  or change configuration:

- `grub-mkconfig` is a script that makes config file based on seperate script files in `/etc/grub.d`.
- To add a menuentry: add a new custom file `/boot/grub/custom.cfg`
- The `/etc/grub.d` configuration directory gives you two options: `40_custom` and `41_custom`
- `40_custom`, is a script that you can edit yourself, but it’s probably the **least stable**; a package upgrade is likely to destroy any changes you make.
- The `41_custom script` is simpler; it’s just a series of commands that load `custom.cfg` when GRUB starts.

## installing grub

- construct a version of GRUB capable of reading that filesystem so that
it can load the rest of its configuration (`grub.cfg`) and any required modules.
- On Linux, this usually means building a version of GRUB with its `ext2.mod`
module preloaded.
- `grub-install` do it automamtically

## Installing GRUB on an External Storage Device (MBR)

- /mnt/boot is the mount point of partition for `/boot/grub`.

```zsh
grub-install --boot-directory=/mnt/boot /dev/sdc
```

## Installing GRUB with UEFI

- copy the boot loader into place. 
- “announce” the boot
loader to the firmware with the `efibootmgr` command. 
- The grub-install com-
mand runs this if it’s available
- if you’re installing to a disk that will eventually
end up in another system, you have to figure out how to announce that boot
loader to the new system’s firmware. 

```zsh
grub-install --efi-directory=efi_dir –-bootloader-id=name
```
