## systemd: unit types and units

- One of the most interesting things about systemd is that it does not just
operate processes and services; it can also mount filesystems, monitor network sockets, run timers, and more.

- Each type of capability is called a
**unit type**, and each specific capability is called a **unit**. When you turn on a
unit, you **activate** it.

A number of unit types:

- Service units: Control the traditional service daemons on a Unix

- Mount units: Control the attachment of filesystems to the system.

- Target units: Control other units, usually by grouping them (e.g `default.target` which is default boot goal)

- resource units: like device, socket, path (to be tied to service units)
  
Unix boot-time tasks are fairly **fault tolerant** and can often fail without
causing serious problems for standard services.  
  
### Systemd dependency types and styles

- **Requires** Strict dependencies. When activating a unit with a Requires
dependency unit, systemd attempts to activate the dependency unit. If
the dependency unit fails, systemd deactivates the dependent unit.

- **Wants** Dependencies for activation only. Upon activating a unit, systemd
activates the unit’s Wants dependencies, but it doesn’t care if those
dependencies fail. The Wants dependency type is especially significant because it does not propagate fail-
ures to other units. The systemd documentation states that this is the way you should
specify dependencies if possible
- **Requisite** Units that must already be active. Before activating a unit
with a Requisite dependency, systemd first checks the status of the
dependency. If the dependency has not been activated, systemd fails
on activation of the unit with the dependency.
- **Conflicts** Negative dependencies. When activating a unit with a
Conflict dependency, systemd automatically deactivates the dependency
if it is active. Simultaneous activation of two conflicting units fails.
  
By **default**, activating a unit with a Requires or Wants causes systemd
to activate all of these dependencies **at the same time** as the first unit.

To activate units in a particular order, you can use the following **dependency modifiers**:

- **Before** The current unit will activate before the listed unit(s). For
example, if Before=bar.target appears in foo.target, systemd activates
foo.target before bar.target.
- **After** The current unit activates after the listed unit(s).
  
### Conditional Dependencies

If a conditional dependency in a unit is false when systemd tries to activate the unit, the unit does not activate, though this applies only to the unit
in which it appears.

- ConditionPathExists=p: True if the (file) path p exists in the system.
- ConditionPathIsDirectory=p: True if p is a directory.
- ConditionFileNotEmpty=p: True if p is a file and it’s not zero-length.

## systemd configurations

In two main directories:

- system unit directory (globally configured): `/usr/lib/systemd/system`: maintained by distro

- system configuration directory (locally configured): `/etc/systemd/system`

```zsh
# check the current systemd configuration search path (including precedence)
systemctl -p UnitPath show
# UnitPath=/etc/systemd/system.control /run/systemd/system.control /run/systemd/transient /etc/systemd/system /run/systemd/system /run/systemd/generator /lib/systemd/# system /run/systemd/generator.late
# or
pkg-config systemd –-variable=systemdsystemunitdir
pkg-config systemd --variable=systemdsystemconfdir
# /etc/systemd/system
```

## enabling a unit and [install] section

**Enabling** a unit is different from **activating** a unit.

- When you enable a unit, you are installing it into systemd’s configuration, making semipermanent changes that will
survive a reboot. But you don’t always need to explicitly enable a unit. If the unit file
has an [Install] section, you must enable it with `systemctl enable`; 

- otherwise, the existence of the file is enough to enable it. When you activate a unit with `systemctl start`, you’re just turning it on in the current runtime environment. In addition, enabling a unit does not activate it.

-  When you
enable a unit, systemd reads the [Install] section; in this case, enabling
the sshd.service unit causes systemd to see the WantedBy dependency for
multi-user.target. In response, systemd creates a symbolic link to sshd.service
in the system configuration directory

- Manually adding a symbolic link  (like above) is a simple
way to add a dependency without modifying a unit file that may be overwrit-
ten in the future

### Variables and Specifiers

- The sshd.service unit file also shows use of variables—specifically, the `$OPTIONS`
and `$MAINPID`, or `$SSGD_OPTS` environment variables that are passed in by systemd.

- A **specifier** is another variable-like feature often found in unit files.
  -  `%n`: current unit name
  -  `%H`: current host name
  -  `%I`: expand to the instance

You can parameterize a single
unit file in order to spawn multiple copies of a service, such as getty processes run-
ning on tty1, tty2, and so on. To use these specifiers, add the `@` symbol to the end of
the unit name. For getty, create a unit file named `getty@.service`, which allows you
to refer to units such as `getty@tty1` and getty@tty2. Anything after the @ is called
the instance, and when processing the unit file, systemd expands the `%I` specifier to
the instance. You can see this in action with the getty@.service unit files that come
with most distributions running systemd.

## systemd operations with `systemctl`

```zsh
# see a list of active units
systemctl
systemctl list-units
# active units and full names
systemctl --full
# all units (not just active ones)
systemctl --all --full

# STATUS OF A UNIT: also contain unit's journal
systemctl status sshd.service

# to see the full journal of a unit
journalctl _SYSTEMD_UNIT=sshd.service

# tell systemd to reload the config file in one of two ways
systemctl reload unit   # Reloads just the configuration for unit.
systemctl daemon-reload  # Reloads all unit configurations.
```

Requests to *activate*, *reactivate*, and *restart* units are known as **jobs** in
systemd, and they are essentially unit state changes. You can check the cur-
rent jobs on a system with

```zsh
systemctl list-jobs
## JOB  UNIT    TYPE    STATE
## 1 graphical.target start waiting
```

The term **job** can be confusing, especially because Upstart, another init system
described in this chapter, uses the word job to (roughly) refer to what systemd calls a
unit. It’s important to remember that although a systemd job associated with a unit
will terminate, the unit itself can be active and running afterwards, especially in the
case of service units.

### removing a unit

```zsh
# deactivate unit
systemctl stop unit
# if unit has [install] section:
# disable unit to remove symbolic links
systemctl disable unit
#  Remove the unit file, if you like
```

## systemd Process Tracking and Synchronization

- systemd uses **control groups (cgroups)**, an optional Linux kernel feature that allows for finer
tracking of a process hierarchy.
- Use the `Type` option in
your service unit file to indicate its **startup behavior**.

Start up  types for service units:

- **Type=simple:** The service process doesn’t fork. doesn’t account for the fact that a service may
take some time to set up, and systemd doesn’t know when to start any depen-
dencies that absolutely require such a service to be ready. 
- **Type=forking** The service forks, and systemd expects the original ser-
vice process to terminate. Upon termination, systemd assumes that the
service is ready.
- **Type=notify** The service sends a notification specific to systemd (with
the sd_notify() function call) when it’s ready.
- **Type=dbus**
 The service registers itself on the D-bus (Desktop Bus) when
it’s ready.
- **Type=oneshot**: the
service process actually terminates completely when it’s finished. With such
a service, you will almost certainly need to add a RemainAfterExit=yes option
so that systemd will still regard the service as active even after its processes
terminate.
- **Type=idle**: This simply instructs systemd not
to start the service until there are no active jobs. The idea here is just to delay
a service start until other services have started to keep the system load down

## systemd on-demand startup

- Create a service unit (unit A)
- create a resource unit (unit R, e.g. socket, device or path)
- tie (implicitly or explicitly) resorce unit to service unit
- upon activation of resource unit, systemd monitors the resource
- When something wants to access the resource, systemd will activate the service unit.

## systemd Resource-Parallelized Startup (Boot Optimization with Auxiliary Units)

- implicitly tieing resource and service units: `echo.socket` and `echo@.service`
- explicitly tieing resourc and servie units: use socket=<> in service file.
- the service unit must support multiple instances because
of the `Accept` option in echo.socket.
- That option instructs systemd not only
to listen on the port, but also to accept incoming connections and pass
the incoming connections on to the service unit (i.e. handoff), with each connection a
separate instance
  
`echo.socket` unit file:

```zsh
[Unit]
Description=echosocket

[Socket]
ListenStream=22222
Accept=yes
```

`echo@.service` unit file:

```zsh
[Unit]
Description=echoservice

[Service]
ExecStart=-/bin/cat
StandardInput=socket
```

## systemd Auxiliary Programs

- When starting out with systemd, you may notice the exceptionally large
number of programs in `/lib/systemd`. 

- These are primarily support programs
for units.

- For example, udevd is part of systemd, and you’ll find it there as
`systemd-udevd`. Another, the `systemd-fsck` program, works as a middleman
between systemd and fsck.

- Many of these programs exist because they contain notification mecha-
nisms that the standard system utilities lack. Often, they simply run the
standard system utilities and notify systemd of the results.

- One other interesting aspect of these programs is that they are written in C, because
one goal of systemd is to reduce the number of shell scripts on a system.

## system V  init

There are **two major components** to a typical System V init installation:

- a central configuration file `/etc/inittab`

- a large set of boot scripts augmented by a symbolic link farm: `/etc/rc.d/rcN.d` or `/etc/rcN.d` wich links to `/etc/init.d/` or `/etc/rc.d/init.d`

- The rc*.d commands are usually shell scripts that start programs
in `/sbin` or `/usr/sbin`.

- Scripts name starts with either `S` for start or `K` for kill or stop mode, followed by a number in range 00-99 which determine the sequence.
  
The `inittab` fileds:

1. a unique identifier
2. runlevel numbers
3. the **action** that init should do:
   - `wait`: wait until command finish.
   - `initdefault`: the defalu runlevel
   - `respawn`:  tells init to run the command that follows and, if the command finishes executing, to run it again.
   - `ctrlaltdel`: what the system does when you press ctrl-alt-del on a virtual console.
   - `sysinit`: the first thing that init should run when starting, before entering any runlevels.
4. a command to execute (optional)

### Examples

Run `/etc/rc.d/rc 5`  (executes anything in `/etc/rc5.d`) once when entering
runlevel 5, then wait for this command to finish before doing anything

```zsh
l5:5:wait:/etc/rc.d/rc 5
```

The getty programs provide login prompts. The respawn action brings the
login prompt back after you log out.

```zsh
1:2345:respawn:/sbin/mingetty tty1
```

### Modifying/adding the Boot Sequence

- Changing the name of symbolic link in `/etc/init.d` (also preserves the original name of the file)

  ```zsh
  mv S99httpd _S99httpd
  ```

- Add a sevice:
  - add a script in init.d & symlink in rc*.d
  - specify a proper sequence for the service.
  - for nonessential services numbers in 90s (after most of the
services that came with the system) is appropriate.

### `run-parts`

- run a bunch of executable programs in a given directory, in some kind of predictable order.
- ability to run programs
based on a regular expression (for example, using the `S[0-9]{2}` expression
for running all “start” scripts in an /etc/init.d runlevel directory

### controling sys v init: `telinit`

```zsh
telinit 3 # change to runlevel 3
telinit q # wake up init to reread inittab file
telinit s # single user mode
```

### runlevels

- runlevels s or S: single user mode, doesnoot require inittab file, opens a root shell in `/dev/console` (stty)
- when entering to multi user runlevels, init runs `boot` and `bootwait` enries to allow ststem to mount filesystem before user can login. Then runllevel is running.  
- By default, `halt` and `reboot` programs call shutdown with the -r or -h options.

### shoutdown

When you want to shutdown in future, it cretes `/etc/nologin` file. When this file is present,
the system prohibits logins by anyone except the superuser.

```zsh
## halt (cut the power) imidiately
shutdown -h now
shutdown -r now
sutdown -r +10 # reboot in 10 min
```

The process of shuting down:

1. init asks every process to shut down cleanly.
2. If a process doesn’t respond after a while, init kills it, first trying a
TERM signal.
3. If the TERM signal doesn’t work, init uses the KILL signal on any
stragglers.
4. The system locks system files into place and makes other preparations
for shutdown.
4. The system unmounts all filesystems other than the root.
5. The system remounts the root filesystem read-only.
6. The system writes all buffered data out to the filesystem with the sync
program.
7. The final step is to **tell the kernel to reboot or stop** with the reboot(2)
system call. This can be done by init or an auxiliary program such as
reboot, halt, or poweroff.

## The Initial RAM Filesystem (initramfs or inird)

- the Linux kernel does not talk to the PC BIOS
or EFI interfaces to get data from disks, so in order to mount its root file-
system, it **needs driver support** for the underlying storage mechanism.

- distributions can’t include all of drivers in their kernels, so many drivers are shipped as loadable modules
- If root filesystem is notmounted at first, these files are not accessible.
- A small collection of kernel driver modules along with a few other utilities (including an init) into an cpio archive
- The boot loader loads
this archive into memory before running the kernel. 
- Upon start, the kernel
reads the contents of the archive into a temporary RAM filesystem (the
`initramfs`), mounts it at `/`, and performs the user-mode handoff to the **init
on the initramfs**.
- Then, the utilities included in the initramfs allow the ker-
nel to load the necessary driver modules for the real root filesystem. Finally,
the utilities mount the real root filesystem and start **true init.**
  
The init of initramfs:

- on some distros,  a fairly simple shell script that starts a **udevd** to load
drivers, then mounts the real root and executes the init there.

- On distribu-
tions that use systemd, you’ll typically see an entire systemd installation
there with no unit configuration files and just a few udevd configuration files.

## emrgency booting

Options:

- bootable live image of distro

- **SystemRescueCd**: a usb bootable linux with networking and a lot of utilities for recovery.

- single user mode: no GUI, no networking, limited.