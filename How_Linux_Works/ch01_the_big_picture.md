## levels of abstraction

Layers (levels):

1. Hardware

2. kernel

3. processes (user space)

The kernel is in charge of managing tasks in four general system areas:

- processes
- memory
- device rivers
- system calls (syscalls)) & supports
  - syscalls:
    - fork()
    - exec()
  - pseudodevices

![sysCalls()](./images/syscalls.png)

## Terms

- **User space** refers to the parts of main memory that the user processes can access.
- **kernel space:** The area that only the kernel can access.
- **state** of a processes in main memory: Instead of describing a state using bits, you describe what something
has done or is doing at the moment.
- **image**: a particular physical arrangement of bits.
- **context switch:** The act of one process giving up control of the CPU to another process.
- **time slice:** Each piece of CPU time­.
- **memory management unit (MMU):** in modern CPUs, enables a memory access scheme called *virtual
memory*.
- **Page table:** The implementation of a memory address map.