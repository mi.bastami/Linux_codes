## partitioning disk devices

- There is a critical difference between partitioning and filesystem manipulation. The partition table defines simple boundaries on the disk, whereas a filesystem is a much more involved data system.
- use `parted` for MBR & GPT partitioning not filesystem manipulations.
- in MBR:
  - **primary** partition: regular subdivision of disk limited to four partitions.
  - **extended** partition: can be extra partions and defines logical partion.
  - **logical**: an extended partition can be logical and used by os like any other partition.

in `dmesg` output, when kernel initialy reads an MBR partition table, it outputs the following message:  

>sda: sda1 sda2 < sda5 >  
  
This indicates that sda2 is an extended partion which contains a logical partion (sda5). Ususally we need to only work with logical partition and we can ignore the extended partition.

## modifying partition table

- `fdisk` only makes the changes as you exit the program. But with `parted`, partitions are created, modified, and removed as you issue the commands.
- All partion mpodyfying  steps are performed in user space. `fdisk` issues a system call on disk to inform kernel for partition changes. kernel outputs debug messages (`dmesg`): sdf: sdf1 sdf2
- But `parted`  signal the kernel when individual partitions are altered and no dmesg is produced.
  - in the case of `parted`, use `blockdev --rereadpt /dev/sdf` to force kernel to reread the partition.
  - or use `udevadm monitor --kernel` to monitor kernel's changes
  - Check `/proc/partitions`
  - check `/sys/block/device/`

## disk geometry

This way of thinking about the disk geometry is called **CHS**, for **cylinder-head-sector**.  
- Platters spinnig arounf spindle
- each palatter has a oving arm one or two head
- each platter has many cylenders
- each cylender has smany sectors
- A track is a part of a cylinder that a single head accesses (usually cylennder is also a track)
- on a modern hard disk, the reported values for number of cylender and sectors are fiction
- Disk hardware supports **Logical Block Addressing (LBA)** to simply address a location on the disk by a block number LBA is simple linear addressing scheme and has replced the CHS.
- cylinders has been important to partitioning because cylinders are ideal boundaries for partitions.
- One of the most significant factors affecting the performance of SSDs is **partition alignment**

```zsh
echo "`cat /sys/block/sdf/sdf2/start` / 4096" | bc
# if it was an integer it is OK
```

## file system

- **virtual file system (VFS)**: VFS abstraction layer completes the filesystem
implementation. Much as the SCSI subsystem standardizes communication
between different device types and kernel control commands, VFS ensures
that all filesystem implementations support a standard interface so that
user-space applications access files and directories in the same manner.

- **superblock**: a key com-
ponent at the top level of the filesystem database, and it’s so important
that mkfs creates a number of backups in case the original is destroyed. When creating file filesystem `mkfs` prints superblocks.

## mounting filesystem

```zsh
mount -t type device mountpoint
mount -t ext4 /dev/sdf2 /home/extra
mount UUID=a9011c2b-1c03-4288-b3fe-8ba961ab0898 /home/extra
```

## `tune2fs` tuning ext2/3/4 filesystem parameters

Parameters like uuid and etc.

## Disk Buffering, Caching, and Filesystems

- *Buffering* is for writes & *caching* is for reads.

- Buffering: The kernel buffers writes to the disk. The kernel usually doesn’t immediately write changes to filesystems when pro-
cesses request changes.

- you can force the kernel to write the changes in its buffer to the disk by running the `sync` command.

## filesystem mount options

- **system mount database**: `/etc/mtab`: also accessible by `mount` command.

```zsh
# short options:
# -r: read only; -n: do not update mtab; -t: fs type
mount -r -n -t ext4

# long options with -o
# ro: read only; conv=auto  automatically convert certain text files from the DOS
# newline format to the Unix style
mount -t vfat /dev/hda1 /dos -o ro,conv=auto

# remounting a filesystem
# remounts the root in read-write mode:
# when root is reaonly, mount can't write to mtab, so use  -n option to make root rw
mount -n -o remount /

# mount all fstab (except does with noauto option)
mount -a
```

### options that are used only in /etc/fstab file

- **defaults** This uses the mount defaults: read-write mode, enable device
files, executables, the setuid bit, and so on. Use this when you don’t
want to give the filesystem any special options but you do want to fill all
fields in /etc/fstab.
- **errors** This ext2-specific parameter sets the kernel behavior when
the system has trouble mounting a filesystem. The default is normally
`errors=continue`, meaning that the kernel should return an error code
and keep running. To have the kernel try the mount again in read-only
mode, use `errors=remount-ro`. The `errors=panic` setting tells the kernel
(and your system) to halt when there is a problem with the mount.
- **noauto** This option tells a `mount -a` command to ignore the entry. Use
this to **prevent a boot-time mount of a removable-media device**, such as
a CD-ROM or floppy drive.
- **user** This option allows unprivileged users to run mount on a particu-
lar entry, which can be handy for allowing access to CD-ROM drives.
Because users can put a setuid-root file on removable media with
another system, this option also sets **nosuid**, **noexec**, and **nodev** (to bar
special device files).

> **nodev**: The "nodev" mount option causes the system to not interpret character or block special devices. Executing character or block special devices from untrusted file systems increases the opportunity for unprivileged users to attain unauthorized administrative access.

## Alternatives to /etc/fstab

- `etc/fstab.d`
- systemd units (which also use fstab file)

## Filesystem Capacity

```zsh
# disk usage for current directory recruisively (1k block size)
du
# summary size
du -s /
# evaluate a paticular directory
du -s *
# view the size and utilization of your currently mounted filesystems (in 1k block size)
df

# The POSIX standard defines a block size of 512 bytes, set the POSIXLY_CORRECT environment variable to use it
```

## checking and repairing filesystem: `fsck`

- You should **never use `fsck` on a mounted filesystem** because the kernel may alter the
disk data as you run the check, causing runtime mismatches that can crash your
system and corrupt files. 
- There is only one exception: If you mount the **root partition read-only in single-user mode**, you may use fsck on it.
- A new generation of files­ystems
supports **journals** to make filesystem corruption far less common, you should
always shut the system down properly.

```zsh
umount <device>
# check filesystem without changing anything
# use it if sripus problem accoured, just use it before main fsck otherwise you loose data
# usefull for identifying block number of superblock backups
fsck -n <device>
# replace currupted superblock
fsck -b <block-number>
# automatically repair ordinary problems
# if the problem is serious, this may mess up the data
fsck -p <device> # or
fsck -a <device>
```

- when a ext3/ext4 fs is currupted (e.g. journal is not empty and kernell doesnot mount it), mount it as ext2 and flush the journal to a regular filesystem database

```zsh
# -f: force check; -y: say yes to all questions
e2fsck -fy <device>
```

### the worst case

- You can try to extract the entire filesystem image from the disk with `dd`
and transfer it to a partition on another disk of the same size.

```zsh
# copy a perfect image of /dev/sda to /dev/sdb
dd if=/dev/sda of=/dev/sdb
# create an .img archive of the /dev/sda drive
dd if=/dev/sda of=/home/username/sdadisk.img
# specify byte size (bytes to copy each time)
dd if=/dev/sda2 of=/home/username/partition2.img bs=4096
# restore it 
dd if=sdadisk.img of=/dev/sdb
# reate a compressed image of a remote drive using SSH and save the resulting archive to your local machine
ssh username@54.98.132.10 "dd if=/dev/sda | gzip -1 -" | dd of=backup.gz

# ecurly wipinf a disk
dd if=/dev/zero of=/dev/sda1  # wrire zeros
dd if=/dev/urandom of=/dev/sda1 # write random chars
```

- You can try to patch the filesystem as much as possible, mount it in
read-only mode, and salvage what you can.

- You can try `debugfs`.

## making a swap space

```zsh
mkswap <device>  # put swap signature
swapon <device>  # register with the kernel
# put a record in fstab
uui=<uuid> none swap sw 0 0

# use a file as swap (when you can't dedicate a full partition)
dd if=/dev/zero of=swap_file bs=1024k count=<num_mb> #<num_mb>: desired size in MB
mkswap swap_file
swapon swap_file
# remove a swap partition or file from the kernel’s active pool
swapoff <device | file>
```

## inode details

```zsh
ls -i  # shows inode number
stat
# stat() is a system call to retirive some kernell level information about
# filesystem like number of inodes and link counts
# for some fs with different internals may retrieve the same informatio but 
# it does not mean anything
```