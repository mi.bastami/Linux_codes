## `/etc` directory

- The basic guideline is
that **customizable configurations for a single machine**, such as user informa-
tion (`/etc/passwd`) and network details (`/etc/network`), go into /etc.

- However,
general application details, such as a distribution’s defaults for a user inter-
face, don’t belong in /etc. And you’ll often find that **noncustomizable** system
configuration files may be found elsewhere, as with the prepackaged systemd
unit files in `/usr/lib/systemd`.

## System Logging

- System logger: `syslog` and a newer version `rsyslog`

- `rsyslog` can load modules to write messages to databases

- `/var/log`: most log files.

### Configuration Files

- `/etc/rsyslog.conf`

- `/etc/rsyslog.d`

- The configuration format is a blend of **traditional rules** (selector & action) and **rsyslog-specific extensions** (start with `$`).

- Traditional rules:

  - selector: has two parts, seperated by dot `.`

    - Facility: subsystem that produced the message (all maail program with mail facility if they log using syslog). include `auth, authpriv, cron, daemon, kern, lpr, mail, news, syslog, user, uucp and local0 through local7.`
    - Priority: specifies the severity of the messag, includes in ascending order `debug, info, notice, warning, err, crit, alert, emerg`

- To exclude log messages from a facility in rsyslog.conf, specify **a priority of none**: `*.info;authpriv.none`

- When you put a specific priority in a selector, rsyslogd sends messages
with that priority and all higher priorities to the destination on that line.

- extended syntax:
  - The configuration extensions are called **directives** and usu-
ally begin with a `$`
- `$IncludeConfig /etc/rsyslog.d/*.conf` a directive to load more conf files.

- Additional rsyslogd configuration file extensions define output templates and chan-
nels.

### Troubleshooting

- test the system logger is to send a log message
manually with the logger command

```zsh
logger -p daemon.info something bad just happened
```

- The most common problems
occur when a configuration doesn’t catch a certain facility or priority or
when log files fill their disk partitions.

- Most distributions automatically trim
the files in `/var/log` with automatic invocations of `logrotate` or a similar util-
ity.

- `logrotate` is  designed  to  ease administration of systems that generate large numbers of log files.  It allows automatic rotation, compression, removal, and mailing of log files.  Each log file may be handled daily, weekly, monthly, or when it grows too large.

- logrotate  reads  everything  about  the log files it should be handling from the series of configuration files specified on the command line.

## User managment files

- At the kernel level, users
are simply numbers (**user IDs**), you’ll normally work with **usernames** (or **login names**)

### The `/etc/passwd` File

- The plaintext file /etc/passwd maps usernames to user IDs.

- Each line represents one user and has seven fields separated by colons.

- The /etc/passwd file syntax is fairly strict, allowing for **no comments** or
**blank lines**.

- A user in /etc/passwd and a corresponding home directory are collectively known as
**an account**.

- the password
is not actually stored in the passwd file, but rather, in the **shadow** file. The shadow file format is similar to that of passwd, but
normal users do not have read permission for shadow.

- An `x` in the second passwd file field indicates that the encrypted
password is stored in the shadow file. A star (`*`) indicates that the user
cannot log in, and if the field is blank (that is, you see two colons in a
row, like :: ), no password is required to log in
  
![passwd](images/passwd.png)

- The users that cannot log in are called **pseudo-users**. Although they can’t
log in, the system can start processes with their user IDs. 
- Pseudo-users such
as **nobody** are usually created for security reasons (nobody cannot write to anything on
the system).

### Manipulating Users and Passwords

- The `passwd` command is an suid-root program.

- By default,
passwd changes the user’s password, but you can also use `-f` to change the
user’s real name or `-s` to change the user’s shell to one listed in /etc/shells.

- You can also use the commands `chfn` and `chsh` to change the real name
and shell.
- It’s much easier (and safer) to make changes to
users using separate commands available from the terminal or through the
GUI
- `adduser` or `userdel` or `passwd user`

### groups `/etc/group`

- Groups in Unix offer a way to share files with certain users but deny access
to all others.
- fields are group name: password (not uesd):group ID: optional list of users
- the optional list of users and also users in passwd files with the same group ID belong to that group


```
root:*:0:juser
daemon:*:1:
bin:*:2:
sys:*:3:
adm:*:4:
disk:*:6:juser,beazley
nogroup:*:65534:
user:*:1000:
```

### mask & umask (file mode creation mask)

- `umask` is a command that determines the settings of a `mask` that controls how file permissions are set for newly created files.

- The mask is **a grouping of bits**, each of which restricts how its corresponding permission is set for newly created files. The bits in the mask may be changed by invoking the umask command.

- When a program creates a file the file permissions are restricted by the mask. If the mask has a bit set to "1", then the corresponding initial file permission will be disabled.

- A bit set to "0" in the mask means that the corresponding permission will be determined by the program and the file system.

- In other words, the mask acts as a last-stage filter that strips away permissions as a file is created; each bit that is set to a "1" strips away its corresponding permission. Permissions may be changed later by users and programs using chmod.
  
- Generally, the mask only affects file permissions during the creation of new files and has no effect when file permissions are changed in existing files

- The mask may be represented as **binary**, **octal** or **symbolic** notation. The umask command allows the mask to be set as octal (e.g. `0754`) or symbolic (e.g. `u=,g=w,o=wx`) notation.

```zsh
umask [-S ] [maskExpression]

umask         # display current value (as octal)
# 0022
umask -S      # display current value symbolically
# u=rwx,g=rx,o=rx
```

Setting the mask using octal notation

```zsh
# If fewer than 4 digits are entered, leading zeros are assumed.
umask 007    # set the mask to 007
umask        # display the mask (in octal)
#0007          #   0 - special permissions (setuid | setgid | sticky )
               #   0 - (u)ser/owner part of mask
               #   0 - (g)roup part of mask
               #   7 - (o)thers/not-in-group part of mask
umask -S      # display the mask symbolically
# u=rwx,g=rwx,o=
```

Octal digits of umask command and permissions that are prohibited. For chmod the same code applies the reverse: e.g. 3 in chmod would set write & execute. 

| octal | Permissions the mask will prohibit from being set during file creation |
| :---- | :--------------------------------------------------------------------- |
| 0     | any permission may be set (read, write, execute)                       |
| 1     | setting of execute permission is prohibited (read and write)           |
| 2     | setting of write permission is prohibited (read and execute)           |
| 3     | setting of write and execute permission is prohibited (read only)      |
| 4     | setting of read permission is prohibited (write and execute)           |
| 5     | setting of read and execute permission is prohibited (write only)      |
| 6     | setting of read and write permission is prohibited (execute only)      |
| 7     | all permissions are prohibited from being set (no permissions          |
  

**Setting the mask using symbolic notation**

user classes:

| u   | user   | the owner                                                                                                            |
| :-- | :----- | :------------------------------------------------------------------------------------------------------------------- |
| g   | group  | users who are members of the file's group                                                                            |
| o   | others | users who are not the owner of the file or members of the group                                                      |
| a   | all    | all three of the above, the same as ugo. (The default if no user-class-letters are specified in the maskExpression.) |

Operators:

| +   | permissions specified are enabled, permissions that are not specified are unchanged.                       |
| :-- | :--------------------------------------------------------------------------------------------------------- |
| -   | permissions specified are prohibited from being enabled, permissions that are not specified are unchanged. |
| =   | permissions specified are enabled, permissions that are not specified are prohibited from being enabled.   |

permission-symbols:

| r   | read            | read a file or list a directory's contents |
| :-- | :-------------- | :----------------------------------------- |
| w   | write           | write to a file or directory               |
| x   | execute         | execute a file or recurse a directory tree |
| X   | special execute | See Symbolic modes.                        |
| s   | setuid/gid      | See File permissions.                      |
| t   | sticky          | See File permissions.                      |

```zsh
# Prohibit write permission from being set for the user. The rest of the flags in the mask are unchanged.
umask u-w
#prohibit the write permission from being set for the user
# allow the read permission to be enabled for the group, while prohibiting write and execute permission for the group;
# allow the read permission to be enabled for others, while leaving the rest of the other flags unchanged.
umask u-w,g=r,o+r
```

| umask command issued | How the mask will affect permissions of subsequently created files/directories                                                                                                                 |
| :------------------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| umask a+r            | allows read permission to be enabled for all user classes; the rest of the mask bits are unchanged                                                                                             |
| umask a-x            | prohibits enabling execute permission for all user classes; the rest of the mask bits are unchanged                                                                                            |
| umask a+rw           | allows read or write permission to be enabled for all user classes; the rest of the mask bits are unchanged                                                                                    |
| umask +rwx           | allows read, write or execute permission to be enabled for all user classes. (Note: On some UNIX platforms, this will restore the mask to a default.)                                          |
| umask u=rw,go=       | allow read and write permission to be enabled for the owner, while prohibiting execute permission from being enabled for the owner; prohibit enabling any permissions for the group and others |
| umask u+w,go-w       | allow write permission to be enabled for the owner; prohibit write permission from being enabled for the group and others;                                                                     |
| umask -S             | display the current mask in symbolic notation                                                                                                                                                  |
| umask 777            | disallow read, write, and execute permission for all (probably not useful because even owner cannot read files created with this mask!)                                                        |
| umask 000            | allow read, write, and execute permission for all (potential security risk)                                                                                                                    |
| umask 077            | allow read, write, and execute permission for the file's owner, but prohibit read, write, and execute permission for everyone else                                                             |
| umask 113            | allow read or write permission to be enabled for the owner and the group, but not execute permission; allow read permission to be enabled for others, but not write or execute permission      |
| umask 0755           | equivalent to u-rwx,go=w. (The 0 specifies that the special modes (setuid, setgid, sticky) may be enabled.)                                                                                    |

```zsh
umask -S       # Show the (frequently initial) setting
u=rwx,g=rx,o=rx
gcc hello.c      # compile and create executable file a.out
ls -l a.out
#-rwxr-xr-x  1 me  developer  6010 Jul 10 17:10 a.out
# the umask prohibited Write permission for Group and Others
ls  > listOfMyFiles    # output file created by redirection does not attempt to set eXecute
ls -l listOfMyFiles
#-rw-r--r--  1 me  developer  6010 Jul 10 17:14 listOfMyFiles
# the umask prohibited Write permission for Group and Others
############################################################
umask u-w  # remove user write permission from umask
umask -S
u=rx,g=rx,o=rx
ls > protectedListOfFiles
ls -l protectedListOfFiles
-r--r--r--  1 me  developer  6010 Jul 10 17:15 protectedListOfFiles
rm protectedListOfFiles
#override r--r--r-- me/developer for protectedListOfFiles?
# warning that protectedListOfFiles is not writable, answering Y will remove the file
#####################################################################################
umask g-r,o-r  # removed group read and other read from mask
umask -S
u=rx,g=x,o=x
ls > secretListOfFiles
ls -l secretListOfFiles
-r--------  1 me  developer  6010 Jul 10 17:16 secretListOfFiles
```

![umask](images/umask.png)

### sticky bit

- The most common use of the sticky bit is on directories residing within filesystems
- When a directory's sticky bit is set, the filesystem treats the files in such directories in a special way so only the file's owner, the directory's owner, or root can rename or delete the file. Without the sticky bit set, any user with write and execute permissions for the directory can rename or delete contained files, regardless of the file's owner.
- Typically, this is set on the /tmp directory to prevent ordinary users from deleting or moving other users' files.

```zsh
# add sticky bit ro directory
chmod +t /usr/local/tmp
# to clear it
chmod -t /usr/local/tmp
chmod 0777 /usr/local/tmp

ls -ld /tmp
# drwxrwxrwt 22 root root 4096 Sep 27 09:47 /tmp
```

### setuid & setgid

- allow users to run an executable with the file system permissions of the executable's owner or group respectively and to change behaviour in directories.
- They are often used to allow users on a computer system to run programs with temporarily elevated privileges in order to perform a specific task. While the assumed user id or group id privileges provided are not always elevated, at a minimum they are specific.

- are needed for tasks that require different privileges than what the user is normally granted, such as the ability to alter system files or databases to change their login password

- The setuid and setgid flags **only have effect on binary executable files**. Setting these bits on scripts like bash, perl or python does not have any effect

- When the setuid or setgid attributes are set on an executable file, then any users able to execute the file will automatically execute the file with the privileges of the file's owner (commonly root) and/or the file's group, depending upon the flags set.

- The setuid and setgid bits are normally set with the command `chmod` by setting the high-order octal digit to **4** for setuid or **2** for setgid.

- `chmod 6711 file` will set both the setuid and setgid bits (4+2=6), making the file read/write/executable for the owner (7), and executable by the group (first 1) and others (second 1). When a user other than the owner executes the file, the process will run with user and group permissions set upon it by its owner. For example, if the file is owned by user root and group wheel, it will run as root:wheel no matter who executes the file. Equivalent to `chmod ug+s`

- Setting the **setgid permission on a directory** ("`chmod g+s`") causes new files and subdirectories created within it to **inherit its group ID**, rather than the primary group ID of the user who created the file (the owner ID is never affected, only the group ID).

  - Newly created subdirectories inherit the setgid bit. Thus, this enables a shared workspace for a group without the inconvenience of requiring group members to explicitly change their current group before creating new files or directories.
  - only affects the group ID of new files and subdirectories created after the setgid bit is set, and is not applied to existing entities.
  - does not affect group ID of the files that are created elsewhere and moved to the directory in question. The file will continue to carry the group ID that was effected when and where it was created.

- Setting the setgid bit on existing subdirectories must be done manually, with a command such as `find /path/to/directory -type d -exec chmod g+s '{}' \;`

## getty and login

- getty is a program that attaches to terminals and displays a login prompt.

- After you enter your login name, getty replaces itself with the login pro-
gram, which asks for your password.

- If you enter the correct password, login
replaces itself (using exec()) with your shell.

- most
users now log in either through a graphical interface such as **gdm** (GNOME Display Manager) or remotely with **SSH**

## Setting the Time

- The kernel maintains the **system clock** (displayed by `date`)

- PC hardware has a battery-backed real-time clock (**RTC**) or hardware clock.

- you can reset the system
clock to the current hardware time with `hwclock`.

- Keep your hardware clock
in Universal Coordinated Time (UTC) in order to avoid any trouble with
time zone or daylight savings time corrections. You can set the RTC to your
kernel’s UTC clock using this command:

  ```zsh
  hwclock --hctosys --utc
  ```

- **Time drift** is the current difference
between the kernel time and the true time

### Kernel Time Representation and Time Zones

- `/etc/localtime` binary file for local time

- `/usr/share/zoneinfo` contain files for different zones

- copy a file from zoneinfo to localtime or create a symbolic link to change the zone

- `tzselect`: selecting right time zone

- `export TZ=US/Central` change time zone for a shell session.

### Network Time

- If machine have permanent internet access: NTP deamon (Network Time Protocol) included in `ntpd` package
- If your machine doesn’t have a permanent Internet connection, you can
use a daemon like `chronyd` to maintain the time during disconnections.

## cron

- crontab files are in `/var/spool/cron/crontabs`

- `crontab file`: make the file your crontab

- Every user have a crontab file

- system crontab files:
  - `/etc/crontab`: is for superuser. Not to be edited with `crontab`; has an extra filed for user to run the job.
  - `/etc/cron.d/` directoy on some systems.
  - These files also have username fields, that none of the other crontabs do. Just edit the file and there is **no need to** run `crontab` to update files.

## Scheduling One-Time Tasks with `at`

```zsh
sudo apt install at
at 10:30
at> myjob
# ctrl+D to close the stdin
at 22:30 30.09.15
atq # list scheduled job
atr # remove job
```

## User switching

- kernel handles userID switching using two ways:
  - setuid() ssyscalls
  - setuid attribute of binary files
- kernel basic rules about processes:
  - A process running as root (userid 0) can use setuid() to become any
other user.
  - A process not running as root has severe restrictions on how it may use
setuid(); in most cases, it cannot.
  - Any process can execute a setuid program as long as it has adequate file
permissions.

## Process Ownership, Effective UID, Real UID, and Saved UID

- real user ID (ruid) is the owner of process (can send signal & kill)

- effective user ID (euid) is like th actor and defines the access right for a process. It is changed to enable a non-privileged user to access files that can only be accessed by root.

- Saved UserID : It is used when a process is running with elevated privileges (generally root) needs to do some under-privileged work, this can be achieved by temporarily switching to non-privileged account.

- By default, `ps` and other system diagnostic programs
show the effective user ID

```zsh
ps -eo pid,euser,ruser,comm

# print uid by
id
```

## User Identification,Authentication, and Authorization

- The **identification** portion of security
answers the question of who users are. Handled by **kernel**.

- The **authentication** piece asks users
to prove that they are who they say they are. Handled by **user space**.

- Finally, **authorization** is used to
define and limit what users are allowed to do. Handled by **kernel** (through setuid()).

- A process wants to know its username (effective user name):
  - send `geteuid()` syscall tp kernel to get euid
  - parses lines in `/etc/passwd` to get relevant unername
- In standard way, standard libraries are used to get information about user (map username to user id):
  - `getuid()` --> `getpwuid()`
- Shadow file provided a solution for a system-wide configured password authentication. But **PAM** is more complete.

## PAM

- Pluggable Authentication Modules (PAM): shared libraries for authentication.

- PAM employs a number of **dynamically loadable authentication modules**.

- PAM configurations in `/etc/pam.d` or `/etc/pam.conf` in older distros

- Each line in pam  config file: `auth requisite pam_shells.so`:
  - function type: The function that a user application asks PAM to perform
  - control argument: ontrols what PAM does after success
or failure of its action for the current line 
  - module: The authentication module that runs for this line
- the module and function together determine PAM’s action:
  - `pam_unix.so` module + auth function: checks password
  - `pam_unix.so` module + password function: sets password
- **Function type:** A user application can ask PAM to perform one of the following **four functions**:
  - auth
  - account
  - session
  - password
- Control arguments and stack rules:
  - user process nvokes a specific function (e.g auth) and PAM takes appropriate actions
  - rules specified by its configuration lines **stack**, meaning that you can apply many rules when performing a sfunction.
  - There are two kinds of control arguments: 
    - the simple syntax:
      - sufficient: if rule successful, return seccess to process & not look foollowing lines
      - requiste: if seccessful, proceed; if not retun failure to process
      - required: always proceed to next rules, but finally return failure if failed
    - a more advanced syntax (inside `[]`).

`chsh` example:

```zsh
# check if root has requested, if yes retun seccess, if no proceed
auth sufficient pam_rootok.so
# check whether shell is listed in /etc/shells, if yes procceed, if no return failure
auth requisite  pam_shells.so
# check user password, if correct, retun success, if no procced
auth sufficient pam_unix.so
# pam_deny.so always retun failure
auth required pam_deny.so
```
