## device files (device nodes)

- Device files are IO interface for devices.
- Not all devices have device files because the block and character device I/O interfaces are not appropriate in all cases. network interfaces don’t have device files.
- Linux uses the same design for device files as do other Unix flavors.
- a programmer use regular file operations to work with a device
- so you don’t have to be a programmer to use a device
- not all devices or device capabilities are accessible with standard file I/O
- device files:  block (hard drives), character (tty, printer), pipe, and socket (b,c,p,s)
- `/sys/block` contain symbolic links to relevant devices in `/sys/devices/`

```zsh
# ls -l sd*
brw-rw---- 1 root disk 8,  0 Aug 29 13:32 sda
brw-rw---- 1 root disk 8,  1 Aug 29 13:32 sda1
brw-rw---- 1 root disk 8,  2 Aug 29 13:32 sda2
brw-rw---- 1 root disk 8,  3 Aug 29 13:32 sda3
brw-rw---- 1 root disk 8, 16 Aug 29 13:32 sdb
brw-rw---- 1 root disk 8, 17 Aug 29 13:32 sdb1

# numbers before date (8,  0 etc) are major
# and minor device numbers.
# same devices have same major device number
```

- `udevadm` utility

```zsh
udevadm info --query=all /dev/sda  #retrrieve all info
udevadm info --query=path /dev/sda # retrieve path
# /devices/pci0000:00/0000:00:1f.2/ata1/host0/target0:0:0/0:0:0:0/block/sda
```

## dd and Devices

-The program dd is extremely useful when working with **block** and **character**
devices.
- This program’s sole function is to read from an input file or stream
and write to an output file or stream, possibly doing some encoding conver-
sion on the way.

```zsh
# read 1024 or 1k of input file and write it to output file. Only read one block.
dd if=/dev/zero of=new_file bs=1024 count=1
```

## find the name of a device

- Standard way: using `udevadm`
- lokking in `/sys`
- looking at `dmesg`
- looking at `cat /proc/devices` files: for a list of character and block devices (and their major device number)
- looking at `mount`
- for block devices: `blkkid` and `lsblk`

A note about **SCSI**:
-SCSI is both the name of hardwares and protocol. The hardware is old and is not used anymore in modern machines but the  protocol is universall.
- USB uses SCSI to tranfer data.
- kernel uses SCSI (at some point) to work with SATA disks.
- `lsscsi` to list all relevant devices

### Hard Disks: /dev/sd*

- sd stands for SCSI disk

### PATA Hard Disks: /dev/hd*

Is used for old hardware on older kernel. If a SATA disk is recognized as hd, it mean that it is running in a compatibility mode (change SATA controler in BIOS).

### CD/DVD devices: /dev/sr*

- these devices are used for **reading** functionality of optical driverss.
- For the write and rewrite capabilities of optical devices, you’ll use the **“generic” SCSI** devices such as `/dev/sg0`.

### Terminals: /dev/tty*, /dev/pts/*, and /dev/tty

- tty: controling terminal of the current process
- tty1: first virtual terminal
- pts0: first psudoterminal

### Display Modes and Virtual Consoles

- Linux has two primary display modes: text mode and an X Window System server
- Linux supports **virtual consoles** to multiplex the display (ctrl+alt+F1: tty1)
- some ttys my be busy with getty
- `sudo chvt 1` to force virtual terminal to change into tty1.

### Serial Ports: /dev/ttyS*

- Older RS-232 type and similar serial ports are special terminal devices.
- Plug-in USB serial adapters show up with USB and ACM with the names /dev/ttyUSB0, /dev/ttyACM0, /dev ttyUSB1, /dev/ttyACM1
- The port known as COM1 on Windows is /dev/ttyS0; COM2 is /dev/ttyS1;

###  Parallel Ports: /dev/lp0 and /dev/lp1

- unidirectional parallel port devices
- The bidirectional parallel ports are /dev/parport0 and /dev/parport1

### Audio Devices: /dev/snd/*, /dev/dsp, /dev/audio, and More

kernel-level devices:

- **Advanced Linux Sound Architecture (ALSA)** system interface: `/dev/snd`

- the older** Open Sound System (OSS)**: `/dev/dsp`, `/dev/audio`

### Creating Device Files

are managed by `devtmpfs` and `udev`.  
Previous managment system used to use: `MAKEDEV` or `devfs`
```zsh
# create device file in /dev with name=sda1 & block device & major number 8 & minor device number 2
# usefull in creating named pipes or creating nodes in system recovery
mknod /dev/sda1 b 8 2
mknod location p
```