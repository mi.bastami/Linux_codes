<!-- TOC -->

- [Quoting](#quoting)
- [Shell commands](#shell-commands)
  - [Pipelines](#pipelines)
  - [List of commands](#list-of-commands)
  - [Compound Commands](#compound-commands)
    - [Looping Constructs](#looping-constructs)
      - [until](#until)
      - [while](#while)
      - [for](#for)
    - [Conditional constructs](#conditional-constructs)
      - [if](#if)
      - [case](#case)
      - [`((expression))`: The arithmetic expression](#expression-the-arithmetic-expression)
      - [`[[expression]]`](#expression)
    - [Grouping Commands](#grouping-commands)
      - [`(list of cammands)`](#list-of-cammands)
      - [`{list of Commands;}`](#list-of-commands-1)
- [Functions](#functions)
- [Shell Parameters](#shell-parameters)
  - [nameref](#nameref)
  - [Positional Parameters](#positional-parameters)
  - [Special Parameters](#special-parameters)
  - [difference between `$*` and `$@`](#difference-between--and-)
  - [return or exit](#return-or-exit)
- [Shell expansions & string manupulations](#shell-expansions--string-manupulations)
  - [History expansion `event:word:modifier`](#history-expansion-eventwordmodifier)
    - [modifiers](#modifiers)
  - [Brace expansion](#brace-expansion)
  - [Tilde Expansion](#tilde-expansion)
  - [parameter or variable expansions](#parameter-or-variable-expansions)
  - [String manupulations with `expr`](#string-manupulations-with-expr)
  - [Command Substitution](#command-substitution)
  - [Arithmetic Expansion `$(( expression ))`](#arithmetic-expansion--expression-)
  - [Process Substitution `<(list)`, `>(list)`](#process-substitution-list-list)
  - [Word Splitting](#word-splitting)
    - [Controlling Word Splitting](#controlling-word-splitting)
  - [Filename Expansion: (Filename generation in  zsh,Path expansion or Globbing)](#filename-expansion-filename-generation-in-zshpath-expansion-or-globbing)
    - [Pattern Matching](#pattern-matching)
  - [File Generation (Globbing, filename  expansion): zsh](#file-generation-globbing-filename-expansion-zsh)
    - [Basic patterns](#basic-patterns)
  - [Quote Removal](#quote-removal)
- [Redirection](#redirection)
  - [Here Documents `<<delimiter`, `<<-delimiter`](#here-documents-delimiter--delimiter)
  - [Here Strings `<<<`](#here-strings-)
- [`mapfile` or `readarray`](#mapfile-or-readarray)
- [`read`](#read)
  - [Syntax](#syntax)
  - [options](#options)
  - [Examples](#examples)
- [`test` command (`[]`)](#test-command-)
  - [Syntax](#syntax-1)
    - [File tests](#file-tests)
    - [String tests](#string-tests)
    - [Shell options and variables](#shell-options-and-variables)
    - [Simple logic (test if values are null)](#simple-logic-test-if-values-are-null)
    - [Numerical comparisons](#numerical-comparisons)
  - [Options](#options-1)
  - [examples](#examples-1)
  - [Combine multiple test conditions](#combine-multiple-test-conditions)
- [Parsing bash script options with `getopts`](#parsing-bash-script-options-with-getopts)
  - [Shifting processed options](#shifting-processed-options)
  - [Parsing options with arguments](#parsing-options-with-arguments)
  - [An example](#an-example)
  - [Another getopts example](#another-getopts-example)
- [`tee`: Redirect output to multiple files or processes](#tee-redirect-output-to-multiple-files-or-processes)
- [Other notes](#other-notes)
- [`split` and `cssplit` functions](#split-and-cssplit-functions)
  - [`split`](#split)
    - [examples](#examples-2)
  - [`csplit`](#csplit)
    - [examples](#examples-3)
- [`body() function in NGSeasy`](#body-function-in-ngseasy)
- [bioawk](#bioawk)
- [Parallel](#parallel)
- [xargs](#xargs)
- [GNU Parallel](#gnu-parallel)
  - [Trivially parallelizable jobs](#trivially-parallelizable-jobs)
  - [installation](#installation)
  - [run multiple commands in parallel](#run-multiple-commands-in-parallel)
  - [basic examples](#basic-examples)
  - [Combinatorials](#combinatorials)
  - [Parallelize Functions](#parallelize-functions)
  - [`--pipe`](#--pipe)
  - [Parallelizing existing scripts](#parallelizing-existing-scripts)
  - [GNU Parallel for Variant Calling](#gnu-parallel-for-variant-calling)
    - [Split chromosomes from a BAM](#split-chromosomes-from-a-bam)
    - [Apply an operation to each chromosome](#apply-an-operation-to-each-chromosome)
    - [Combine the variant calls](#combine-the-variant-calls)
  - [Using parallel for programs that require more than one input file](#using-parallel-for-programs-that-require-more-than-one-input-file)
  - [splitting a bog job to make use of all cpus](#splitting-a-bog-job-to-make-use-of-all-cpus)
  - [EXAMPLE: Replace a for-loop](#example-replace-a-for-loop)
  - [Running NCBI-BLAST jobs in parallel](#running-ncbi-blast-jobs-in-parallel)
    - [Using GNU Parallel to split the large job to smaller chunks](#using-gnu-parallel-to-split-the-large-job-to-smaller-chunks)
    - [Splitting input query in to smaller pieces](#splitting-input-query-in-to-smaller-pieces)
  - [EXAMPLE: Parallelizing BLAT](#example-parallelizing-blat)
  - [EXAMPLE: Blast on multiple machines](#example-blast-on-multiple-machines)
- [`find`](#find)
- [`sed`](#sed)
- [working with processes: `ps`](#working-with-processes-ps)
  - [Interrogating Processes with +top+ and Understanding Bottlenecks](#interrogating-processes-with-top-and-understanding-bottlenecks)
- [usefull process related commands](#usefull-process-related-commands)
  - [`pgrep`](#pgrep)
  - [`pstree`](#pstree)
  - [`pidof`](#pidof)
  - [`ps`](#ps)
- [Interacting with Processes Through Signals: Using Kill](#interacting-with-processes-through-signals-using-kill)
- [Prioritizing Processes: Using Nice and Renice](#prioritizing-processes-using-nice-and-renice)
    - [Disk Usage](#disk-usage)
- [installing bioawk](#installing-bioawk)
- [arrays](#arrays)
  - [Define arrays](#define-arrays)
  - [Accessing array elements](#accessing-array-elements)
  - [Array Attributes: `declare`](#array-attributes-declare)
    - [`-a` and `-p` attributes](#-a-and--p-attributes)
    - [`-U` attribute of zsh](#-u-attribute-of-zsh)
    - [`-T` attribute of zsh and tied arrays](#-t-attribute-of-zsh-and-tied-arrays)
  - [Array expansion](#array-expansion)

<!-- /TOC -->

# Quoting

Quoting is used to remove the special meaning of certain characters or words to the shell. Quoting can be used to disable special treatment for special characters, to prevent reserved words from being recognized as such, and to prevent parameter expansion. Each of the shell metacharacters as special meaning to the shell and must be quoted if it is to represent itself.
There are **three quoting mechanisms**:

1. Escape Character `\`: It preserves the literal value of the next character that follows, with the exception of newline.
2. Single Quotes `''`: reserves the literal value of each character
3. Double Quotes `""`: reserves the literal value of all characters with the exception of `‘$’, ‘‘’, ‘\’`, and, when history expansion is enabled, `!`; A double quote may be quoted within double quotes by preceding it with a backslash.

**Other Quotes:**

- **ANSI-C Quoting**: `$'word'`: The word expands to string, with backslash-escaped characters replaced as specified by the ANSI C standard. It underestand backslashes (e.g. \n as newline).  

 ```shell
 echo "${v:-'ab\ncd'}"
 # output: 'ab\ncd'

 echo "${v:-$'ab\ncd'}"
 ab
 cd

 echo $'ab\ncd'
 ab
 cd
 ```

- **Locale-Specific Translation**: $"word": will cause the string to be translated according to the current locale. If the current locale is C or POSIX, the dollar sign is ignored. If the string is translated and replaced, the replacement is double-quoted.

# Shell commands

## Pipelines

- A pipeline is a sequence of one or more commands separated by one of the control operators
`|` or `|&`. each command reads the previous command’s output. This connection is performed **before any redirections** specified by the command.
- If `|&` is used, command1’s standard error, in addition to its standard output, is connected to command2’s standard input through the pipe; it is shorthand for `2>&1 |`. This implicit redirection of the standard error to the standard output is performed **after any redirections** specified by the command.
- The reserved word `time` causes timing statistics to be printed for the pipeline once it finishes. `time` outputs to `2>` stderr.

  ```Shell
  # get the individual times
  ( time foo.sh ) 2>foo.time | ( time bar.sh ) 2> bar.time

  # get the whole time
  time sh -c "foo.sh | bar.sh "
  time ( foo.sh | bar.sh )
  time { foo.sh | bar.sh; } 2>time.txt
  ```

- Each command in a pipeline is executed in its own subshell, which is a separate process
- If the `lastpipe` option is enabled using the `shopt` builtin, the last element of a pipeline may be run by the shell process.
- If the reserved word `!` precedes the pipeline, the exit status is the logical negation of the exit status as described above.
- The **exit status of a pipeline** is the exit status of the last command in the pipeline, unless the `pipefail` option is enabled.
- If pipefail is enabled, the pipeline’s return status is the value of the last (rightmost) command to exit with a non-zero status, or zero if all commands exit successfully.
- The shell waits for all commands in the pipeline to terminate before returning a value.
- `command1 && command2`: command2 is executed if, and only if, command1 returns an exit status of zero (success).
- `command1 || command2`: command2 is executed if, and only if, command1 returns a non-zero exit status.
- The return status of `and` and `or` lists is the exit status of the last command executed in the list.

## List of commands

- A list is a sequence of one or more pipelines separated by one of the operators `;`, `&`, `&&`, or `||`, and optionally terminated by one of `;`, `&`, or a newline.
- A sequence of one or more newlines may appear in a list to delimit commands, equivalent to a semicolon.
- If a command is terminated by the control operator `&`, the shell executes the command
**asynchronously** in a subshell (excecuting in **background**,). These are asynchronous commands: The shell does not wait for the command to finish, and the return status is 0 (true). The process inherits stdout/stderr from the shell (so it still writes to the terminal).
- Commands separated by a ‘;’ are executed sequentially; the shell waits for each command to terminate in turn. The return status is the exit status of the last command executed.

## Compound Commands

Compound commands are the shell programming language constructs. Bash provides **looping constructs**, **conditional commands**, and mechanisms to **group** commands and execute them as a unit.

### Looping Constructs

#### until

  ```Shell
  until test-commands; do consequent-commands; done
  ```

  ```Shell
  until [CONDITION]
  do
    [COMMANDS]
  done
```

- Execute consequent-commands as long as test-commands has an exit status which is not zero.
- If the condition evaluates to false, commands are executed. Otherwise, if the condition evaluates to true the loop will be terminated and the program control will be passed to the command that follows.
- The return status is the exit status of the last command executed in consequent-commands, or zero if none was executed.
- **The while and until loops are similar** to each other with the **main difference** being that the while loop iterates as long as the condition evaluates to true and the until loop iterates as long as the condition evaluates to false.

  ```Shell
  counter=0

  until [ $counter -gt 5 ]
  do
    echo Counter: $counter
    ((counter++))
  done
  ```

- **A git example** The following script may be useful if your git host is having downtime and instead of manually typing git pull multiple times until the host is online you can run the script once.

  ```Shell
  until git pull &> /dev/null
  do
      echo "Waiting for the git host ..."
      sleep 1
  done

  echo -e "\nThe git repository is pulled."

```

#### while
  ``` Shell
  while test-commands; do consequent-commands; done
  ```

- Execute consequent-commands as long as test-commands has an exit status of zero.
- The return status is the exit status of the last command executed in consequent-commands, or zero if none was executed.

The **example** below was written to copy pictures that are made with a webcam to a web directory. Every five minutes a picture is taken. Every hour, a new directory is created, holding the images for that hour. Every day, a new directory is created containing 24 subdirectories. The script runs in the background.

  ```Shell
  #!/bin/bash

  # This script copies files from my homedirectory into the webserver directory.
  # (use scp and SSH keys for a remote directory)
  # A new directory is created every hour.

  PICSDIR=/home/carol/pics
  WEBDIR=/var/www/carol/webcam

  while true; do
     DATE=`date +%Y%m%d`
     HOUR=`date +%H`
     mkdir $WEBDIR/"$DATE"

     while [ $HOUR -ne "00" ]; do
      DESTDIR=$WEBDIR/"$DATE"/"$HOUR"
      mkdir "$DESTDIR"
      mv $PICDIR/*.jpg "$DESTDIR"/
      sleep 3600
      HOUR=`date +%H`
     done

  done
  ```

#### for

  ```shell
  for name [ [in [words ...] ] ; ] do commands; done
  ```

- Expand words and execute commands once for each member in the resultant list, with name bound to the current member.
- If `in words` is not present, the for command executes the commands once for each positional parameter that is set, as if `in "$@"` had been specified
- The return status is the exit status of the last command that executes. If there are no items in the expansion of words, no commands are executed, and the return status is zero.
- The `break` and `continue` builtins may be used to control loop execution.
- The continue statement resumes iteration of an enclosing for, while, until or select loop

  ```Shell
  for i in 1 2 3 4 5
  do
    command1
    command2
    if (disaster); then
      break
    fi
    command3 # executed only if no disaster occured
  done
  ```

  **Using continue in a bash for loop**
  There are also times when you want to break the execution of the series of commands, for a certain value on the series, but do not stop the complete program. In this case, if the condition is true, the command 3 is not executed over that value, and the program jumps to the next one.

  ```Shell
  for i in [series]
  do
          command 1
          command 2
          if (condition) # Condition to jump over command 3
                  continue # skip to the next value in "series"
          fi
          command 3
  done
  ```

  ```Shell
  for file in /etc/*
  do
    if ("$file" == "scecific file"); then
      # do some operation to file
      break
    fi
  done
  ```

  ```Shell
  for i in $(command)
  for i in {1..5}
  for i in {1..10..2}
  for i in $(seq 1 2 20) # {1..20..2} is better
  ```

**An alternative form:**

  ```Shell
  for (( expr1 ; expr2 ; expr3 )) ; do commands ; done
  ```

- expr1: initializer; expr2: condition; expr3: countinh expressions
- `for ((c=5;c < 5; c++))`

### Conditional constructs

#### if

  ```Shell
  if test-commands; then
      consequent-commands;
  [elif more-test-commands; then
      more-consequents;]
  [else alternate-consequents;]
  fi
  ```

- in the below example, only the exit status of the COMMAND is needed for the if test and therefore the output should be suppressed somehow

  ```Shell
  if COMMAND 2>& /dev/null; then
    # perform some actions
  fi

  if grep -q "patter" file #-q make grep quient
  then
    # do something
  fi

  if cmp a b &> /dev/null; then
    echo "files are identical"
  else echo "files rae not identical"
  fi
  ```

#### case

The CASE statement is the simplest form of the IF-THEN-ELSE statement in BASH.

  ```Shell
  case word in
     [ [(] pattern [| pattern]...) command-list ;;]...
  esac
  ```

  ```Shell
  case EXPRESSION in

    PATTERN_1)
      STATEMENTS
      ;;

    PATTERN_2)
      STATEMENTS
      ;;

    PATTERN_N)
      STATEMENTS
      ;;

    *)
      STATEMENTS
      ;;
  esac
  ```

- case will selectively execute the command-list corresponding to the **first pattern**
that matches word. The match is performed accoring to pattern matching.
- The `|` is used to separate multiple patterns
- list of patterns and an associated command-list is known as a **clause**.
- There may be an arbitrary number of case clauses, each terminated by a `;;`,
`;&`, or `;;&`. If the ‘;;’ operator is used, no subsequent matches are attempted after the first pattern match. Using `;&` in place of `;;` causes execution to continue with the command-list associated with the next clause, if any. Using `;;&` in place of `;;` causes the shell to test the patterns in the next clause, if any, and execute any associated command-list on a successful match, continuing the case statement execution as if the pattern list had not matched.
- The first pattern that matches determines the command-list that is executed.
- It’s a **common idiom** to use `*` as the final pattern to define the default case, since that pattern will always match.
- The return status is zero if no pattern is matched. Otherwise, the return status is the exit status of the command-list executed.

  ```Shell
  echo -n "Enter the name of an animal: "
  read ANIMAL
  echo -n "The $ANIMAL has "

  case $ANIMAL in
    horse | dog | cat) echo -n "four";;
    man | kangaroo ) echo -n "two";;
    *) echo -n "an unknown number of";;
  esac
  echo " legs."
  ```

This **example** prints the number of lines,number of words and delete the lines that matches the given pattern.

  ```Shell
  #!/bin/bash

  # Check 3 arguments are given #
  if [ $# -lt 3 ]
  then
        echo "Usage : $0 option pattern filename"
        exit
  fi

# Check the given file is exist #
  if [ ! -f $3 ]
  then
        echo "Filename given \"$3\" doesn't exist"
        exit
  fi

  case "$1" in

  # Count number of lines matches
  -i) echo "Number of lines matches with the pattern $2 :"
      grep -c -i $2 $3
      ;;
    # Count number of words matches
  -c) echo "Number of words matches with the pattern $2 :"
      grep -o -i $2 $3 | wc -l
      ;;
    # print all the matched lines
  -p) echo "Lines matches with the pattern $2 :"
      grep -i $2 $3
      ;;
    # Delete all the lines matches with the pattern
   -d) echo "After deleting the lines matches with the pattern $2 :"
      sed -n "/$2/!p" $3
      ;;
   *) echo "Invalid option"
      ;;
   esac
  ```

#### `((expression))`: The arithmetic expression

If the value of the expression is non-zero, the return status is 0; otherwise the return status is 1. This is exactly equivalent to `let "expression"`

#### `[[expression]]`

Return a status of 0 or 1 depending on the evaluation of the conditional expression expression. **Word splitting and filename expansion are not performed** on the words between the `[[` and `]]`. Therefore, the following is safe (without "") `[[ -f $filenames]]` but this is not safe [-f $filenames]; tilde expansion, parameter and variable expansion, arithmetic expansion, command substitution, process substitution, and quote removal are performed.

- `[[string = pattern]]` identical to `[[string == pattern]]`. The `=` operator is identical to `==`.
- When the `==` and `!=` operators are used, the string to the right of the operator is considered a **pattern** and matched according to the pattern matching rules.
- `[[string =~ regexp`:  is considered a POSIX extended regular expression (ERE). If the regular expression is syntactically incorrect, the conditional expression’s return value is 2. In **zsh**, upon match, the scalar parameter **MATCH** is set to the substring that matched the pattern and the integer parameters **MBEGIN** and **MEND** to the index of the start and end, respectively, of the match in string, such that if string is contained in variable var the expression `${var[$MBEGIN,$MEND]}` is identical to `$MATCH`. For example, if the string ‘a short string’ is matched against the regular expression `s(...)t`, then (assuming the option KSH_ARRAYS is not set) MATCH, MBEGIN and MEND are ‘short’, 3 and 7, respectively, while **match**, **mbegin** and **mend** are single entry arrays containing the strings ‘hor’, ‘4’ and ‘6’, respectively.
- **Bracket expressions in regular expressions must be treated carefully**, since normal quoting characters lose their meanings between brackets.
- If the pattern is stored in a shell variable, quoting the variable expansion forces the entire pattern to be matched as a string. The following two sets of commands are not equivalent:

  ```Shell
  pattern=’\.’

  [[ . =~ $pattern ]]    # will succeed
  [[ . =~ \. ]]          # will succeed

  [[ . =~ "$pattern" ]]  # will fail
  [[ . =~ ’\.’ ]]        # will fail
  ```

  In the second two, pattern is a string and the backslash will be part of the pattern to be matched. In the first two examples, the backslash removes the special meaning from ‘.’, so the literal ‘.’ matches.

- Storing the regular expression in a shell variable is often a useful way to avoid
problems with quoting characters that are special to the shell.

  ```Shell
  [[ $line =~ [[:space:]]*?(a)b ]]

  pattern=’[[:space:]]*?(a)b’
  [[ $line =~ $pattern ]]
  ```

  If you want to match a character that’s special to the regular expression grammar, it has to be quoted to remove its special meaning. This means that in the pattern ‘xxx.txt’, the ‘.’ matches any character in the string (its usual regular expression meaning), but in the pattern ‘"xxx.txt"’ it can only match a literal ‘.’;

  ```Shell
  pattern='xx.txt'
  [[ "xxxtxt =~ $pattern" ]] && echo TRUE # TRUE

  pattern="xx.txt"
  [[ "xxxtxt =~ $pattern" ]] && echo TRUE # TRUE

  pattern='"xx.txt"'
  [[ "xxxtxt =~ $pattern" ]] && echo TRUE # it fails to match
  ```

- **`( expression )`**
inside `[[]]`, returns the value of expression. This may be used to override the normal precedence of operators.
- **`[[! expression]]`**: returns true if expression is false.
- `[[ expression1 && expression2 ]]`
- `[[expression1 || expression2]]`
True if either expression1 or expression2 is true.

### Grouping Commands

Bash provides two ways to group a list of commands to be executed as a unit. When commands are grouped, redirections may be applied to the entire command list. For example, the output of all the commands in the list may be redirected to **a single stream**.

#### `(list of cammands)`

Placing a list of commands between parentheses causes a **subshell** environment to be created

#### `{list of Commands;}`

Placing a list of commands between curly braces causes the list to be executed in the **current shell** context. The **semicolon** (or newline) following list is required.

# Functions

```shell
name () compound-command [ redirections ]
function name [()] compound-command [ redirections ]
```

- The exit status of a function definition is zero unless a syntax error occurs or a readonly function with the same name already exists.
- When a function is executed, the arguments to the function become the positional parameters during its execution
- The special parameter ‘#’ that expands to the number of positional parameters is updated to reflect the change. Special parameter 0 is unchanged.
- Variables local to the function may be declared with the `local` builtin.
- The shell uses **dynamic scoping** to control a variable’s visibility within functions:
  - Local variables "**shadow**" variables with the same name declared at previous scopes. For instance, a local variable declared in a function hides a global variable of the same name: references and assignments refer to the local variable, leaving the global variable unmodified. When the function returns, the global variable is once again visible.
- The value of a variable that a function sees depends on its value within its caller

For example, if a variable var is declared as local in function func1, and func1 calls
another function func2, references to var made from within func2 will resolve to the local
variable var from func1, shadowing any global variable named var.

  ```shell
  #In func2, var = func1 local
  func1()
  {
  local var=’func1 local’
  func2
  }
  func2()
  {
  echo "In func2, var = $var"
  }
  var=global
  func1
  ```

# Shell Parameters

- A parameter is an entity that stores values. It can be a name, a number, or one of the special characters listed below. A variable is a parameter denoted by a name. A parameter is set if it has been assigned a value. The null string is a valid value. Once a variable is set, it may be unset only by using the `unset` builtin command.
- If the variable has its integer attribute set, then value is evaluated as an arithmetic expression even if the $((...)) expansion is not used.

  ```shell
  declare -i x
  x=1+1
  echo $x # 2
  ```

- **Word splitting** is not performed, with the exception of "$@".
- **Filename expansion** is not performed.
- `+=`: to append or  add to the previouss value

  ```shell
  y="test"
  y+="B"
  echo $y # testB
  ```

## nameref

A variable can be assigned the nameref attribute using the -n option to the declare or local builtin commands to create a nameref, or a reference to another variable. This allows variables to be manipulated indirectly. Whenever the nameref variable is referenced, assigned to, unset, or has its attributes modified (other than using or changing the nameref attribute itself), the operation is actually performed on the variable specified by the nameref variable’s value. A nameref is commonly used within shell functions to refer to a variable whose name is passed as an argument to the function. For instance, if a variable name is passed to a shell function as its first argument, running `declare -n ref=$1` inside the function creates a nameref variable ref whose value is the variable name passed as the first argument.

```shell
function boo()
{
    local -n ref=$1
    ref='new'
}

SOME_VAR='old'
echo $SOME_VAR # -> old
boo SOME_VAR
echo $SOME_VAR # -> new
# Passing the variable's name to boo, not its value: boo SOME_VAR, not boo $SOME_VAR
```

## Positional Parameters

Positional parameters are assigned from the shell’s arguments when it is invoked, and may be reassigned using the set builtin command. Positional parameter N may be referenced as ${N}, or as $N when N consists of a single digit. The set and shift builtins are used to set and unset them. The positional
parameters are temporarily replaced when a shell function is executed.  
After 9 you need to use braces: ${10}, though **zsh** allows $10, $11, and so on

## Special Parameters

- `$*`
  - Expands to the positional parameters, starting from one.
  - When the expansion is **not within double quotes**, each positional parameter expands to a separate word. Those words are subject to **further word splitting** and pathname expansion.
  - When the expansion occurs **within double quotes**, it expands to a single word with the value of each parameter separated by the first character of the IFS special variable. If IFS is null, the parameters are joined without intervening separators.
- `$@`
  - Expands to the positional parameters, starting from one.
  - this expands each positional parameter to a separate word; if not within double quotes, these words are subject to word splitting.
  - When the expansion occurs within double quotes, and word splitting is performed, each parameter expands to a separate word.

- `$#`: Expands to the number of positional parameters in decimal.
- `$?`: exit status
- `$$`: expands to process ID of shell
- `$!`:  expands to process ID of job recently pplaced to background
- `$0`: expands to name of shell

## difference between `$*` and `$@`

The difference between $* and $@ is exactly the same as for ${arr[@]} and ${arr[*]} array expansions. Functionally, you will only see the difference when you use them in double quotes:

```zsh
set -- one two three
printf '%s\n' "$*"
# one two three
printf '%s\n' "$@"
#one
#two
#three
```
  
Using the set command as in this example allows you to set the positional parameters. **zsh** also allows conventional assignments to be used for them. For example, `5=five` will assign “five” to $5.

## return or exit

A common use for `$0` is when printing the command name in an error message

```bash
if [[ -z $1 ]]; then
  echo "${0##*/}: parameter expected" >&2
  exit 1
fi
```
  
The `exit` statement here exits the shell and says that we want to use an exit status of 1. It won’t just return from the function. In a function, we need to use a return statement instead. In bash, you have to use the `FUNCNAME` (instead of $0) special variable instead to get the name of the function. In **zsh**, it would work as expected.  
One more feature provided by zsh is that the positional parameters can also be accessed via `argv` and `ARGC` special variables.

# Shell expansions & string manupulations

- **bash**: The order of expansions is: **brace expansion**; **tilde expansion**,**parameter and variable expansion**, **arithmetic expansion**, and **command substitution** (done in a left-to-right fashion); **word splitting**; and **filename expansion**. After these expansions are performed, quote characters present in the original word are removed unless they have been quoted themselves (quote removal).

-**zsh**:

  1. **History Expansion**: This is performed only in interactive shells.
  2. **Alias Expansion**: Aliases are expanded immediately before the command line is parsed as explained in Aliasing.
  3. **Process Substitution**
  4. **Parameter Expansion**
  5. **Command Substitution**
  6. **Arithmetic Expansion**
  7. **Brace Expansion**

After these expansions, all unquoted occurrences of the characters ‘\’,‘’’ and ‘"’ are removed.

  8. **Filename Expansion**: If the `SH_FILE_EXPANSION` option is set, the order of expansion is modified for compatibility with sh and ksh. In that case filename expansion is performed immediately after alias expansion, preceding the set of five expansions mentioned above.
  9. Filename Generation (globbing): This expansion, commonly referred to as globbing, is always done last.

## History expansion `event:word:modifier`

Usually in the form `!n:n:t`:
 -`!n` starts history expansion and returns command line n.

- `n`: return the nth argument (e.g. in previous command). `0` return the command itself. `$` last arg. `^` first arg.
  
- `:t` is a modifier returning the tail (the non directory part of path)

### modifiers

After the optional word designator, you can add a sequence of one or more of the following modifiers, each preceded by a `:`. These modifiers also work on the result of **filename generation (globbing)** and **parameter expansion**, except where noted.

```zsh
ls -l linuxbrew/.linuxbrew/Cellar.c
echo !:2:t # echo the tail for the last command (Cellar.c)
echo !-2:2:h # echo current CMD minus one, show head (linuxbrew/.linuxbrew)
echo !-3:2:r # remove suffix (linuxbrew/.linuxbrew/Cellar)
str="milad.c"
echo ${str:r} # milad
```

## Brace expansion

Brace expansion is used to generate arbitrary strings. Brace expansion allows you to create multiple modified command line arguments out of a single argument. ince the list is executed in a subshell, variable assignments do not remain in effect after the subshell completes.

```Shell
echo last{mce,boot,xorg}.log
## output:
lastmce.log lastboot.log lastxorg.log

mkdir /usr/local/src/bash/{old,new,dist,bugs}

echo /usr/{ucb/{ex,edit},lib/{ex?.?*,how_ex}}
# /usr/ucb/ex /usr/ucb/edit /usr/lib/ex?.?* /usr/lib/how_ex
```

**Example for Backup using brace expansion**

```Shell
$ cat bkup.sh
set -x # expand the commands
da=`date +%F`
cp $da.log{,.bak}

$ ./bkup.sh
++ date +%F
+ da=2010-05-28
+ cp 2010-05-28.log 2010-05-28.log.bak
```

**Example for Restore using brace expansion**

```Shell
$ cat restore.sh
set -x # expand the commands
da=`date +%F`
cp $da.log{.bak,}

$ ./restore.sh
++ date +%F
+ da=2010-05-28
+ cp 2010-05-28.log.bak 2010-05-28.log
```

**Brace expansion for Ranges** `{1..5}`, `{a..b}`, `{1..10..2}`
{1..10..2} here 2 is increment

```Shell
$ cat sequence.sh
cat /var/log/messages.{1..3}
echo {a..f}{1..9}.txt

$ ./sequence.sh
May  9 01:18:29 x3 ntpd[2413]: time reset -0.132703 s
May  9 01:22:38 x3 ntpd[2413]: synchronized to LOCAL(0), stratum 10
May  9 01:23:44 x3 ntpd[2413]: synchronized to
May  9 01:47:48 x3 dhclient: DHCPREQUEST on eth0
May  9 01:47:48 x3 dhclient: DHCPACK from 23.42.38.201
..
..
a1.txt a2.txt a3.txt a4.txt b1.txt b2.txt b3.txt b4.txt c1.txt c2.txt c3.txt c4.txt
```

**Brace expansion does not expand variables**
Brace expansion does not expand bash variables, because the brace expansion is the very first step of the shell expansion, variable will be expanded later. The following **doesnot work**: `x=1;y=10;{$x..$y}` as expected and would yield `{1..10} not the sequence`

## Tilde Expansion

- ~  ----> The value of $HOME
- ~/foo ----> $HOME/foo
- ~fred/foo ----> The subdirectory foo of the home directory of the user fred

## parameter or variable expansions

- Basic form is `$a` or `${b}cd`
- if the variable is unset or empty, set the **defalut value** of a variable: `${variable=-default}` example: `test=${test:-0}`. Examples:

  ```Shell
  #test.sh
  TEST_MODE={$TEST_MODE:-0}
  if [[ TEST_MODE -eq 0 ]]
  then
    echo "Running in live mode"
  else
    echo "Running in test mode"
  fi
  ```

  Normally scrit is running in live mode. To change it:

  ```Shell
  env TEST_MODE=1 sh test.sh
  ```

  or use the default value with command line argument or from a config file:

  ```Shell
  if [[ ${cmd_arg_x:-0} -eq 0 ]]
  then
    echo "-x not specified"
  else
    echo "-x is specified"
  fi
  ```

- **alternative value**: `${variable:+alternative}`. If the variable is set, alternative would substitude its value.
 print a file using lp. If the dest
 variable is set, it will be substituted with the -d option preceding it:  

    ```Shell
    lp ${dest:+-d$dest} file
    ```

- **Indirect expansion** `${!var}`

  ```shell
  export xyzzy=plugh ; export plugh=cave
  echo ${xyzzy}  # plugh
  echo ${!xyzzy} # cave
  ```

  There are two execption to the above usage:

  1. `${!prefix*}`: Expands to the names of variables whose names begin with prefix, separated by the first character of the IFS special variable.
  2. `${!name[@]}`: indexes of an array

- **default value of positional parameters:**

  ```Shell
  var=${1:-defaultValue}
  ```

  ```Shell
  #!/bin/bash
  #test.sh
  var=${1:-/home/phpcgi}
  echo "Setting php-cgi at ${var}..."
  # rest of script
  ```

  Run the script as:

  - `test.sh` /my_dir  <--- set php jail at /my_dir dir
  - `test.sh` <--- set php jail at /home/phpcgi dir (the default value)

  A handy example:

  ```Shell
  _mkdir(){
          local d="$1"               # get dirname
          local p=${2:-0755}         # get permission, set default to 0755
          [ $# -eq 0 ] && { echo "$0: dirname"; return; } # $0:expands to'bash'
          [ ! -d "$d" ] && mkdir -m $p -p "$d"
  }
  ```

- `${var:=value}`: he assignment (:=) operator is used to assign a value to the variable if it doesnot already have one; if variable hsa already a value, the assignment doesnot change it; This will not work for positional parameters.

- `${variable:?Error: "Message"}` Display an **Error Message** If `$VAR` not passed and exit.
  If parameter is null or unset, the expansion of word (or a message to that effect if word is not present) is written to the standard error and the shell, if it is not interactive, **exits**. Otherwise, the value of parameter is substituted.

  ```Shell
  ${varName?Error varName is not defined}
  ${varName:?Error varName is not defined or is empty}
  ${1:?"mkjail: Missing operand"}
  MESSAGE="Usage: mkjail.sh domainname IPv4"      ### define error message
  _domain=${2?"Error: ${MESSAGE}"}  ### you can use $MESSAGE too
  ```

  `_domain="${1:?Usage: mknginxconf domainName}"`: if the $1 command line arg is not passed, stop executing the script with an error message.

- **string substitution**
  greedy matching is actually the default for both `${variable/pattern/string}` and `${variable//pattern/string}` substitutions.
  - `${var/search/replace}`: replace the first instance

    ```Shell
    var=aabbcc
    ${var/b/-dd-}
    # output: aa-dd-bcc

    Arr=(milAAA milBBB milCCC)
    echo ${Arr[@]/mil/ziba} # zibaAAA zibaBBB zibaCCC

    str=milAAA milBBB milCCC
    set -- $str
    echo ${@/mil/ziba} # zibaAAA zibaBBB zibaCCC

    str="f11:f12|f21:f22|f31:f32"
    IFS='|'
    set -- $str
    echo $1 # f11:f12

    ```

  - `${var//search/replace}`: replace all instances

     ```Shell
     var=aabbcc
     ${var//b/-dd-}
     ```

     output: aa-dd--dd-cc
  - `${=PATH//:/ }`: in **zsh** you need to add an equals to enable word splitting. Seperate directorie in path with a space or with a tab `${=PATH//:/\\t}`.

- Pattern operators: **removing prefixes and suffixes**:
  - `${VAR#pattern}`: Removes any prefix from the expanded value that matches the pattern.
  The removed prefix is the shortest matching prefix, if you use double pound-signs/hash-marks the longest matching prefix is removed `${VAR##pattern}`
  - `${VAR%pattern}`: removes a matching suffix (single percent for the shortest suffix, double for the longest).
  - In zsh, you can use `${HOME%%[^/]*}` to leave a trailing slash. From end remove suffix using the pattern exept `/`.

  ```Shell
  # The difference is that the star (*) is able to match slashes (/) separating directory names. This is different from globing
  HOME=/usr/people/milad
  echo ${HOME##*/} # milad

  file=data.txt
  echo ${file%.*} #file base: data
  echo ${file#*.} #file extension: txt

  stringZ=abcABC123ABCabc

  echo ${stringZ#a*C}  # 123ABCabc
  #strip out shortest path between a and C
  echo ${stringZ##a*C} # abc

  #parametrize the substrings
  X='a*C'
  echo ${stringZ#$X}  # 123ABCabc
  echo ${stringZ##$X} # abc

  declare -A arr=( [idx1]=milAAA [idx2]=milBBB [idx3]=milCCC)
  echo ${!arr[@]}    # idx1 idx2 idx3
  echo ${!arr[*]}    # idx1 idx2 idx3
  echo ${#arr[@]}    # 3
  echo ${#arr[idx1]} # 6
  echo ${arr[@]#mil} # AAA BBB CCC
  echo ${arr[idx1]#mil} # AAA
  echo ${arr[idx1]#m*A} # AA


  str="milAAA milBBB milCCC"
  set -- $str
  echo ${@#mil}  # AAA BBB CCC
  ```

  ```Shell
  # Remove all filenames in $PWD with "TXT" suffixes to a
  # "txt" suffix. For example, "file.TXT" becomes "file.txt"
  SUFF=TXT
  suff=txt

  for i in $(ls *.$SUFF)
  do
    mv -f $i ${i%.$SUFF}.$suff
  done
  ```

  Also note that these are **glob patterns** not regular expressions.
- `${str/#search/replacement}`: if search matched in front of str, replace it.
- `${str/%search/replacement}`: if search matched in end of str, replace it.

  ```Shell
  stringZ=abcABC123ABCabc
  echo ${stringZ/#abc/XYZ} # XYZABC123ABCabc
  echo ${stringZ/%abc/XYZ} # abcABC123ABCXYZ
  ```

  **The awk equivalent:**

  ```Shell
  string=23skid001
  # Bash: zero based positions
  # awk: 1 based positions

  echo ${string:2:4} # skid

  echo | awk '{print substr("'"${string}"'",3,4)}' # skid
  # piping an empty echo to awk gives it dummy input, and makes it unnecessary # to supply a flename.
  ```

- **extract substrings**
  - bash: `${VAR:offset:length}`: offsets start at zero, if you don't specify a length it goes to the end of the string.
  - zsh `$VAR[3,5]`: array like indexing. Substring starting with the third character in the original string and finishing with the fifth. **zsh also supports the bash syntax**.
character

  ```Shell
  str=abcdefgh
  echo ${str:0:1} # a
  echo ${str:1}   # bcdefgh
  ```

  Negative offsets which count backwards from the end of the string. But in the below example, Bash misinterpret the code because it is like default value expansion.

  ```Shell
  str=abcdefgh
  echo ${str:-3:2} ##abcdefgh
  ```cdef

  To solve it and select from the end of sting:

  ```Shell
  str=abcdefgh
  echo ${str:$((-3)):2} ## fg

  # or
  i=-3
  echo ${str:$i:2} ## fg
  
  # the simple way: add a space
  echo ${str: -3:2

 ```

 The length can also be negative which means from offset to the last minus the last characters specified by length:

 ```Shell
 str=abcdefgh
 echo ${str:2:-2} # cdef
 ```

 Can also be parametrized: `POS=4; $LEN=3; echo ${str:$POS:$LEN}`

 **Reset the positional parameters**
 You can reset positional parameter using `set`.
 Here the first number (i.e. 1) refers to the whole str string, because there is only one whole string here.

 ```Shell
 str=abcdefgh
 set -- $str
 echo ${1:4}   # efgh
 echo ${1:4:2} # ef

 str="abcd efgh ijklm"
 set -- $str
 echo ${2:2} # gh
 echo $3     # ijklm
 echo ${@:2} # efgh ijklm
 echo ${@:2:3} # 3 positional param starting at $2

 arr[0]=abcdefgh
 echo ${arr[0]:2} # cdefgh
 echo ${arr[0]: -7:2} # bc
 ```

- `${!prefix@}` and `${!prefix*}`
Expands to the names of variables whose names begin with prefix, separated by the first character of the IFS special variable. When ‘@’ is used and the expan- sion appears within double quotes, each variable name expands to a separate word.

- `${!name[@]}` and `${!name[*]}`
If name is an array variable, expands to the list of array indices (keys) assigned
in name. If name is not an array, expands to 0 if name is set and null otherwise.
When ‘@’ is used and the expansion appears within double quotes, each key expands to a separate word.

- `${#VAR}`Expand to the length of a variable:
- `${#@}`, `${#*}`: number of positional parameters
- `${#arr[@]}`, `${#arr[**@**]}`: number of array elements
- `${#arr[-1]}`: length of the last element of the array
- `${#arr[3]}`: length of the third element of the array

- **Changing the case of matched patterns**:
  - `${parameter^pattern}`: lowercase to uppercase; only first match
  - `${parameter^^pattern}`: lowercase to uppercase; all matches
  - `${parameter,pattern}`: uppercase to lowercase; only first match
  - `${parameter,,pattern}`: uppercase to lowercase; all matches

  ```Shell
  str="milad Bastami"
  echo ${str^m}  # Milad Bastami
  echo ${str^^m} # Milad BastaMi
  echo ${str,,B} # milad bastami
  echo ${str^^} # MILAD BASTAMI
  echo ${str^} # Milad Bastami

  also works for arrays and positional parameters:
  echo ${array[@]^m}
  echo ${@^m}
  ```

- **operator expansion**: `${parameter@operator}`
  - `${var@Q}`: var will be single quoted; with eny special character being escaped;
  - `${var@E}`: var will be expanded; returning the special meaning of characters.

```Shell
foo="one\ntwo\n\tlast"
echo "$foo"    # one\ntwo\n\tlast
echo -e "$foo" # will expand it
echo ${foo@Q}  # 'one\ntwo\n\tlast'
echo ${foo@E}  # will expand it
done
two
  last

# display a file with multiple lines as a single string
# with escape chars (\n)
foo=$(<file.txt)
echo "${foo@Q}"
'line1\nline2'
```

## String manupulations with `expr`

- String length:

  ```shell
  echo `expr length $string`
  echo `expr  "$string" : '.*'`
  ```

- Length of matching substring at the beginning of string: `expr match "$string" 'substring'`

  ```Shell
  echo `expr match "$string" 'abc[A-Z]*.2'` # 8
  ```

- index: `expr index "$string" '$substring'`: numerical position of first matched character of substring in string.

  ```Shell
  string=abcABC123ABCabc
  echo `expr index "$string" C12` # 6
  echo `expr index "$string" 1c`  # 3
  # c in position#3 matches before 1
  ```

## Command Substitution

```Shell
$(command)
or
`command`
```

- **Embedded newlines** are not deleted, but they may be removed during word splitting.
- If the substitution appears **within double quotes**, **word splitting and filename expansion are not performed** on the results.
- Command substitutions may be **nested**. To nest when using the backquoted form, escape
the inner backquotes with backslashes.
- The command substitution `$(cat file)` can be replaced by the equivalent but **faster** `$(< file)`

## Arithmetic Expansion `$(( expression ))`

All tokens in the expression undergo parameter and variable expansion, command substitution, and quote removal

## Process Substitution `<(list)`, `>(list)`

- The process list is run asynchronously (subshell)
- The process list is run asynchronously, and its input or output appears as a filename.
- Note that no space may appear between the < or > and the left parenthesis

## Word Splitting

Occurs in Bash. **zsh doesn’t do word splitting on variable expansions.**. In zsh `setopt shwordsplit` will set it on and `unsetopt shword split` or `setopt no_shwordsplit` will set it off again.  
The first point to note is that word splitting on variable expansions is rarely useful. It allows, for example, a list of filenames to be separated when passed to
a command, but arrays now serve this purpose. Word splitting also allows a command to be separated from its parameters when they are stored together in a variable. For example

```bash
PAGER=less -s
nroff -man zshall.1 | $PAGER
## in zsh it wont work unless shwordsplit is set
```

- The shell scans the results of parameter expansion, command substitution, and arithmetic expansion that did **not occur within double quotes** for word splitting.
- The shell treats each character of `$IFS` as a delimiter, and splits the results of the other expansions into words using these characters as field terminators.

**Looking at `$IFS`**

```Shell
echo -n "$IFS" | hexdump -c
printf %q "$IFS"
```

**Example**: A script that will show us the arguments as passed by the shell

```Shell
#!/bin/sh -
# args
printf "%d args:" "$#"
printf " <%s>" "$@"
echo

#-----#
args hello world "how are you?"
# output:
3 args: <hello> <world> <how are you?>

# if we unquote the $@:
3 args: <hello> <world> <how> <are> <you?>
# $# still says 3 args, but after word expansion there are 5 words
# because word expansion occurs after parameter expansion.
```

- If `IFS` is not set, then it will be performed as if IFS contained a space, a tab, and a newline

```Shell
var="This is a variable"
args $var
4 args: <This> <is> <a> <variable>

#-----#

log=/var/log/qmail/current IFS=/
args $log
5 args: <> <var> <log> <qmail> <current>
unset IFS
```

_ if `IFS` is default or it contains `whitespace`, the withespace, tab and newlines are start or end will be ignored (removed), but if `IFS` is anything else (like the example above), delimiters at starts and ends will be considered.

### Controlling Word Splitting

- we usually do not want to let word splitting occur when filenames are involved
- **Double quoting** an expansion suppresses word splitting, except in the special cases of `"$@"` and `"${array[@]}"`
- `"$@"` causes each positional parameter to be expanded to a separate word; its array equivalent likewise causes each element of the array to be expanded to a separate word.

```shell
var="This is a variable"; args "$var"
1 args: <This is a variable>

array=(testing, testing, "1 2 3"); args "${array[@]}"
3 args: <testing,> <testing,> <1 2 3>
```

- **sequences of non-whitespace characters**: If IFS contains non-whitespace characters, then empty words can be generated:

```Shell
getent passwd sshd
sshd:x:100:65534::/var/run/sshd:/usr/sbin/nologin

IFS=:; args $(getent passwd sshd)
7 args: <sshd> <x> <100> <65534> <> </var/run/sshd> </usr/sbin/nologin>
griffon:~$ unset IFS
```

- non-whitespace IFS characters are not ignored at the beginning and end of expansions, the way whitespace IFS characters are.
- Whitespace IFS characters get consolidated. Multiple spaces in a row, for example, have the same effect as a single space, when IFS contains a space (or is not set at all).
- **Newlines also count as whitespace** for this purpose, which has important ramifications when attempting to load an array with lines of input.

- Word splitting is **not performed** on expansions inside Bash keywords such as `[[ ... ]]` and `case`.
- Word splitting is **not performed** on expansions in **assignments**. Thus, one does not need to quote anything in a command like these: `foo=$bar; bar=$(a command); logfile=$logdir/foo-$(date +%Y%m%d); PATH=/usr/local/bin:$PATH ./myscript`
- In either case, quoting anyway will not break anything. **So if in doubt, quote!**
- When using the `read` command (which you generally don't want to use without `-r`), word splitting is performed on the input. That's generally wanted when read `-a` is used (to populate an array) or when passing more than one variable to read. Some amount of word splitting is still done when only one variable is given in that leading and trailing IFS whitespace characters are stripped and one trailing non-whitespace IFS character (possibly surrounded by IFS whitespace characters) would be stripped if there was no other separator in the input (like in `IFS=": " read -r var <<< "input : "; echo $var` [# input]). Quoting is irrelevant here, though this behavior can be disabled by emptying IFS, typically with `IFS= read -r var`

## Filename Expansion: (Filename generation in  zsh,Path expansion or Globbing)

in Bas, after word splitting, Bash scans each word for the characters `*`, `?` and `[`. If one of these characters appears, then the word is regarded as a pattern, and replaced with an alphabetically sorted list of filenames matching the pattern. If no matching filenames are found, and the shell option `nullglob` is disabled, the word is left unchanged. If the `nullglob` option is set, and no matches are found, the word is removed. If the `failglob` shell option is set, and no matches are found, an error message is printed and the command is not executed.

### Pattern Matching

In **Bash** and **zsh**, followings are **basic patterns**.
Bash: The `nul` character may not occur in a pattern. Quting prevents special characters from being interpreted.

- `*`: Matches any string, including the null string. When the `globstar` shell option is enabled, `**` is recrusive (files directories and subdirectories).
- `?`: match any single character
- `[...]`: Matches any one of the enclosed characters.
- `[A-Z]`, `[a-dx-z]`: range
- `[^A]` or `[!A]`: anything except A
- `[class]`: Any single character from class
- `**/`: in zsh, directories to any level

In **Bash**, the followings are  `extglob` shell option (shopt builtin). In `zsh`, the same patterns are `KSH_GLOB` options (`setopt KSH_GLOB`) several extended pattern matching operators are recognized. pattern-list is a list of one or more patterns separated by a `|`.

- `?(pattern-list)`: zero or one
- `*(pattern-list)`: zero or more
- `+(pattern-list)`: one or more
- `@(pattern-list)`: one of the given patterns
- `!(pattern-list)`: anything execpt one of the given patterns
  
**zsh EXTENDED_GLOB:**

- `^pat`: anything that doesnot match pat.
- `pat1^pat1`: match pat1 then anything other than pat2
- `pat1~pat2`: anyhting matching pat1 but not pat2.
- `X#`: Zero or more occurrences of element X
- `X##`: One or more occurrences of element X

**Globbing flags with zsh EXTENDED_GLOB:**

There are various flags which affect any text to their right up to the end of the enclosing group or to the end of the pattern; they require the EXTENDED_GLOB option. All take the form (#X)

- `(#qexpr)`: expr is a set of glob qualifies (see below)
- `(#b)(pattern)`: set match, mbegin, mend arrays for pattern inside (). **Not used in filename expansiom.**
- `(#m)(pattern)`: like (#b) but set references to the match data for the entire string matched.
  
```zsh
foo="a_string_with_a_message"
if [[ $foo = (a|an)_(#b)(*) ]]; then
  print ${foo[$mbegin[1],$mend[1]]}
fi
# out: string_with_a_message
# $mbegin is 3
# $mend is 23
# emd $match is string_with_a_message

#forces all the matches (i.e. all vowels) into uppercase, printing ‘vEldt jynx grImps wAqf zhO bUck’.
arr=(veldt jynx grimps waqf zho buck)
print ${arr//(#m)[aeiou]/${(U)MATCH}}
```

**zsh Glob qualifiers**
Glob qualifiers (in parentheses after file name pattern)

```zsh
ls  # test.sh test.txt
echo test*(#qon) #test.sh test.txt
echo test*(#qol) # test.txt test.sh
echo test*(#q[1]) # test.sh
echo test*(#q[1,2]:r) # :r is a modifier(test test)
```

## File Generation (Globbing, filename  expansion): zsh

### Basic patterns

- `(pat1)`: group patterns
- `(pat1|pat2)`: pat1 or pat2 (any number of |’s)

## Quote Removal

After the preceding expansions, all unquoted occurrences of the characters `\`, `''`, and `"` that did not result from one of the above expansions are removed.

# Redirection

Note that the order of redirections is significant. `ls > dirlist 2>&1` directs both standard output (file descriptor 1) and standard error (file descriptor 2) to the file dirlist, while the command `ls 2>&1 > dirlist` directs only the standard output to file dirlist, because the standard error was made a copy of the standard output before the standard output was redirected to dirlist.

- Redirecting Input `[n]<word`
Redirection of input causes the file whose name results from the expansion of word to be opened for reading on file descriptor n, or the standard input (file descriptor 0) if n is not specified.
- Redirecting Output `[n]>[|]word`
If the redirection operator is `>|`: the redirection is attempted even if the file named by word exists
- Appending Redirected Output `[n]>>word`
- Redirecting Standard Output and Standard Error
  - `&>word`: preffered
  - `>&word`
- Appending Standard Output and Standard Error `&>>word`

## Here Documents `<<delimiter`, `<<-delimiter`

This type of redirection instructs the shell to read input from the current source until a line containing only word (with no trailing blanks) is seen.

  ```Shell
  [n]<<[−]word
      here-document
  delimiter
  ```

No parameter and variable expansion, command substitution, arithmetic expansion, or filename expansion is performed on word.
If the redirection operator is `<<-`, then all leading tab characters are stripped from input lines and the line containing delimiter. This allows here-documents within shell scripts to be indented in a natural fashion.

A block of code or text which can be redirected to the command script or interactive program is called here document or HereDoc. So when the coder needs less amount of text data, then using code and data in the same file is a better option and it can be done easily by using here documents in a script.

```Shell
Command << HeredocDelimiter
. . .
. . .
HeredocDelimiter
```

**ecample**

```Shell
#!/bin/bash
#example.sh
cat <<ADDTEXT
This text is
added by Here Document
ADDTEXT
```

if we run the script using `bash example.sh`, the code will be executed anfd the text is echoed.\

HereDoc uses ‘–‘ symbol to suppress any tab space from each line of heredoc text. When the script executes then all tab spaces are omitted from the starting of each line but it creates no effect on normal space.

```Shell
#!/bin/bash
cat <<-ADDTEXT2
  Line-1: Here Document is helpful to print short text
  Line-2: Here Document can be used to format text
  Line-3: Here Document can print variable within the text
  Line-4: Here Document with '-' removes tab space from the line
ADDTEXT2
```

Tabs will not be printed in the output.

## Here Strings `<<<`

A variant of here documents, the format is: `[n]<<< word`\

- The word undergoes tilde expansion, parameter and variable expansion, command sub-stitution, arithmetic expansion, and quote removal.
- **Pathname expansion** and **word splitting** are not performed.
- The result is supplied as a single string, with a newline appended, to the command on its standard input

```Shell
var=$'hello world\nice cream\n\nhello beautiful\nhello viewwers!!!'
echo "${var}" | grep 'hello' # this will run in subshell
grep 'hello' "$var"  # doesnot work, $var will not expand

grep 'hello' <<< "$var"
grep 'hello' <<< "$(< data)"
grep 'hello' <<< "$(cat data)"
```

# `mapfile` or `readarray`

mapfile is a builtin command of the Bash shell. It reads lines from standard input into an indexed array variable. `readarray` is an alias to `mapfile`. These commands are not quite portable and the same purpose can be achived by `read` loop. However, mapfile is  faster.

```Shell
mapfile [-n count] [-O origin] [-s count] [-t] [-u fd]
        [-C callback [-c quantum]] [array]
```

# `read`

By default, read considers a newline character as the end of a line, but this can be changed using the `-d` option.
After reading, the line is split into words according to the value of the special shell variable **IFS**, the internal field separator.
To preserve white space at the beginning or the end of a line, it's common to specify `IFS=` (with no value) immediately before the read command. After reading is completed, the IFS returns to its previous value. **By default, the IFS value is "space, tab, or newline".**

read assigns the first word it finds to name, the second word to name2, etc. If there are more words than names, all remaining words are assigned to the last name specified. If only a single name is specified, the entire line is assigned to that variable. If no name is specified, the input is stored in a variable named **REPLY**.

## Syntax

```shell
read [-ers] [-a array] [-d delim] [-i text] [-n nchars] [-N nchars]
     [-p prompt] [-t timeout] [-u fd] [name ...] [name2 ...]
```

## options

- `-a array` Store the words in an indexed array named array. Numbering of array elements starts at zero.
- `-d delim` Set the delimiter character to delim. This character signals the end of the line. If -d is not used, the default line delimiter is a newline.
- `-e` Get a line of input from an interactive shell. The user manually inputs characters until the line delimiter is reached.
- `-i text` When used in conjunction with -e (and only if -s is not used), text is inserted as the initial text of the input line. The user is permitted to edit text on the input line.
- `-n nchars` Stop reading after an integer number nchars characters have been read, if the line delimiter has not been reached.
- `-N nchars` Ignore the line delimiter. Stop reading only after nchars characters have been read, EOF is reached, or read times out (see -t).
- `-p prompt` Print the string prompt, without a newline, before beginning to read.
- `-r` Use "raw input". Specifically, this option causes read to interpret backslashes literally, rather than interpreting them as escape characters.
- `-s` Do not echo keystrokes when read is taking input from the terminal.
- `-t timeout` Time out, and return failure, if a complete line of input is not read within timeout seconds. If the timeout value is zero, read will not read any data, but returns success if input was available to read. If timeout is not specified, the value of the shell variable TMOUT is used instead, if it exists. The value of timeout can be a fractional number, e.g., 3.5.
- `-u fd` Read from the file descriptor fd instead of standard input. The file descriptor should be a small integer. For information about opening a custom file descriptor, see opening file descriptors in bash.

## Examples

```Shell
while read; do echo "$REPLY"; done
```

read takes data from the **terminal**. Type whatever you'd like, and press Enter. The text is echoed on the next line. This loop continues until you press **Ctrl+D (EOF)** on a new line. Because no variable names were specified, the entire line of text is stored in the variable **REPLY**.

```Shell
while read text; do echo "$text"; done
```

Same as above, using the variable name text.

```Shell
while read -ep "Type something: " -i "My text is " text; do
  echo "$text";
done
```

Provides a prompt, and initial text for user input. The user can erase "My text is ", or leave it as part of the input. Typing Ctrl+D on a new line terminates the loop.

```shell
echo "Hello, world!" | (read; echo "$REPLY")
```

Enclosing the read and echo commands in parentheses executes them in a dedicated subshell. **This allows the REPLY variable to be accessed by both read and echo**. But this will not work `echo "Hello, world!" | read | echo "$REPLY"`

```Shell
echo "one two three four" | while read word1 word2 word3; do
  echo "word1: $word1"
  echo "word2: $word2"
  echo "word3: $word3"
done
```

word1: one
word2: two
word3: three four

```Shell
echo "one two three four" | while read -a wordarray; do
  echo ${wordarray[1]}
done```
two

```shell
while IFS= read -r -d $'\0' file; do
  echo "$file"
done < <(find . -print0)
```

The above commands are **the "proper" way to use find and read together to process files**. It's especially useful when you want to do something to a lot of files that have odd or unusual names. Let's take a look at specific parts of the above example:

`while IFS=`. IFS= (with nothing after the equals sign) sets the internal field separator to "no value". Spaces, tabs, and newlines are therefore considered part of a word, which preserves white space in the file names.
Note that IFS= comes after while, ensuring that IFS is altered only inside the while loop. For instance, it won't affect find.

`read -r`: Using the -r option is necessary to preserve any backslashes in the file names.

`-d $'\0'`: The -d option sets the newline delimiter. We're using NULL as the line delimiter because Linux file names can contain newlines, so we need to preserve them. (This sounds awful, but yes, it happens.)
However, a NULL can never be part of a Linux file name, so that's a reliable delimiter to use. Luckily, find can use NULL to delimit its results, rather than a newline, if the -print0 option is specified:

`< <(find . -print0)`: Here, find . -print0 creates a list of every file in and under . (the working directory) and delimit all file names with a NULL. When using -print0, all other arguments and options must precede it, so make sure it's the last part of the command.

Enclosing the find command in <( ... ) performs process substitution: the output of the command can be read like a file. In turn, this output is redirected to the while loop using the first "<".

For every iteration of the while loop, read reads one word (a single file name) and puts that value into the variable file, which we specified as the last argument of the read command.

When there are no more file names to read, read returns false, which triggers the end of the while loop, and the command sequence terminates.

# `test` command (`[]`)

test provides no output, but returns an exit status of 0 for "true" (test successful) and 1 for "false" (test failed).
**Examples**

```shell
num=4; if (test $num -gt 5); then echo "yes"; else echo "no"; fi  # no
file="/etc/passwd"; if [ -e $file ]; then echo "whew"; else echo "uh-oh"; fi
```

## Syntax

### File tests

```shell
test [-a] [-b] [-c] [-d] [-e] [-f] [-g] [-h] [-L] [-k] [-p] [-r] [-s] [-S]
     [-u] [-w] [-x] [-O] [-G] [-N] [file]
```

```shell
    test -t fd
```

```shell
    test file1 {-nt | -ot | -ef} file2
```

### String tests

```shell
test [-n | -z] string
```

```shell
test string1 {= | != | < | >} string2
```

### Shell options and variables

```shell
test -o option
```

```shell
test {-v | -R} var
```

### Simple logic (test if values are null)

```shell
test [!] expr

test expr1 {-a | -o} expr2
```

### Numerical comparisons

for integer values only; bash doesn't do floating point math

```Shell
test arg1 {-eq | -ne | -lt | -le | -gt | -ge} arg2
```

## Options

- `a file` Returns true if **file exists**. Does the same thing as -e. Both are included for compatibility reasons with legacy versions of Unix.
- `b file` Returns true if file is **"block-special"**. Block-special files are similar to regular files, but are stored on block devices — special areas on the storage device that are written or read one block (sector) at a time.
- `c file` Returns true if file is **"character-special."** Character-special files are written or read byte-by-byte (one character at a time), immediately, to a special device. For example, /dev/urandom is a character-special file.
- `d file` Returns true if file is a **directory**.
- `e file` Returns true if **file exists**. Does the same thing as -a. Both are included for compatibility reasons with legacy versions of Unix.
- `f file` Returns true if **file exists**, and is a regular file.
- `g file` Returns true if file has the setgid bit set.
-`h file` Returns true if file is a **symbolic link**. Does the same thing as **-L**. Both are included for compatibility reasons with legacy versions of Unix.
- `-k file` Returns true if file has its sticky bit set.
- `-p file` Returns true if the file is a **named pipe**, e.g., as created with the command `mkfifo`.
- `-r file` Returns true if file is readable by the user running test.
- `-s file` Returns true if **file exists, and is not empty**.
- `-S file` Returns true if file is a socket.
- `-t fd` Returns true if file descriptor fd is opened on a terminal.
- `-u file` Returns true if file has the setuid bit set.
- `-w file` Returns true if the user running test has **write permission to file**, i.e. make changes to it.
- `-x file` Returns true if file is **executable by the user** running test.
- `-O file` Returns true if file is **owned by the user** running test.
- `-G file` Returns true if file is **owned by the group** of the user running test.
- `-N file` Returns true if file was **modified** since the last time it was read.
- `file1 -nt file2` Returns true if file1 is **newer** (has a newer modification date/time) than file2.
- `file1 -ot file2` Returns true if file1 is **older** (has an older modification date/time) than file2.
- `file1 -ef file2` Returns true if file1 is a **hard link** to file2.
- `test [-n] string` Returns true if **string is not empty**. Operates the same with or without -n.
For example, if mystr="", then test "$mystr" and test -n "$mystr" would both be false. If mystr="Not empty", then test "$mystr" and test -n "$mystr" would both be true.
- `-z string` Returns true if string *string* is **empty**, i.e. "".
- `string1 = string2` Returns true if string1 and string2 are equal, i.e. contain the same characters.
- `string1 != string2` Returns true if string1 and string2 are not equal.
- `string1 < string2` Returns true if string1 sorts before string2 lexicographically, according to ASCII numbering, based on the first character of the string. For instance, test "Apple" < "Banana" is true, but test "Apple" < "banana" is false, because all lowercase letters have a lower ASCII number than their uppercase counterparts.

  **Tip:** Enclose any variable names in double quotes to protect whitespace. Also, escape the less than symbol with a backslash to prevent bash from interpreting it as a redirection operator. For instance, use test "$str1" \< "$str2" instead of test $str1 < $str2. The latter command will try to read from a file whose name is the value of variable str2. For more information, see redirection in bash.
- `string1 > string2` Returns true if string1 sorts after string2 lexicographically, according to the ASCII numbering. As noted above, use test "$str1" \> "$str2" instead of test $str1 > $str2. The latter command creates or overwrites a file whose name is the value of variable str2.
- -`o option` Returns true if the **shell option** opt is enabled.
- `-v var` Returns true if the **shell variable** var is set.
- -`R var` Returns true if the **shell variable** var is set, and is a name reference. (It's possible this refers to an indirect reference, as described in Parameter expansion in bash.)
- `! expr` Returns true if and only if the expression expr is null.
- `expr1 -a expr2` Returns true if expressions **expr1 and expr2 are both not null**.
- `expr1 -o expr2` Returns true if **either of the expressions expr1 or expr2 are not null**.
- `arg1 -ne arg2` true if argument arg1 is **not equal to** arg2.
- `arg1 -lt arg2` true if numeric value arg1 is **less than** arg2.
- `arg1 -le arg2` true if numeric value arg1 is **less than or equal** to arg2.
- `arg1 -gt arg2` true if numeric value arg1 is **greater than** arg2.
- `arg1 -ge arg2` true if numeric value arg1 is **greater than or equal** to arg2

## examples

```Shell
if test false; then
  echo "Test always returns true for only one argument, unless it is null.";
fi
# Test always returns true for only one argument, unless it is null.

[ false ]; echo $? # 0
[ true ]; echo $? # 0

# works for files and directories
test -e /sys; echo $?   # true if file or directory exists
# for directories
if test -d /home; then echo "/home is a directory"; fi
# for regular files (i.e not directories)
test -f /etc/shadow; echo $?   # true for regular files

touch newfile; if [ -O newfile ]; then echo "I own newfile"; fi
str=""; test -z "$str"; echo $?   # true if variable str is empty string ""
str=""; test -n "$str"; echo $?   # true if variable str is not empty string ""
str=""; test "$str"; echo $?      # same as above command; -n is optional
str="Not empty"; test -z "$str"; echo $?
```

## Combine multiple test conditions

```Shell
[ $x -gt 5 ] && [ $x -lt 8 ] && [ $x -ne 6 ]
[ $x -gt 5 ] || [ $x -lt 8 ]
```

```Shell
[ $x -gt 5 -a $x -lt 8 ] # -a  for AND
[ $x -gt 5 -o $x -lt 8 ] # -o for OR
```

```Shell
[[ $x -gt 5 ]] && [[ $x -lt 8 ]] && [[ $x -ne 6 ]]

[[ $x -gt 5 && $x -lt 8 && $x -ne 6 ]]
```

# Parsing bash script options with `getopts`

There ian alder but robust alternative to `getopts`, naming `getopts`. See <https://mariusvw.com/2013/02/24/bash-getopt-versus-getopts/> for a comparison.  
This tutorial explains how to use the `getopts` built-in function to parse arguments and options to a bash script.
The getopts function takes three parameters:

1. The first is a specification of **which options are valid**, listed as a sequence of letters. For example, the string `'ht'` signifies that the options -h and -t are valid.
2. The second argument to getopts is a **variable** that will be populated with the option or argument to be processed next. In the following loop, `opt` will hold the value of the current option that has been parsed by `getopts`.

  ```shell
  while getopts ":ht" opt; do
    case ${opt} in
      h ) # process option a
        ;;
      t ) # process option t
        ;;
      \? ) echo "Usage: cmd [-h] [-t]"
      ;;
    esac
  done
  ```

- if an invalid option is provided, the option variable is assigned the value `?`.
- Second, this behaviour is only true when you prepend the list of valid options with `:` to disable the default error handling of invalid options. It is **recommended** to always disable the default error handling in your scripts.
- The third argument to getopts is the list of arguments and options to be processed. When not provided, this defaults to the arguments and options provided to the application (`$@`)

## Shifting processed options

The variable `OPTIND` holds the number of options parsed by the last call to getopts. It is common practice to call the shift command at the end of your processing loop to remove options that have already been handled from $@ by:

```shell
shift $((OPTIND -1))
```

## Parsing options with arguments

Options that themselves have arguments are signified with a `:`. The argument to an option is placed in the variable `OPTARG`. In the following example, the option `t` takes an argument. When the argument is provided, we copy its value to the variable target. If no argument is provided getopts will set opt to `:`. We can recognize this error condition by catching the : case and printing an appropriate error message.

## An example

check that -s exists, if not return error; check that the value after the -s is 45 or 90; check that the -p exists and there is an input string after; if the user enters ./myscript -h or just ./myscript then display help.

```Shell
#!/usr/bin/env bash
usage() { echo "$0 usage:" && grep " .)\ #" $0; exit 0; }
[ $# -eq 0 ] && usage
while getopts ":hs:p:" arg; do
  case $arg in
    p) # Specify p value.
      echo "p is ${OPTARG}"
      ;;
    s) # Specify strength, either 45 or 90.
      strength=${OPTARG}
      [ $strength -eq 45 -o $strength -eq 90 ] \
        && echo "Strength is $strength." \
        || echo "Strength needs to be either 45 or 90, $strength found instead."
      ;;
    h | *) # Display help.
      usage
      exit 0
      ;;
  esac
done
```

## Another getopts example

based on a condition script would need mandatory option. For example, if argument to script is a directory, I will need to specify -R or -r option along with any other options (myscript -iR mydir or myscript -ir mydir or myscript -i -r mydir or myscript -i -R mydir), in case of file only -i is sufficient (myscript -i myfile).
You can concatenate the options you provide and getopts will separate them. You can set a flag when options are seen and check to make sure mandatory "options" (!) are present after the getopts loop has completed.

```shell
#!/bin/bash
rflag=false
small_r=false
big_r=false

usage () { echo "How to use"; }

options=':ij:rRvh'
while getopts $options option
do
    case "$option" in
        i  ) i_func;;
        j  ) j_arg=$OPTARG;;
        r  ) rflag=true; small_r=true;;
        R  ) rflag=true; big_r=true;;
        v  ) v_func; other_func;;
        h  ) usage; exit;;
        \? ) echo "Unknown option: -$OPTARG" >&2; exit 1;;
        :  ) echo "Missing option argument for -$OPTARG" >&2; exit 1;;
        *  ) echo "Unimplemented option: -$OPTARG" >&2; exit 1;;
    esac
done

if ((OPTIND == 1))
then
    echo "No options specified"
fi

shift $((OPTIND - 1))

if (($# == 0))
then
    echo "No positional arguments specified"
fi

if ! $rflag && [[ -d $1 ]]
then
    echo "-r or -R must be included when a directory is specified" >&2
    exit 1
fi
```

# `tee`: Redirect output to multiple files or processes

The tee command copies standard input to standard output and also to any files given as arguments. This is useful when you want not only to send some data down a pipe, but also to save a copy.

- The tee command is useful when you happen to be transferring a large amount of data and also want to summarize that data without reading it a second time.
  - you are downloading a DVD image, you often want to verify its signature or checksum right away. The **inefficient way** to do it is simply: `wget https://example.com/some.iso && sha1sum some.iso`. it makes you wait for the download to complete before starting the time-consuming SHA1 computation and reads DVD two times.
  - The efficient way: `get -O - https://example.com/dvd.iso | tee >(sha1sum > dvd.sha1) > dvd.iso`. That makes tee write not just to the expected output file, but also to a pipe running sha1sum and saving the final checksum in a file named dvd.sha1.
  - Note also that if any of the process substitutions (or piped stdout) might exit early without consuming all the data, the `-p` option is needed to allow tee to continue to process the input to any remaining outputs.
  - A more conventional and portable use of `tee`: `wget -O - https://example.com/dvd.iso | tee dvd.iso | sha1sum > dvd.sha1`
- You can extend this example to **make tee write to two processes**:

    ```Shell
    wget -O - https://example.com/dvd.iso \
    | tee >(sha1sum > dvd.sha1) \
          >(md5sum > dvd.md5) \
    > dvd.iso
      ```

      another example

    ```Shell
    paste <(cut -f1 file1) <(cut -f3 file2) |
     tee >(process1) >(process2) >/dev/null
    ```

- This technique is also useful when you want to make a compressed copy of the contents of a pipe. For a
large hierarchy, ‘du -ak’ can run for a long time, and can easily produce terabytes of data, so you won’t want to rerun the command unnecessarily. Nor will you want to save the uncompressed output.
  - Inefficient way:

    ```shell
    du -ak | gzip -9 > /tmp/du.gz
    gzip -d /tmp/du.gz | xdiskusage -a
      ```

  - Efficient way:

    ```shell
    du -ak | tee >(gzip -9 > /tmp/du.gz) | xdiskusage -a
    ```

- Another example
-

  ```Shell
  tardir=your-pkg-M.N
  tar chof - "$tardir" \
  | tee >(gzip -9 -c > your-pkg-M.N.tar.gz) \
  | bzip2 -9 -c > your-pkg-M.N.tar.bz2
  ```

- If you want to further process the output from process substitutions, and those processes write atomically (i.e., write less than the system’s PIPE BUF size at a time), that’s possible with a construct like:
-

  ```shell
  tardir=your-pkg-M.N
  tar chof - "$tardir" \
    | tee >(md5sum --tag) > >(sha256sum --tag) \
    | sort | gpg --clearsign > your-pkg-M.N.tar.sig
  ```

# Other notes

- `set -o errexit` or `set -e` in a script: `-e` Exit immediately if a pipeline, which may consist of a single simple command, a subshell command enclosed in parentheses, or one of the commands executed as part of a command list enclosed by braces returns a non-zero status. **The shell does not exit if** the command that fails is part of the command list immediately following a while or until keyword, part of the test in an if statement, part of any command executed in a && or || list except the command following the final && or ||, any command in a pipeline but the last, or if the command’s return status is being inverted with !. Err exit doesnot work inside functions, but find a solution at this **[link][b4bf5877].**

# `split` and `cssplit` functions

## `split`

`split` creates output files containing consecutive or interleaved sections of INPUT (standard input if none is given or INPUT is ‘-’).
  
**Synopsis:** `split [OPTION] [INPUT [PREFIX]]`  
  
By default, ‘split’ puts 1000 lines of INPUT (or whatever is left
over for the last section), into each output file.  
The output files’ names consist of PREFIX (‘x’ by default) followed
by a group of characters **(‘aa’, ‘ab’, ... by default)**, such that
concatenating the output files in traditional sorted order by file name
produces the original input file (except ‘-nr/N’).  By default split
will initially create files with two generated suffix characters, and
will increase this width by two when the next most significant position
reaches the last character.  (‘yz’, ‘zaaa’, ‘zaab’, ...).

- `--filter=COMMAND`
     With this option, rather than simply writing to each output file,
     write through a pipe to the specified shell COMMAND for each output
     file.  COMMAND should use the $FILE environment variable, which is
     set to a different output file name for each invocation of the
     command.  

     **spliting a large compressedf file:**
     For example, imagine that you have a 1TiB compressed file
     that, if uncompressed, would be too large to reside on disk, yet
     you must split it into individually-compressed pieces of a more
     manageable size.  To do that, you might run this command:

     ```zsh
     xz -dc BIG.xz | split -b200G --filter='xz > $FILE.xz' - big-
      #  Assuming a 10:1 compression ratio, that would create about fifty
      # 20GiB files with names ‘big-aa.xz’, ‘big-ab.xz’, ‘big-ac.xz’, etc.
     ```

### examples

Split the file newfile.txt into files beginning with the name new, each containing 300 lines of text.

```zsh
split -l 300 file.txt new
```

split the file newfile.txt into three separate files called newaa, newab and newac..., with each file containing 10MB of data.

```zsh
split -b 10MB newfile.txt new

# Split file into N equal-sized chunks
# This may split files halfway through a line
split largefile.txt -n 10

# Split files into N equal-sized chunks without splitting lines
# Use l/N where N is the number of parts you want. This will only split files at line endings.
# For example, divide a large file into 10 parts, each having 1/10 of the lines
split largefile.txt -n l/10

# Rebuild a file from its parts
split largefile.txt
ls  # xaa xab xac
cat ./x* > rebuiltfile.txt
```

## `csplit`

splits based on content. **csplit** outputs pieces of FILE separated by PATTERN(s) to files 'xx00', 'xx01', ..., and output byte counts of each piece to standard output.  
  
**Synopsis:**  csplit [OPTION]... FILE PATTERN...  
  
Each **pattern** may be:

- `INTEGER` copy up to but not including specified line number.
  
- `/REGEXP/[OFFSET]` copy up to but not including a matching line.
  
- `%REGEXP%[OFFSET]` skip to, but not including a matching line.
  
- `{INTEGER}` repeat the previous pattern specified number of times.
  
- `{*}` repeat the previous pattern as many times as possible.

- A line OFFSET is a required '+' or '-' followed by a positive integer.

### examples

```zsh
# Creates four files, cobol00...cobol03.
csplit -f cobol filename '/procedure division/' /par5./ /par16./

# After editing the split files, they can be recombined into filename using the cat
cat cobol0[0-3] > filename
```

# `body() function in NGSeasy`

<https://unix.stackexchange.com/questions/11856/sort-but-keep-header-line-at-the-top/11859#11859>

# bioawk

# Parallel

# xargs

```zsh
# fastqc can read gz files. zcat is only used fo demonstration of pipe with xargs
# in zsh it is not neccasary to use -print0 in find and -0 in xargs (no fine name exansion occures in zsh)
find . -type f -name "*.fastq.gz$" -print0 | xargs -0 -I {} sh -c "zcat {} | fastqc -o ./outDir {}"

find . -type f -name "*.fastq.gz$" -print0 | xargs -0 -I {} sh -c "zcat {} | fastqc -o ./outDir {}"

# to find the parent process ID (PPID) of the jobs created (fastqc is run using java)
pstree -p | grep java
# zsh(3804)---xargs(8488)---java(8489)-+-{java}(8490)
                                       |-{java}(8491)

# -p: max number of parallel processes (0 for all capacity). -n: for max number of argument for each process.
# -p 8: is relatively fast
# -p 8 -n10: extremly fast (uses all  8 cores)
find . -type f -name "*.fastq.gz$" -print0 | xargs -0 -P 8 -n 1 -I {} sh -c "zcat {} | fastqc -o ./outDir {}"
```

# GNU Parallel

## Trivially parallelizable jobs

Some analyses take a long time because it is running on a single processor and there is a lot of data that needs processing. A problem is consider trivially parallelizable if the data can be chunked into pieces and processed separately.

**Examples of data that can be trivially parallelized include:**

- When each line of a file can be processed individually
- Each chromosome of a genome can be processed individually
- Each scaffold of an assembly can be processed individually

**Examples of problems that are trivially parallelizable**

- Zipping or unzipping 10s to 100s of files
- Counting the number of lines in a large file
- Aligning raw sequencing data files of many samples to a genome

**Examples of problems that are not trivially parallelizable**

Genome assembly is not trivially parallelizable because the first step requires alignment of each read to each other read in order to find which ones are similar and should be joined (assembled). Take a subset of the reads would result in a bunch of small poor assemblies.

## installation

```zsh
(wget -O - pi.dk/3 || curl pi.dk/3/ || fetch -o - http://pi.dk/3) | bash
sudo apt-get install parallel
```

## run multiple commands in parallel

Prepare a script with a list of seperate commands to be executed in parallel, one command per line and then run:

```zsh
parallel --jobs 6 < jobs2run

cat jobs2run
 # bzip2 oldstuff.tar
 # oggenc music.flac
 # opusenc ambiance.wav
 # convert bigfile.tiff small.jpeg
 # ffmepg -i foo.avi -v:b 12000k foo.mp4
 # xsltproc --output build/tmp.fo style/dm.xsl src/tmp.xml
 # bzip2 archive.tar
```

## basic examples

```zsh
# -j: number of jobs
seq 1 5 | parallel -j 4 echo

#the --dry-run flag which prints the commands to be run
seq 1 5 | parallel --dry-run -j 4 echo
echo 3
echo 4
echo 5
echo 2
echo 1
```
The results are out of order! This is due to the nature of parallelization. Not all “jobs” initiate or take the same amount of time so it is common to observe outputs in a different order. We can enforce a “first in first out” result set by using the `-k` flag. Use ::: for args.

```zsh
seq 1 5 | parallel -j 4 -k echo
seq 1 5 | parallel -j 4 -k echo > out.txt  # set output to a file
parallel -j 100% # Uses 100% of cores.
parallel -j -1 # Uses 1 less than the total number of cores.
parallel -j +1 # Parallelize across the number of cores + 1
```

Use `:::` to specify arguments derived from commands or lists. There are limits to the number of arguments you can provide with a process substitution as shown below. In these instances, it may be better to pipe arguments or use a file (below) rather than supply them with a process substitution.  
Use `::::` for args within files. For large argument lists you can specify a file with a list of arguments. Specify a file of arguments (one per line) using `::::`

```zsh
parallel -j 4 -k echo ::: `seq 1 5`
seq 1 5 | parallel -j 4 -k echo
parallel -j 4 -k echo :::: my_args.txt
```

Use `{}`: by default, parallel assumes the arguments are placed at the end of the input command, but you can explicitly define where arguments are substituted using `{}`. Notice that we are having to escape quotes - there are ways around this.

```zsh
parallel --dry-run -j 4 -k echo \"{} "<-- a number"\" ::: `seq 1 5`
# output:
echo "1 <-- a number"
echo "2 <-- a number"
echo "3 <-- a number"
echo "4 <-- a number"
echo "5 <-- a number"
```

## Combinatorials

You can keep adding ::: and :::: to add additional arguments, and these will be combined to generate all possible combinations. This is extremely useful for testing commands with different combinations of input parameters.

```zsh
parallel --dry-run -k -j 4 Rscript run_analysis.R {1} {2} ::: `seq 1 2` ::: A B C
# output
Rscript run_analysis.R 1 A
Rscript run_analysis.R 1 B
Rscript run_analysis.R 1 C
Rscript run_analysis.R 2 A
Rscript run_analysis.R 2 B
Rscript run_analysis.R 2 C
```

## Parallelize Functions

In some cases, you want to perform a series of commands. For example, the code below compute the number of ATCGs of the complement of a DNA sequence.
  
one-liner form:  

```zsh
echo "ATTA" |  tr ATCG TAGC | \
    python -c "import sys; o=sys.stdin.read().strip(); print(o, o.count('T'), o.count('G'), o.count('C'), o.count('A'))"
```

This command has two operations. While is it possible to incorporate this into a ‘one-liner’, it is far easier to create a bash function, export it, and use that as input.  

```zsh
function count_nts {
    # $1 is the first argument passed to the function
    echo $1 | tr ATCG TAGC | \
    python -c "import sys; o=sys.stdin.read().strip(); print(o, o.count('T'), o.count('G'), o.count('C'), o.count('A'))"
}

# Use the `-f` flag to export functions
export -f count_nts

parallel -j 4 count_nts ::: TAAT TTT AAAAT GCGCAT | tr ' ' '\t'
```

## `--pipe`

The `--pipe` functionality puts GNU parallel in a different mode: Instead of treating the data on stdin (standard input) as arguments for a command to run, the data will be sent to stdin (standard input) of the command.  
The typical situation is as below, where command_B is slow, and you want to speed up command_B.

```zsh
command_A | command_B | command_C
```
  
- `--block`: block size (chunnk size)**: By default GNU parallel will start an instance of command_B, read a chunk of 1 MB, and pass that to the instance. Then start another instance, read another chunk, and pass that to the second instance.

  ```zsh
  cat num1000000 | parallel --pipe wc
  # changing chunk size to 2MB
  cat num1000000 | parallel --pipe --block 2M wc
  ```

- `--roundrobin`:
  GNU parallel treats each line as a record. If the order of records is unimportant (e.g. you need all lines processed, but you do not care which is processed first), then you can use --roundrobin. Without --roundrobin GNU parallel will start a command per block; with `--roundrobin` only the requested number of jobs will be started (`--jobs`). The records will then be distributed between the running jobs
- **Record:** GNU parallel sees the input as records. The default record is a **single line**.
- `-N`: number of records. Using -N140000 GNU parallel will read 140000 records at a time
- `--pipepart` more efficient than `--pipe` but used for normal files (with `-a file` or `::::`)
- `-L`: number of lines for each record. By default each line is a record. `-L` changes this.

  ```zsh
  cat num1000000 | parallel --pipe -N140000 wc

  # If a record is 75 lines -L can be used (it passes full 75 lines at a time)
  cat num1000000 | parallel --pipe -L75 wc
  ```

- **Record separators**: GNU parallel uses separators to determine where two records split
  - `recend` gives the string that ends a record (default is `--recend '\n'`)
  - `recstart`: gives the string that starts a record
  - With `--regexp` the --recend and --recstart will be treated as a regular expression
  - remove the record separators with `--remove-rec-sep` or `--rrs`

  ```zsh
  # Here the --recend is set to ', '
  echo /foo, bar/, /baz, qux/, | parallel -kN1 --recend ', ' --pipe echo JOB{#}\;cat\;echo END

  # Output:

    JOB1
    /foo, END
    JOB2
    bar/, END
    JOB3
    /baz, END
    JOB4
    qux/,
    END

  # Here the --recstart is set to /:

  echo /foo, bar/, /baz, qux/, | \
      parallel -kN1 --recstart / --pipe \
      echo JOB{#}\;cat\;echo END

  # Output:

    JOB1
    /foo, barEND
    JOB2
    /, END
    JOB3
    /baz, quxEND
    JOB4
    /,
    END

  # With --regexp the --recend and --recstart will be treated as a regular expression:
  echo foo,bar,_baz,__qux, | \ 
      parallel -kN1 --regexp --recend ,_+ --pipe \
      echo JOB{#}\;cat\;echo END

  # Output:

    JOB1
    foo,bar,_END
    JOB2
    baz,__END
    JOB3
    qux,
    END

  echo foo,bar,_baz,__qux, | \
      parallel -kN1 --rrs --regexp --recend ,_+ --pipe \
      echo JOB{#}\;cat\;echo END

  # Output:

    JOB1
    foo,barEND
    JOB2
    bazEND
    JOB3
    qux,
    END
  ```

- `--header`: If the input data has a header, the header can be repeated for each job by matching the header with `--header`. 

  ```zsh
  # If headers start with % you can do this:
  cat num_%header | \
      parallel --header '(%.*\n)*' --pipe -N3 \
      echo JOB{#}\;cat

  # If the header is 2 lines, --header 2 will work:
  cat num_%header | parallel --header 2 --pipe -N3 echo JOB{#}\;cat
  ```

## Parallelizing existing scripts

Create scripts like the templatees below and run then like `script foo bar`.
Python:

```python
#!/usr/bin/parallel --shebang-wrap /usr/bin/python
  
  import sys
  print 'Arguments', str(sys.argv)
```

zsh (or bash):

```zsh
  #!/usr/bin/parallel --shebang-wrap /bin/zsh
  
  echo Arguments "$@"
```

R:

```R
  #!/usr/bin/parallel --shebang-wrap /usr/bin/Rscript --vanilla --slave
  
  args <- commandArgs(trailingOnly = TRUE)
  print(paste("Arguments ",args))
```

## GNU Parallel for Variant Calling

When working with BAMs or VCFs you can parallelize across chromosomes. Most variant callers or annotation tools allow you to operate on a single chromosome at a time by specifying a region. This allows us to apply a split-apply-combine strategy by splitting by chromosome, operating on each chromosome, and combining the results at the end.
  
### Split chromosomes from a BAM

```bash
chrom_list=`samtools idxstats in.bam | cut -f 1 | grep -v '*'`
# For c. elegans you can would see the following 7
# I
# II
# III
# IV
# V
# X
# MtDNA
```

We can create a function so this operation is easier going forward:

```bash
function bam_chromosomes {
    samtools idxstats $1 | cut -f 1 | grep -v '*'
}
```

### Apply an operation to each chromosome

* You must export any variables you use within a parallelized function. That is what I am doing here with the reference genome variable.
* bcftools mpileup outputs an uncompressed pileup (--output-type=u). This is done for efficiency sake - there is no reason to pipe a compressed form of data for it to need to be uncompressed by the next tool.
* Similarly, I also output an uncompressed set of variant calls ${1/.bam/}.$2.bcf because these are temporary files that we will remove later.
* Finally, I use a variable substitution to remove the extension from the bam and to generate a `<sample>.<chromsome>.bcf` filename: `${1/.bam/}.$2.bcf` → sample_A.I.bam, sample_A.II.bam, etc. This prevents filename collisions if we are calling many samples simultaneously.

```bash
genome=path/to/genome.fa
export genome # This is critical!

function parallel_call {
    bcftools mpileup \
        --fasta-ref ${genome} \
        --regions $2 \
        --output-type u \
        $1 | \
    bcftools call --multiallelic-caller \
                  --variants-only \
                  --output-type u - > ${1/.bam/}.$2.bcf
}

export -f parallel_call

chrom_set=`bam_chromosomes test.bam`
parallel --verbose -j 4 parallel_call sample_A.bam ::: ${chrom_set}
```

### Combine the variant calls

Once we have completed variant calling we need to combine everything back in the right order. We can use a bash array to add a prefix and suffix to the list of chromosomes to reconstruct the output filenames and concatenate them into a single file.

```bash
# Remove intermediate files using  a bash trap
# the codes inside function will be performed when script is exiting
# for aany reason (error or success)
function cleanup {
  rm $@
}
trap cleanup EXIT

# Generate an array of the resulting files
# to be concatenated.
sample_name="sample_A"
set -- `echo $chrom_set | tr "\n" " "`
set -- "${@/#/${sample_name}.}" && set -- "${@/%/.bcf}"
# This will generate a list of the output files:
# sample_A.I.bcf sample_B.II.bcf etc.

set -- "${@/#/test.}" && set -- "${@/%/.bcf}"

# Output compressed result
bcftools concat $@ --output-type b > $sample_name.bcf
```

## Using parallel for programs that require more than one input file

A good example of this in bioinformatics is aligning paired-end reads to a genome. Every sample has a forward (Left) and reverse (right) read pair. For this example, we will be aligning several small sample files to a genome using bowtie2.  
Let’s download a toy example from Arabidopsis. I grabbed the first 250 sequences from four Arabidposis samples taken from NCBI’s SRA database.

**Download the example raw data**

The genome index is located in a directory called bwt_index

```zsh
mkdir bowtie
cd bowtie
wget www.bioinformaticsworkbook.org/Appendix/GNUparallel/fastqfiles.tar.gz
tar -zxvf fastqfiles.tar.gz

# download arabidopsis genome
wget https://www.arabidopsis.org/download_files/Genes/TAIR10_genome_release/TAIR10_chromosome_files/TAIR10_chr_all.fas

## Create the bowtie2 alignment database for the Arabidopsis genome**
bowtie2-build  TAIR10_chr_all.fas tair
```

We can design a bash script with the bowtie2 command for each pair separately . Obviously if dealing with hundreds of files this could become cumbersome.

```zsh
bowtie2 --threads 4 -x tair -k1 -q -1 SRR4420293_1.fastq.gz -2 SRR4420293_2.fastq.gz -S first_R1.sam >& first.log
bowtie2 --threads 4 -x tair -k1 -q -1 SRR4420295_1.fastq.gz -2 SRR4420295_2.fastq.gz -S fifth_R1.sam >& third.log
```

GNU parallel let’s us automate this task by using a combination of substitution and separators notably ::: and :::+. We can also make optimum use of the available threads. You may have also noticed some new syntax where we are using {1}, {2}, {1/.} and {2/.}. The {1} will give us the first file and {2} the second from the list taking two at a time. The {1/.} and {2/.} will take the prefix of the file name before the “.”

```zsh
time parallel -j 2 "bowtie2 --threads 4 -x tair -k1 -q -1 {1} -2 {2} -S {1/.}.sam >& {1/.}.log" ::: fastqfiles/*_1.fastq.gz :::+ fastqfiles/*_2.fastq.gz

```

## splitting a bog job to make use of all cpus

Lets assume we have a large file test.fa. Our aim is simply to count the lines in the fasta file.
  
You can download this example trinity file like this.

```zsh
mkdir trinity
cd trinity
wget www.bioinformaticsworkbook.org/Appendix/GNUparallel/test.fa

# instead of 
time wc -l test.fa

# we use parallel
# -a input is read from a file
# --pipepart: pipe parts of a file**
# --joblog  A logfile of the jobs completed so far
parallel -a test.fa --pipepart --block -1  time wc -l
```



## EXAMPLE: Replace a for-loop

It is often faster to write a command using GNU Parallel than making a for loop:

```zsh
for i in *gz; do 
      zcat $i > $(basename $i .gz).unpacked
done
```

Can also be written as:

```zsh
parallel 'zcat {} > {.}.unpacked' ::: *.gz
```

The added benefit is that the zcats are run in parallel - one per CPU core.

## Running NCBI-BLAST jobs in parallel

If there is a large file of sequences, then the traditional way for doing a BLAST search is to start with the first sequence and run them sequentially for all the sequences. This is very time consuming and waste of the processing power HPCs offer. There are several ways to speed up this process. Most of them split the input sequence file into multiple pieces (usually equal to the number of processors) and run the BLAST search simultaneously on each of the split file. So larger the number of processors, more faster the whole process.

###  Using GNU Parallel to split the large job to smaller chunks

One easy way is to use the ‘‘parallel’’ command from the GNU Parallel.  
Here, the large file is divided into blocks of 100kb, making sure that each ‘piece’ starts with the symbol >. The total number of pieces is dependent on total number of processors (from all nodes combined) eg., (standard fasta format). If there are 64 processors and 2 nodes, there will be a total of 128 sequence file pieces. On each of these pieces, blastp program is called with options. Note that you need to follow your query with a dash - indicating that the input is coming from the stdout, rather than the file. The results are written to the final file combined_results.txt. The order of the results will vary depending on what sequence went through the BLASTpipe and doesn’t match the input order.

```zsh
cat large.fasta | \
  parallel -S :,server1,server2 \
    --block 100k \
    --recstart '>' \
    --pipe \
  blastp \
    -evalue 0.01 \
    -outfmt 6 \
    -db db.fa
    -query - > combined_results.txt
```

### Splitting input query in to smaller pieces

This is a good example of running parallel using ssh login and multiple nodes. Please read the topic from <https://bioinformaticsworkbook.org/dataAnalysis/blast/running-blast-jobs-in-parallel.html#gsc.tab=0>

## EXAMPLE: Parallelizing BLAT

This will start a blat process for each processor and distribute foo.fa to these in 1 MB blocks:

```zsh
cat foo.fa | parallel --round-robin --pipe --recstart ">" "blat -noHead genome.fa stdin >(cat) >&2" >foo.psl
```

## EXAMPLE: Blast on multiple machines

Assume you have a 1 GB fasta file that you want blast, GNU Parallel can then split the fasta file into 100 KB chunks and run 1 jobs per CPU core:

    cat 1gb.fasta | parallel --block 100k --recstart '>' --pipe blastp -evalue 0.01 -outfmt 6 -db db.fa -query - > results

If you have access to the local machine (and can ideally log into either of them using SSH keys), here named `server1` and `server2`, GNU Parallel can distribute the jobs to each of the servers. It will automatically detect how many CPU cores are on each of the servers:

    cat 1gb.fasta | parallel -S :,server1,server2 --block 100k --recstart '>' --pipe blastp -evalue 0.01 -outfmt 6 -db db.fa -query - > result

**EXAMPLE: Run [bigWigToWig](https://www.encodeproject.org/software/bigwigtowig/) for each chromosome:**

If you have one file per chomosome, it is easy to parallelize processing each file. Here we do bigWigToWig for chromosome 1..19 + X Y M. These will run in parallel but only one job per CPU core. The `{}` will be substituted with arguments following the separator `':::'.`

    parallel bigWigToWig -chrom=chr{} wgEncodeCrgMapabilityAlign36mer_mm9.bigWig mm9_36mer_chr{}.map ::: {1..19} X Y M

**EXAMPLE: Running composed commands:**

GNU Parallel is not limited to running a single command. It can run a [composed command](http://www.mit.edu/~jtidwell/language/composed_command.html). Here is now you process multiple FASTA files using [Biopieces](https://maasha.github.io/biopieces/) (which uses [pipes](https://en.wikipedia.org/wiki/Pipeline_%28Unix%29) to communicate):

    parallel 'read_fasta -i {} | extract_seq -l 5 | write_fasta -o {.}_trim.fna -x' ::: *.fna

See [this](http://code.google.com/p/biopieces/wiki/HowTo#Howto_use_Biopieces_with_GNU_Parallel) also for more information.

**EXAMPLE: Running experiments:**

Experiments often have several parameters where every combination should be tested. Assume we have a program called experiment that takes 3 arguments: `--age`, `--sex` and  `--chr`:

    experiment --age 18 --sex M --chr 22

Now we want to run experiment for every combination of ages 1..80, sex M/F, chr 1..22+XY:

    parallel experiment --age {1} --sex {2} --chr {3} ::: {1..80} ::: M F ::: {1..22} X Y

To save the output in different files you could do:

    parallel experiment --age {1} --sex {2} --chr {3} '>' output.{1}.{2}.{3} ::: {1..80} ::: M F ::: {1..22} X Y

But GNU Parallel can structure the output into directories so you avoid having thousands of output files in a single directory for neatness:

    parallel --results outputdir experiment --age {1} --sex {2} --chr {3} ::: {1..80} ::: M F ::: {1..22} X Y

This will create files like `outputdir/1/80/2/M/3/X/stdout` containing the standard output of the job.

If you have many different parameters it may be handy to name them:

    parallel --result outputdir --header : experiment --age {AGE} --sex {SEX} --chr {CHR} ::: AGE {1..80} ::: SEX M F ::: CHR {1..22} X Y

Then the output files will be named like `outputdir/AGE/80/CHR/Y/SEX/F/stdout`

If one of your parameters take on many different values, these can be read from a file using `'::::'`

    echo AGE > age_file
    seq 1 80 >> age_file
    parallel --results outputdir --header : experiment --age {AGE} --sex {SEX} --chr {CHR} :::: age_file ::: SEX M F ::: CHR {1..22} X Y

**Advanced example: Using GNU Parallel to parallelize you own scripts:**

Assume you have BASH/Perl/Python script called `launch`. It takes one arguments, ID:

    launch ID

Using parallel you can run multiple IDs in parallel using:

    parallel launch ::: ID1 ID2 ...

But you would like to hide this complexity from the user, so the user only has to do:

    launch ID1 ID2 ...

You can do that using `--shebang-wrap`. Change the shebang line from:

    #!/usr/bin/env bash
    #!/usr/bin/env perl
    #!/usr/bin/env python

to:

    #!/usr/bin/parallel --shebang-wrap bash
    #!/usr/bin/parallel --shebang-wrap perl
    #!/usr/bin/parallel --shebang-wrap python

You further develop your script so it now takes an `ID` and a `DIR`:

    launch ID DIR

You would like it to take multiple IDs but only one DIR, and run the IDs in parallel. Again, simply change the shebang line to:

    #!/usr/bin/parallel --shebang-wrap bash

And now you can run:

    launch ID1 ID2 ID3 ::: DIR

If you want/need to build a cluster of nodes that can run jobs with parallel over a shared sshfs mount, see [this](https://gist.github.com/Brainiarc7/24c966c8a001061ee86cc4bc05826bf4) gist for the definitive guide. 

# `find`

# `sed`

  [b4bf5877]: https://stackoverflow.com/questions/19789102/why-is-bash-errexit-not-behaving-as-expected-in-function-calls?noredirect=1&lq=1 "Link"

# working with processes: `ps`

`ps aux | grep openconnect` is common syntax. `aux` means display processes for all users (from **-a**), adding a column indicating the user (from **-u**), and outputs processes that are running even if they weren't started from a terminal (**-x**)
  
Columns in `ps aux`:
  
- `USER`: user running the process
- `PID`: process ID
- `CPU`: percentage of CPU used
- `%MEM`: percentage of memory used
- `VSZ`: virtual memory size (in kilobytes)
- `RSS`: resident set size (in kilobytes)
- `TT`: controlling terminal. `??`: process is started from another process or your OS.
- `STAT`: process state code. running (`R`), sleeping (`S`), stopped (`T`)
- `START`: time command was started (sometimes this is `STARTED`)
- `TIME`: time running
- `COMMAND`: command that started process

Occasionally your system runs low on physical memory (RAM), and your operating
system does its best to manage. Unfortunately the only way your operating
system can give a process more memory than is physical available is by taking a
chunk of less-used memory (these chunks are used by *pages*, a word you may see
in the `ps` and `top` manuals), writing it to disk (this is slow!), and then
using that now-free page for your process. Since you're *swapping* a in-memory
physical page of memory for one on a hard drive, the part of your disk that
manages this type of activity is called *swap space*. This may seem like a lot
of technical detail, but it has a very real result in day-to-day bioinformatics
work: if you run out of physical memory, your processes will be forced to start
swapping memory to the hard disk, and hard disks are really slow. High-memory
tasks like assembly (and in some cases alignment) on machines with insufficent
physical memory will halt even the fastest machines to a stand still.
  
With this information, `VSZ` and `RSS` will now make more sense. `VSZ` is the
amount of virtual memory and `RSS` is the amount of physical memory. Virtual
memory includes both swap and physical memory, so `VSZ` is larger than `RSS`.
`ps aux` gives you a quick glance at these values, but the way an operating
system allocates memory can be a very baffling process to decode. When we want
to integate our processes to see which are using the most memory, CPU, or swap
space, `ps` becomes a less useful and `top` becomes our tool of choice. But
remember, for searching for processes, `ps` and `grep` can be combined in great
ways.

## Interrogating Processes with +top+ and Understanding Bottlenecks

Unlike `ps`, `top` keeps refreshing your list of processes. Enter `top` at a
command line, and it will update (Linux versions usually update every three
seconds and OS X every one second). To exit `top`, just press 'q'. But
constantly updating isn't top's greatest strength — be able to interactively
look at which processes are using the most resources is.
  
```zsh
top
```

There's undoubtly a lot going on here, but let's take a look at the most
important parts. The manual for `top` are quite dense, so I'll cover the most
commonly used sections. The very first line covers the uptime (22 minutes), how
many users are on the system, and the *load averages*. Loads in Unix systems
are given as three numbers: the load in the past one minute, five minutes, and
fifteen minutes. The number ranges from 0 to over 1, but the interpretation
depends on how many *cores* your processor has, and what's holding up
processes. When we run bioinformatics programs, we can hit different
bottlenecks: memory running out requiring we use swap space, reading large
files from a slow disk, waiting for BLAST results to be recieved over the
network from another machine, or actually processing done in the CPU. In cases
where we have many processes all trying to use our CPU, we will see the load
average rise. Processes will have to wait for their turn to use our CPUs, and
this can lead to slow behavior. This is why if we have a single processor
machine, we don't want to use GNU Parallel or `xargs` in parallel: it would
create two processes that would have to weight on each other to share the only
CPU.

On a two core system, a load average of 2 means that both CPU cores are
constantly working over a given time period (in the load average case, one,
five, or fifteen minutes). A load average over 2 means that now processes are
having wait for each other. Under 2 means that our CPU has periods where it's
not doing processing. Load averages are a very important statistic to look at
when running large bioinformatics jobs because they could give you a hint if
you're doing too much at once, and maxing out your CPU (load average above the
number of cores you have), or if processes have been running for a while and
load average is low, this could indicate something else (disk, memory, or
network) is a the bottleneck.

The next line breaks gives you the number of processes broken down by state
(running, sleeping, etc), which we saw in `ps aux`'s output per process. The
third line is another important one: it specifies what the CPU spent its time
on since the last update of `top`. In our example, we see it spent 70.9% of
it's time in "us", 2% of it's time in "sy", and 27.1% of its time in "wa".
There are many options that we don't need to go into too much detail about (see
the manual if you are curious), but it's worth know that Unix systems divide up
userland ("us") and system ("sy"), and most of our data crunching
bioinformatics work will tax our userland resources. More importantly, Linux
`top` gives "wa", which is the percent of time the CPU is waiting on other
stuff, and a consistenly high percent "wa" is a good warning sign something
other than CPU is the bottleneck.

The next too lines cover memory and swap space usage. This machine has
physically more than 594 megabytes of memory, but this is the amount accessible
to the operating system. The amount of free memory can be an indicator as to
whether we're running out of memory and our machine is about to hit the much
slower swap space.

The next lines are each process running, and a summary of their process ID
(`PID`), who started them (`USER`), their priority (`PR`), the amount of
virtual and physical memory (`VIRT` and `RES`, repsectively), the state (`S`),
percentage CPU and memory usage (`%CPU` and `%MEM`), the time running
(`TIME`), and finally the actual command (`COMMAND`). We had many of these same
columns with `ps aux`, but a big advantage with `top` is that we can
interactively sort them and watch them update. Recall that your bioinformatics
processes may need lots of memory one minute, then start getting CPU-hungry the
next, and finally start reading gigabytes of information off the disk. We can
watch our process's resource requirements live with `top`.

Particularly useful is being able to sort this live in `top` (and for
completness, note this is possible with `ps` too). To sort by memory, just
press `O` (capital letter "o"), which brings up a list of possible sort fields.
One of these fields is `%MEM`, and corresponds to the letter "n" (recall, this
is only Linux `top`, consult `man top` if your version is different). Pressing
"n", then enter will resort your `top` process list by memory usage. Other
options I commonly use are "K" for CPU usage, "p" for swap size, "e" for user
name, and "l" for CPU time. If your machine is running slowly, using `top`
interactively to find greedy processes should be the first place to start.

# usefull process related commands

## `pgrep`

look up or signal processes based on name and other attributes

```zsh
pgrep java
pgrep -u milad java
# pid of processes own by milad or root with name java
# usually the bigest number is the parent pid
pgrep -u milad,root java
```

## `pstree`

```zsh
pstree -p | grep java
#
#           |               |                    |-zsh(3804)---xargs(8488)---java(8489)-+-{java}(8490)
#           |               |                    |                                      |-{java}(8491)
#           |               |                    |                                      |-{java}(8492)
#           |               |                    |                                      |-{java}(8493)
#           |               |                    |                                      |-{java}(8494)
#           |               |                    |                                      |-{java}(8495)
#           |               |                    |                                      |-{java}(8496)
#           |               |                    |                                      |-{java}(8497)
#           |               |                    |                                      |-{java}(8498)
```

## `pidof`

```zsh
pidof apache2
# 3754 2594 2365 2364 2363 2362 2361
# 3754 is parent pid
pidof java
```

## `ps`

```zsh
ps aux | grep "apache2"
# by command name (-C)
ps -fC firefox
```

# Interacting with Processes Through Signals: Using Kill

If you've used `ps aux` and `grep` to find a particular program, or
you've spotted a greedy process with `top`, you can kill or set its
priority using the Unix tools `kill` and `renice` respectively.

The command `kill` is used to send signals to running processes. The
default signal it sends to a running program is to terminate a
program. A _signal_ is a way to communicate with a running
process. The termination signal, called `SIGTERM`, and we can
terminate a program called "greedy_cmd" by first getting its process
ID with `ps aux` and `grep` (or through `top` too) and then using
`kill`:

```zsh
  ps aux | grep "greedy_cmd"
  # vinceb  10141   99.0  0.0  9235248  428 s004  U+  1:48PM  0:00.00 greedy_cmd
  kill 10141
```

However, programs can choose how they wish to handle a `SIGTERM`. Some
programs could even ignore a termination signal entirley, although
this is not common practice with most programs we'll use. Still,
sometimes you'll need to send a more forceful signal like `SIGKILL`,
which can't be ignored by programs. To specify the signal with `kill`,
we use `kill -s SIGKILL 10141`

If you've used Unix for a while, you've probably run across pressing
control-C to stop a program. This sends another common signal called
`SIGINT`, of which the the "int" is short for interupt. `kill -s
SIGINT` is the same as pretty control-C in the same terminal as a
program is running. If you've ever used control-Z (for suspend), this
is very similar too, as it sends the signal `SIGSTP`. `SIGSTP` is an
signal that suspends a process. Suspending a process pauses it, and
this paused process can then be changed to run in the background or
foreground with the `jobs`, `bg`, and `fg` commands we learned
earlier.

Most of the time when we're using `kill` we're not using it to send
these other signals to processes, we're trying to quickly kill an out
of control command that's using too many resources. For this, `kill
-s SIGKILL <pid>` is the standard tool we reach for. Signals also
have numeric shortcuts, and 9 corresponds to `SIGKILL`, so `kill -9
<pid>` does the same thing.

```zsh
kill -s SIGTERM <PID> # equivalent to `kill <PID>`
kill -s SIGKILL <PID>
kill -s SIGSTP <PID> # equivalent to ctrl+z, pause
kill -s SIGINT <PID> # equivalent to ctrl+c, interupt
```

# Prioritizing Processes: Using Nice and Renice

Resources are finite on even our most powerful servers, so it's necessary to
mind how many resources are being used by programs. This is especially the case
with Unix machines which multitask, running many processes simultaneously. Since
multitasking is such a core part of Unix (and Unix comes from an era of
comparatively slower machines with fewers resources), there's a way to
prioritize certain processes: through a process's *nice* value.

The nice value of a process ranges from -20 to 20 (19 on some systems), where a
lower nice value gives a process _more_ priority. A good way to remember this is
that a lower nice value means a program isn't being nice to other processes, and
is instead using all the CPU resources for itself. A very high nice value like
19 tells your operating system that this process is pretty low priority, so it
should run it whenever resources are available. Note that the nice value only
affects how much CPU priority a process gets. Memory or disk-bound processes
will not gain much from getting a lower nice value.

The default nice value a process is 0, but this can be set by a using
the command `nice` to run a program under a specified nice value.
Good usage examples include tasks like backups, system updates, or
archiving old projects. For example, if you want to run gzip on a
large FASTQ file in the background, with lower priority, you would use
`nice`:

```zsh
 nice -n 10 gzip zmaysA_R1.fastq
```

This runs the command `gzip zmaysA_R1.fastq`, incrementing the default
value of 0 to 10. If we have an already running process, we could
adjust its nice value with the command `renice`, which takes a nice
value and a process ID, like: `renice 10 <pid>`. This sets the nice
value of the process with ID `<pid>`.

As more cores are packed into modern CPUs, CPUs are less likely to be
the bottleneck than the disk or memory. Nice is handy, but it's not
can't work miracles on heavily-taxed systems. If a program is hanging,
or your system feels sluggish, it's very important to use `top` to
monitor CPU and memory usage. Somewhat surprisingly for beginners in
large data processing, the disk is usually the culprit for processing
bottlenecks, so in the next section we'll look at a way to monitor
disk input and output.

### Disk Usage

When processing bioinformatics data, it's not uncommon to fill up entire disk
volumes. As disks fill up, they also become more fragmented, meaning that they
write data to the disk in non-consecutive chunks. Disk fragmentation leads to
slower disk performance; disks pushing 80% full not only run a risk of being
filled up during data processing, but even performance tasks not requiring lots
of disk space will suffer. Thus, it's useful to monitor disk usage
periodically.

The two tools used to look at disk usage are `df` and `du`. The first, `df`
simply gives you a terminal-based display of your disk usage, broken down by
volume. Since I don't like having to trying to count the digits in figures like
"51228178" bytes (the default unit), I almost always the `-h` option to
display the units in human readable format. On the machine I'm on now, this
looks like:

    $ df -h
    Filesystem                  Size  Used Avail Use% Mounted on
    /dev/sda1                   854G   32G  779G   4% /
    none                        4.0K     0  4.0K   0% /sys/fs/cgroup
    udev                         32G  4.0K   32G   1% /dev
    tmpfs                       6.3G  372K  6.3G   1% /run
    nas-8-0:/export/1/vinceb     11T  2.6G   11T   1% /home/vinceb

Note that there are some file systems in this output that look strange, like
`none` and `tmpfs`. Many Unix-like operating systems use some virtual disks.
This all goes back to how files can be abstracted under Unix-like operating
systems. Real disks, like `/dev/sda1`, are interfaced with device files in
`/dev` on Unix (as well as the psuedo-devices like `/dev/null` we saw in the
redirection section above).

The command `df` is relatively straight forward: it lists the file system size,
the amount used, available, and the percent used, as well as where the file
system is mounted. _Mounting_ file systems allows us to access them through a
certain point on the Unix filesystem. For example, `/home/vinceb` is mounted to
a directory on nas-8-0, a Network Attached Storage device in the example above.
In day-to-day work, it's usually good to check that disks aren't getting too
full before running large data processing tasks.

However, now assume you've found that your disk is getting too full, and you
want to figure out which files are using the most disk space. One command
that's useful in finding large files is `du`, which recursively lists the sizes
of the file in the directory it's being run. For example, if you suspect that
there are some large files in a project directory named
`~/Projects/tarsier_genes`, you could use `du` to find which directories
contain the largest files:

```zsh
  du -h ~/Projects/tarsier_genes
   # 20M    /home/vinceb/Projects/tarsier_genes
   # 64K    /home/vinceb/Projects/tarsier_genes/notes
   # 20M    /home/vinceb/Projects/tarsier_genes/data/
   # 640G   /home/vinceb/Projects/tarsier_genes/data/alignments
```

Clearly, there's some big files in our project `alignment/` directory we may
want to delete or compress with a program like `gzip`.

The program `du` can also be combined with `sort` and `head` to find the
largest files on a Unix system. This is a good example of how Unix pipes allow
many small programs to be connected to create useful other programs. If we run
`du /`, it will output the size of the contents for each directory below
`/` (recursively). With `/`, this will be a lot of directories, and
since we only care about the largest ones, we use `sort` with the `-n`
option to numerically sort the lines, and to do so `-r` in reverse order
so the largest files are at the top). Then, we pipe all output to `head -n 10`
to only give us the top five directories containg the most content. Note that
we can't use human readble formats (`-h`) anymore, since numeric sorting
doens't understand suffixes like "K" and "M" for kilobytes and megabytes. On
some BSD-variant systems, it may be necessary to explicitly say that you want
sizes in one unit with (`-m`). The basic command would like like:

```zsh
    du / | sort -r -n | head -n 5
    # 2036816991542 /
    # 411030308497 /share/data/genomes
    # 19390538953 /Users
    # 1042908919 /Users/lauren
    # 1041811032 /Users/lauren/s_cereale_genome
```

Note that `du` is hierarchical, so there will be some redundancy as the
directory containing many large files, as well as the directory containg this
directory, and so on are all included. Finding large files is a common problem,
and specialized programs are also used.

# installing bioawk

```zsh
git clone git://github.com/lh3/bioawk.git && cd bioawk && make && sudo cp bioawk /usr/local/bin/
```

# arrays

## Define arrays

```zsh
set -A arr one two three
arr=( one two three )

# shell allows newlines to separate array elements
arr=(
'first element'
'second element'
'third element'
)
echo ${#arr} # 3 (spaces are qouted inside each element)

declare -a arr # an empty array
```

you can save the result of a zsh recursive file search as follows:

```zsh
src_files=( **/*.[ch] )
```

a similar example that will work in bash is as follows:  
In bash you can’t match files recursively using globbing, but you can use the find command in a command substitution.

```bash
# find seperate each entry with new line
src_files=( $(find . -name '*.[ch]' -print) )
```

## Accessing array elements

In bash, arrays are 0-based.

```bash
arr=( one two three )
echo ${arr[1]}  # two

# if you leave the subscript out in an array expansion,
#just the first element of the array will be expanded
echo $arr # one
```

In zsh, arrays are 1-based.

```zsh
echo $arr[1] # one
offset=1
echo $arr[1+$offset]

#all elements are expaded, when no index is provided
echo $arr # one two three (in bash, ${arr[@]} or ${arr[*]})

#In zsh, you can even replace a range of values in an array
arr[2,3]=( zwei drei )

#remove element(s)
arr[2]=()

# change first two element and remove third element
arr[1,2,3] <- ( 1 2 )

# get the second  through last element (-1 is for last element)
echo $arr[2,-1] # two three (in Bash, echo ${arr[${#arr}-1]})
```

## Array Attributes: `declare`

In bash and zsh, it doesn’t matter which of the two names you use—they are both the same command.  
The declare command allows you to specify the type of a variable or to specify one or more attributes for a variable. To specify that a variable is an array, you use the `-a` option.  
If arr already existed as another type of variable, declare -a converts it to an array.

### `-a` and `-p` attributes

```zsh
declare -a arr
```

```bash
var=string
declare -a var  # make it an array
declare -p var  # output an assignment statement that could be used to re-create the variable (next line)
# declare -a var='([0]="string")'

arr=( [0]=one [1]=two [2]=three )
declare -a arr=( [0]=one [1]=two [2]=three )
declare -a arr=( one two three )
```

### `-U` attribute of zsh

-U option, which can be very useful with arrays. It removes duplicate values in an array. For example, in your startup file, you may want to add a directory to your path but it is better if it doesn’t then appear in your path multiple times. Try this, repeating the second command multiple times. It should have no further effect
on the path. As with all the attributes specified with declare, you can remove them by using a plus in front of the option—`declare +U` path in this case.

```zsh
declare -U path
path=( ~/bin $path )
```

### `-T` attribute of zsh and tied arrays

The path array (see the example above) is the companion of the PATH variable—if you modify one, changes are reflected in the other. Using an option to declare, you
can tie together any other two variables in a similar manner. The scalar variable will contain each of the elements in the array separated by **colons**.
zsh 4.2 allows you to choose a **different separator** character.

```zsh
declare -T LD_LIBRARY_PATH ld_library_path
```

## Array expansion

In bash: `${arr[@]}`   and `${arr[*]}`  
The first form results in each element of the array being a separate word while the
second form amounts to joining all the array elements together with a space 1 between each
element and treating the resulting string like a scalar variable expansion. One of the reasons
why the difference isn’t always obvious is that, if you use ${arr[*]} outside double quotes in
bash, it is promptly split up again at all the spaces that were used to join the array elements.

```bash
alias showargs="printf '>>%s<<\n'"
arr=( one 'two three' four )
showargs ${arr[*]}
 # >>one<<
 # >>two<<
 # >>three<<
 # >>four<<
showargs "${arr[*]}"
 # >>one two three four<<
showargs "${arr[@]}"
 # >>one<<
 # >>two three<<
 # >>four<<
```

In zsh, by default, no word split would happen in array (equal to third example above `"${arr[@]}"` in bash) In zsh you would need an equals sign for the words to be split ($=arr[*])

```zsh
arr=( one 'two three' four )
showargs $=arr
 # >>one<<
 # >>two<<
 # >>three<<
 # >>four<<

showargs ${=arr}
 # >>one<<
 # >>two<<
 # >>three<<
 # >>four<<
```

in ZSH, If you don’t use array expansions on their own but have adjacent text, the text is made to adjoin the first or last element of the array:

```zsh
showargs "BEFORE${arr[@]}AFTER"
 # >>BEFOREone<<
 # >>two three<<
 # >>fourAFTER<<
```

This isn’t particularly useful, especially if you compare it to how brace expansions work. With the **rc_expand_param** option on, arrays in zsh work like this. `^` activates the rc_expand_param for single expansion. Using two carets as in this example turns off rc_expand_param for the single expansion.

```zsh
showargs BEFORE{one,two\ three,four}AFTER
 # >>BEFOREoneAFTER<<
 # >>BEFOREtwo threeAFTER<<
 # >>BEFOREfourAFTER<<
showargs BEFORE${^arr}AFTER # same result as above
```

Braces and array expansions can be mixed. With rc_expand_param, that produces all the
possible combinations, which can result in quite a lot of words. With the option unset, the
brace expansion is applied against just the first (or last) element of the array:

```zsh
#Using two carets as in this example turns off rc_expand_param for the single expansion
showargs {A,B}$^^arr{C,D}
 # >>Aone<<
 # >>Bone<<
 # >>two three<<
 # >>fourC<<
 # >>fourD<<

showargs {A,B}$^arr{C,D} # with rc on, it will produce all combination.
arr_2=( {A,B}$^arr{C,D} )  # make anew array with all combinations
```
