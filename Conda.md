# Conda tutorial

- list environments & check active env: ```conda env list```
- create env & install packages: ```conda create -c anaconda | conda-forge -n myenv python=3.6 numpy matplotdb pandas```
- create env using YAML file: ```conda env create --file env.yml```
- activate environment: ```conda activate myenv```
- deavtivate enviroment and back to base: ```conda deactivate```
- exporting env to YAML file & sharing it: ```conda env export -f myenv.yml -n myenv```
- Making an exact copy of an environment (test to live) ```conda create --name live_env --clone test_env```
- deleting an environment: ```conda env remove -n live_env```
- list available packages in an env: ```conda list -n test_env```
- **Anaconda cloud** is a package management service with many channels/owners creating conda packages. Not all of them are trusted. **conda-forge** is a reliable source for many python packages. ```conda install -c conda-forge rasterio=0.35```
