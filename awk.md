# AWK

## Arrays in awk

- In awk , it isn’t necessary to specify the size of an array before starting to use it. Additionally,any **number** or **string**, not just consecutive integers, may be used as an array index.

- Arrays in awk are **associative arrays** (like dictionaries). This means that each array is a collection of pairs—an index and its corresponding array element value.

- One advantage of associative arrays is that new pairs can be added at any time.

- array subscripts are always strings

- single array can have both numbers and strings as indices.

- awk ’s arrays are **efficient—the** time to access an element is independent of the number of elements in the array.

### checking if an element exists in array

```zsh
# Incorrect: Check if "foo" exists in a.
 # This will creat an element with index foo and empty value
if (a["foo"] != "") ...

#Incorrect (will creat frequencies[2])
if (frequencies[2] != "")
print "Subscript 2 is present."

# corrret way (testing for index not value)
if (2 in frequencies)
    print "Subscript 2 is present."
```

### example: sorting lines by line  number

Here in the input file:

```zsh
5   I am the Five man
2   Who are you? The new number two!
4 . . . And four on the floor
3   Who is number one?
1   I three you.
```

Here is the scipt  
It is a very simple program and gets confused upon encountering repeated numbers, gaps, or lines that don’t begin with a number. The first rule keeps track of the largest line number seen so far; it also stores each line into the array arr , at an index that is the line’s number. The second rule runs after all the input has been read, to print out all the lines.  
If a line number is repeated, the last line with a given number overrides the others.

```zsh
{
 if ($1 > max) # max variable defined on the fly
    max = $1
 arr[$1] = $0
}
END {
for (x = 1; x <= max; x++)
    print arr[x]
}
```

Gaps in the line numbers can be handled with an easy improvement to the program’s END rule

```zsh
END {
    for (x = 1; x <= max; x++)
    if (x in arr)
    print arr[x]
}
```

### Scanning All Elements of an Array

This loop executes body once for each index in array that the program has previously used, with the
variable var set to that index.

```zsh
for (var in array)
    body
```

#### example

The first rule scans the input records and
notes which words appear (at least once) in the input, by storing a one into the array used with the word
as the index. The second rule scans the elements of used to find all the distinct words that appear in the input.

```zsh
# Record a 1 for each word that is used at least once
{
for (i = 1; i <= NF; i++)
    used[$i] = 1  # order of indexes are arbitarary and cannot be channged
}
# Find number of distinct words more than 10 characters long
END {
for (x in used) {
    if (length(x) > 10) {
        ++num_long_words
        print x
}
}
print num_long_words, "words longer than 10 characters"
}
```

## sorted array traversal in gawk

By default, when a for loop traverses an array, the order is undefined, meaning that the awk
implementation determines the order in which the array is traversed. This order is usually based on the
internal implementation of arrays and will vary from one version of awk to the next.  
  
Solution: define `PROCINFO["sorted_in"]` before loop in gawk.  

```zsh
gawk '
    BEGIN {
         PROCINFO["sorted_in"] = "@ind_str_asc"
         a[4] = 4
         a[3] = 3
         for (i in a)
            print i, a[i]
    }'
# -| 3 3
# -| 4 4
```
