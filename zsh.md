# Tips about zsh

# word-splitting behaviour

SH_WORD_SPLIT is set when zsh is invoked with the names `ksh or sh`, or (entirely equivalent) when emulate ksh or emulate sh is in effect.  
Note also the "$@" method of word splitting is always available in zsh functions and scripts (though strictly this does array splitting, not word splitting). This is more portable than the $*, since it will work regardless of the SH_WORD_SPLIT setting; the other difference is that $* removes empty arguments from the array. You can fix the first half of that objection by using ${==*}, which turns off SH_WORD_SPLIT for the duration of the expansion.  

## word-splitting behaviour: variable

```zsh
var="foo bar" #var with two seperated words
args() {$#}   # fun to show number of its args
args $var     # in zsh it outputs 1

setopt shwordsplit # set option to be compatible wit bash
args $var     # now outputs 2

#This would happen regardless of the value of the internal field separator($IFS)
foo=foo:bar
IFS=:
setopt no_shwordsplit #putting no befor option unset it
args $foo #outputs 1

setopt shwordsplit
args $foo #outputs 2
```

## word-spliting behaviour: arrays

The natural way to produce word-splitting behaviour in zsh is via arrays.

```zsh
set -A array one two three twenty # or
array=(one two three twenty)
args $array # outputs 4 regardless of the setting of SH_WORD_SPLIT

```

## word-spliting behaviour: eval

```zsh
sentence="Milad is a good boy"
# eval performs the word splitting
eval "words=($sentence)"  # $words is an array with the words of $sentence
args $words # 5
```

## word-splitting behaviour: `${=sentence}`

less standard but more reliable, turning on SH_WORD_SPLIT for one variable only:

```zsh
    args ${=sentence} #always returns 8
```

## `take myfolder`: create folder and navigate to it

## `...`: navigates two folder back

## `cd Desktop` becomes `Desktop`

## `cd \u\lo\b\` press <TAB> to auto compplete path

## `z` pluggins: redirect to recent folders:
```zsh
z my_project
```
