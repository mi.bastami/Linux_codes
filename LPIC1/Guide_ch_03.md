# Configuring Hardware

## USB

There are two steps to get Linux to interact with USB devices:  
1. `kernel module to recognize USB controller`. First, the Linux kernel must have the proper module installed to recognize the USB
controller that is installed on your server, workstation, or laptops. The
controller provides communication between the Linux kernel and the USB
bus on the system. When the Linux kernel can communicate with the
USB bus, any device you plug into a USB port on the system will be recog-
nized by the kernel, but may not necessarily be useful. 
2. the Linux system must then also have a kernel module installed for the individual device type plugged into the USB bus.

## `setpci` view and change PCI settings

Most PCI boards utilize the **Plug-and-Play (PnP)** standard, which auto-
matically determines the configuration settings for the boards so no two
boards conflict with each other. If you do run into conflicts, you can use the
`setpci` utility to view and manually change settings for an individual PCI
board.

## The `/dev` Directory

### device files

After the Linux kernel communicates with a device on an interface, it must be able to trans-
fer data to and from the device. This is done using **device files**. Device files are fi les that the
Linux kernel creates in the special /dev directory to interface with hardware devices.
To retrieve data from a specific device, a program just needs to read the Linux device
file associated with that device. The Linux operating system handles all the unsightliness of
interfacing with the actual hardware. Likewise, to send data to the device, the program just
needs to write to the Linux device file.  
As you add hardware devices such as USB drives, network cards, or hard drives to
your system, Linux creates a file in the /dev directory representing that hardware device.
Application programs can then interact directly with that file to store and retrieve data
on the device. This is a lot easier than requiring each application to know how to directly
interact with a device. 
   
There are **two types of device files in Linux**, based on how Linux transfers data to the
device:

- **Character device files:** Transfer data one character at a time. This method is often used for serial devices such as *terminals* and *USB devices*.
- **Block device files**: Transfer data in large blocks of data. This method is often used for high-speed data transfer devices such as hard drives and network cards.

The type of device file is denoted by the first letter in the permissions list:

```zsh
ls -al /dev/{sd*,tty*}
#c for charatcter and b for block
# crw-rw---- 1 root dialout 4, 73 Aug 22  2020 ttyS9
# brw-rw---- 1 root disk 8,  0 Aug 22  2020 sda
```

### device mapper

Besides device files, Linux also provides a system called the **device mapper** . The device
mapper function is performed by the Linux kernel. It maps **physical block devices** to **virtual block devices**. These virtual block devices allow the system to intercept the data written to or read from the physical device and perform some type of operation on them.  
The device mapper creates virtual devices in the `/dev/mapper` directory.
These files are links to the physical block device files in the `/dev` directory.  
 Mapped
devices are used by the **Logical Volume Manager (LVM)** for creating logical drives and by
the **Linux Unifi ed Key Setup (LUKS)** for encrypting data on hard drives when those fea-
tures are installed on the Linux system.

## `/proc` direcotry files

### Interrupt Requests `/proc/interrupts`

Interrupt requests (IRQs) allow hardware devices to indicate when they have data to send to the CPU. The PnP system must assign each hardware device installed on the system a unique IRQ address. Some IRQs are reserved by the system for specific hardware devices, such as 0 for the
system timer and 1 for the system keyboard. Other IRQs are assigned by the system as
devices are detected at boot time.

```zsh
# cat /proc/interupts
         CPU0       CPU1       CPU2       CPU3       CPU4       CPU5       CPU6       CPU7       
  0:         10          0          0          0          0          0          0          0   IO-APIC   2-edge      timer
  8:          0          0          0          0          0          1          0          0   IO-APIC   8-edge      rtc0
  9:          0          4          0          0          0          0          0          0   IO-APIC   9-fasteoi   acpi
 16:          0          0         29          0          0          0          0          0   IO-APIC  16-fasteoi   ehci_hcd:usb1

```

### I/O ports `/proc/ioports`

The system I/O ports are locations in memory where the CPU can send data to and receive
data from the hardware device. As with IRQs, the system must assign each device a unique
I/O port. This is yet another feature handled by the PnP system.  
You can monitor the I/O ports assigned to the hardware devices on your system by look-
ing at the `/proc/ioports` file.  
With **PnP**, I/O port conflicts aren’t very com-
mon, but it is possible that two devices are assigned the same I/O port. In that case, you
can manually override the settings automatically assigned by using the `setpci` command.

### direct memory access (DMA) `/proc/dma`

Using I/O ports to send data to the CPU can be somewhat slow. To speed things up, many
devices use direct memory access (DMA) channels. DMA channels do what the name
implies—they send data from a hardware device directly to memory on the system, without
having to wait for the CPU. The CPU can then read those memory locations to access the
data when it’s ready.  
As with I/O ports, each hardware device that uses DMA must be assigned a unique
channel number.

```zsh
cat /proc/dma
# 4: cascade
# DMA chanel 4 is in use
```

### `/proc/cmdline`

the command that used by bootloader to start the kernel. It is usefull for seeing the kerrnel options.

```zsh
cat /proc/cmdline
# BOOT_IMAGE=/boot/vmlinuz-4.15.0-112-generic root=UUID=7daab171-126f-49ea-93ce-f1a455f7608c ro quiet splash
```

## `lsdev`

Insyall it using `sudo apt-get install procinfo`. The lsdev command-line command displays information about the hardware devices
installed on the Linux system. It retrieves information from the `/proc/interrupts`, `/proc/ioports`, and `/proc/dma` virtual files and combines them together in one output. This gives you one place to view all the important information about the devices run-
ning on the system, making it easy to pick out any conflicts that can be causing problem.

## `lsblk`

The lsblk command displays information about the block devices installed on the Linux
system. By default, the lsblk command displays all block devices. Notice that at the end of Listing 3.6, the lsblk command also indicates blocks that are
related, as with the device-mapped LVM volumes and the associated physical hard drive.  
The root  `/` and `swap` are controled using LVM (logical volume Manager) and device mapper (in `/dev/napper`). They are virtual block devices that are associated with a physical block device using device mapper.

```zsh
lsblk
# displays information only about SCSI block devices
lsblk -S
```

## `lspci`

The lspci command allows you to view the currently installed and recognized PCI and
PCIe cards on the Linux system.

## managing modules

- `/lib/modules/$(name -r)`: Hardware modules for each kernel.

- It includes a `modules.dep` file generated by `depmod` specifying modules dependency in the following format: each line: <modile_name>: dep1 dep2

- The modules the kernel will load at boot time are listed in the `/etc/modules` file, one
per line. Most hardware modules can be loaded dynamically as the system automatically detects hardware devices, so this file may not contain very many modules.

- `/etc/modules.conf`


```zsh
lsmod
insmod
modprobe
depmod
rmmod
moderob -r btusb
sudo modprobe -iv btusb
# insmod /lib/modules/3.13.0-63-generic/kernel/drivers/bluetooth/btusb.ko
```

# Storage basics

## Types of drives

While HDDs and SSDs differ in the way they store data, they both interface with the Linux
system using the same methods.  
There are **three main types of drive connections** that you’ll run into with Linux systems:

- **Parallel Advanced Technology Attachment (PATA)** connects drives using a parallel interface, which requires a wide cable. PATA supports **two** devices per adapter.
- **Serial Advanced Technology Attachment (SATA)** connects drives using a serial interface, but at a much faster speed than PATA. SATA supports up to **four** devices per adapter.
- **Small Computer System Interface (SCSI)** connects drives using a parallel interface, but with the speed of SATA. SCSI supports up to **eight** devices per adapter.

When you connect a drive to a Linux system, the Linux kernel assigns the drive device
a file in the `/dev` directory. That file is called a **raw device**, as it provides a path directly to
the drive from the Linux system. Any data written to the file is written to the drive, and
reading the file reads data directly from the drive.
For PATA devices, this file is named `/dev/hdx`, where x is a letter representing the indi-
vidual drive, starting with a. For SATA and SCSI devices, Linux uses `/dev/sdx` , where x is
a letter representing the individual drive, again, starting with a. Thus, to reference the first
SATA device on the system, you’d use `/dev/sda`, then for the second device `/dev/sdb`, and
so on.

## Drive partiotions

Partitions must be tracked by some type of indexing system on the drive. Systems that
use the old BIOS boot loader method use the master boot record (**MBR**) method for man-
aging disk partitions. This method supports only up to four primary partitions on a drive.
Each primary partition itself, however, can be split into multiple *extended partitions*.
Systems that use the UEFI boot loader method use the more advanced GUID Partition
Table (**GPT**) method for managing partitions, which supports up to 128 partitions on a
drive. Linux assigns the partition numbers in the order that the partition appears on the
drive, starting with number 1.  
Linux creates `/dev` files for each separate disk partition. It attaches the partition number
to the end of the device name and numbers the primary partitions starting at 1, so the first
primary partition on the first SATA drive would be `/dev/sda1`. MBR extended partitions
are numbered starting at 5, so the first extended partition is assigned the file `/dev/sda5`.

## `udev` Automatic Drive Detection

Most Linux systems now use the udev application. The udev program runs in the back-
ground at all times and automatically detects new hardware connected to the running
Linux system. As you connect new drives, USB devices, or optical drives (such as CD and
DVD devices), udev will detect them and assign each one a unique device filename in the
`/dev` directory.  
Another feature of the `udev` application is that it also creates **persistent device files**
for storage devices. When you add or remove a removable storage device, the /dev name
assigned to it may change, depending on what devices are connected at any given time.
That can make it difficult for applications to find the same storage device each time.
To solve that problem, the udev application uses the `/dev/disk` directory to create links
to the /dev storage device files based on unique attributes of the drive. udev creates four
separate directories for storing links:

- `/dev/disk/by-id`: Links storage devices by their manufacturer make, model, and serial
number
- `/dev/disk/by-label`: Links storage devices by the label assigned to them
- `/dev/disk/by-path` Links storage devices by the physical hardware port they are con-
nected to
- `/dev/disk/by-uuid`: Links storage devices by the 128-bit universally unique identifier
(UUID) assigned to the device  
With the udev device links, you can specifically reference a storage device by a perma-
nent identifier rather than where or when it was plugged into the Linux system.

```zsh
# ls /dev/disks/by-uuid
lrwxrwxrwx 1 root root  10 Aug 22 13:01 d64752be-5a5d-4720-ac82-3c03e5033af8 -> ../../sdb3
lrwxrwxrwx 1 root root  10 Aug 22 13:01 E4EAE0B1EAE080E2 -> ../../sda1
lrwxrwxrwx 1 root root  10 Aug 22 13:01 EAFE2955FE291B79 -> ../../sdf4
```
As new devices are detected, udev searches for a matching rule in the pre-defined rules stored in the directory `/etc/udev/rules.d/`. The most important rules are provided by the distribution, but new ones can be added for specific cases.

## advanced storage management techniques

All three technologies uses device mapper (`/dev/mapper`) to create a sort of virtual device.

### Logical Volume Manager

The Linux Logical Volume Manager (LMV) also utilizes the /dev/mapper dynamic device
directory to allow you to create virtual drive devices. You can aggregate multiple physical
drive partitions into virtual volumes, which you then treat as a single partition on your system.
The benefit of LVM is that you can add and remove physical partitions as needed to a
logical volume, expanding and shrinking the logical volume as needed. 
  
The Linux LVM layout

![lvm](./images/LVM.png)

For each physical partition, you must mark the partition type as the Linux LVM filesys-
tem type in fdisk or gdisk. Then, you must use several LVM tools to create and manage
the logical volumes:
- `pvcreate`: Creates a physical volume
- `vgcreate`: Groups physical volumes into a volume group
- `lvcreate`: Creates a logical volume from partitions in each physical volume  
The logical volumes create entries in the /dev/mapper directory, which represent the
LVM device you can format with a filesystem and use like a normal partition.
While the initial setup of an LVM is complicated, it does provide great benefits. If you run
out of space in a logical volume, just add a new disk partition to the volume.

### RAOD

Redundant Array of Inexpensive Disks (RAID) technology has changed the data storage
environment for most data centers. RAID technology allows you to improve data access
performance and reliability, as well as implement data redundancy for fault tolerance by com-
bining multiple drives into one virtual drive. Several versions of RAID are commonly used:

- RAID 0: Disk striping, which spreads data across multiple disks for faster access
- RAID 1: Disk mirroring, which duplicates data across two drives
- RAID 10: Disk mirroring and striping, which provides striping for performance and mirroring for fault tolerance
- RAID 4: Disk striping with parity, which adds a parity bit stored on a separate disk so that data on a failed data disk can be recovered
- RAID 5: Disk striping with distributed parity, which adds a parity bit to the data stripe so that it appears on all disks so that any failed disk can be recovered
- RAID 6: Disk striping with double parity, which stripes both the data and the parity bit so that two failed drives can be recovered  

The downside is that hardware RAID storage devices can be somewhat expensive
(despite what the I stands for), and they are often impractical for most home uses. Because
of that, Linux has implemented a software RAID system that can implement RAID fea-
tures on any disk system.
The `mdadm` utility allows you to specify multiple partitions to be used in any type of
RAID environment. The RAID device appears as a single device in the /dev/mapper direc-
tory, which you can then partition and format to a specific filesystem.

## Multipath

The Linux kernel now supports Device Mapper (DM) multipathing, which allows you
to configure multiple paths between the Linux system and network storage devices.
Multipathing aggregates the paths providing for increased throughout while all paths are
active, or fault tolerance if one of the paths becomes inactive.  
  
The Linux DM multipathing tools include:

- dm-multipath: The kernel module that provides multipath support
- multipath: A command-line command for viewing multipath devices
- multipathd: A background process for monitoring paths and activating/deactivating paths
- kpartx: A command-line tool for creating device entries for multipath storage devices

The DM multipathing feature uses the dynamic /dev/mapper device file directory in
Linux. Linux creates a /dev/mapper device file named mpathN for each new multipath stor-
age device you add to the system, where N is the number of the multipath drive. That file
acts as a normal device file to the Linux system, allowing you to create partitions and file-
systems on the multipath device just as you would a normal drive partition.

## partiotioning tools

### `fdisk`

The most common command-line partitioning tool is the fdisk utility. The fdisk program
allows you to create, view, delete, and modify partitions on any drive that uses the **MBR**
method of indexing partitions.  
The fdisk command is somewhat rudimentary in that it d**oesn’t allow you to alter the size of an existing partition**; all you can do is delete the existing partition and rebuild it
from scratch.


```zsh
# use device driver (not partition name)
sudo fdisk /dev/sda
```

## gdisk

If you’re working with drives that use the **GPT** indexing method, you’ll need to use the
`gdisk` program. The gdisk program identifies the type of formatting used on the drive. If the drive
doesn’t currently use the GPT method, gdisk offers you the option to convert it to a GPT
drive. 

## GNU parted

One of the selling features of the parted program is that it allows you to modify
existing partition sizes, so you can easily shrink or grow partitions on the drive.

```zsh
sudo gnu parted /dev/sda
```

There is also a GUI gparted (GNOME parted).

# Linux filesystem

## File System Hierarchy

- Linux uses a **virtual directory structure**. The virtual directory contains file paths from all the storage devices installed on the system,
consolidated into a single directory structure.

- Linux places physical devices in the virtual directory using **mount points.** A mount point
is a directory placeholder within the virtual directory that points to a specific physical
device.
  
- Linux **filesystem hierarchy standard (FHS)** A standard format has been defined for the Linux virtual directory. The FHS defines core directory
names and locations that should be present on every Linux system.  
  
Common Linux FHS directories:  


| Directory  | Description                                                         |
| :--------- | :------------------------------------------------------------------ |
| /boot      | Contains boot loader files used to boot the system                  |
| /etc       | Contains system and application configuration files                 |
| /home      | Contains user data files                                            |
| /media     | Used as a mount point for removable devices                         |
| /mnt       | Also used as a mount point for removable devices                    |
| /opt       | Contains data for optional third-party programs                     |
| /tmp       | Contains temporary files created by system users                    |
| /usr       | Contains data for standard Linux programs                           |
| /usr/bin   | Contains local user programs and data                               |
| /usr/local | Contains data for programs unique to the local installation         |
| /usr/sbin  | Contains data for system programs and data                          |
| /var       | Contains variable data files, including system and application logs |

## Common Filesystem Types: Linux Filesystems

- **btrfs:** pronounced as "butter fuss", "better F S"). A newer, high-performance filesystem that supports files up to 16 exbibytes
(EiB) in size, and a total filesystem size of 16 EiB. It also can perform its own form of
Redundant Array of Inexpensive Disks (RAID) as well as logical volume management
(LVM) and subvolumes. It includes additional advanced features such as built-in snap-
shots for backup, improved fault tolerance, and data compression on the fly.

- **ecryptfs:** The Enterprise Cryptographic Filesystem (eCryptfs) applies a Portable Oper-
ating System Interface (POSIX)–compliant encryption protocol to data before storing
it on the device. This provides a layer of protection for data stored on the device. Only
the operating system that created the filesystem can read data from it.

- **ext3:** Also called ext3fs, this is a descendant of the original Linux ext filesystem. It
supports files up to 2 tebibytes (TiB), with a total filesystem size of 16 TiB. It supports
journaling, as well as faster startup and recovery.

- **ext4:** Also called ext4fs, it’s the current version of the original Linux filesystem. It sup-
ports files up to 16 TiB, with a total filesystem size of 1 EiB. It also supports journaling
and utilizes improved performance features.

- **reiserFS**: Created before the Linux ext3fs filesystem and commonly used on older
Linux systems, it provides features now found in ext3fs and ext4fs. Linux has dropped
support for the most recent version, reiser4fs.

- **swap:** The swap filesystem allows you to create virtual memory for your system using
space on a physical drive. The system can then swap data out of normal memory into
the swap space, providing a method of adding additional memory to your system. This
is not intended for storing persistent data.  
  
The **default** filesystem used by most Linux distributions these days is **ext4fs**. The ext4fs
filesystem provides journaling, which is a method of tracking data not yet written to the
drive in a log file, called the **journal**. If the system fails before the data can be written to the
drive, the journal data can be recovered and stored upon the next system boot.  
Many Linux administrators have taken a liking to the newer **btrfs** filesystem. The btrfs
filesystem provides many advanced features, such as the ability to create a filesystem across
multiple devices, automatic data compression, and the ability to create subvolumes.

## Common Filesystem Types: Non-Linux Filesystems

 CIFS: The Common Internet Filesystem (CIFS) is a filesystem protocol created by
Microsoft for reading and writing data across a network using a network storage
device. It was released to the public for use on all operating systems.

- exFAT: The Extended File Allocation Table is a filesystem built on the original Micro-
soft FAT table architecture, and it’s commonly used to format **USB** devices and **SD**
cards.

- HFS: The Hierarchical Filesystem (HFS) was developed by Apple for its Mac systems.
Linux can also interact with the more advanced HFS+ filesystem.

- ISO-9660: The ISO-9660 standard is used for creating filesystems on CD-ROM
devices.

- NFS: The Network Filesystem (NFS) is an open source standard for reading and writ-
ing data across a network using a network storage device.

- NTFS: The New Technology Filesystem (NTFS) is the filesystem used by the Microsoft
NT operating system and subsequent versions of Windows. Linux can read and write
data on an NTFS partition as of kernel 2.6.x.

- VFAT: The Virtual File Allocation Table (VFAT) is an extension of the original Micro-
soft File Allocation Table (FAT) filesystem. It’s not commonly used on drives but is
often used for removable storage devices such as USB memory sticks.

## Creating Filesystems `mkfs`

The mkfs program is actually a front end to several individual tools for creating specific filesystems, such
as the mkfs.ext4 program for creating ext4 filesystems.
The beauty of the mkfs program is that you need to remember only one program name
to create any type of filesystem on your Linux system. Just use the -t option to specify the
filesystem type.  

```zsh
# it will wrase data in partion
# & createe all indexing and table files for the specified fs
sudo mkfs -t ext4 /dev/sdb1
```

## Manually mounting devices

The downside to the mount command is that it only temporarily mounts the device in
the virtual directory. When you reboot the system, you have to manually mount the devices
again.  
  
>`mount -f fstype device mountpoint`
  
```zsh
# displays all devices currently mounted
mount
cat  /etc/mtab # similar results to mount (either automatically mounted or manually mounted)

sudo mount -f ext4 /dev/sdb1 /media/usb1

# -o options: specify mounting options
sudo mount -o r,w /dev/sda /
# to unmount use umount
sudo umount <mnt_point | /dev/name>

# to mount a swap area
swapon
```

## `/etc/fstab`: Automatically mounting devices

For permanent storage devices, Linux maintains the `/etc/fstab` file to indicate which drive
devices should be mounted to the virtual directory at boot time. The /etc/fstab fi le is a
table that indicates the drive device fi le (either the raw file or one of its permanent udev file-
names), the mount point location, the filesystem type, and any additional options required
to mount the drive.  
Fileds of fstab:

- The block device: `UUID=80b496fa-ce2d-4dcf-9afc-bcaa731a67f1`. retrieve uuid from `ldblk` or `blkid`
- Second field - The mountpoint `UUID=80b496fa-ce2d-4dcf-9afc-bcaa731a67f1 /mnt/example`
- Third field - The filesystem type: retrive from `lsblk -d -f /dev/sda`
- Fourth field - Mount options. `default` for default values
  - rw (read-write);
  - suid (respect setuid and setgid bits);
  - dev (interpret characters and block devices on the filesystem);
  - exec (allow executing binaries and scripts);
  - auto (mount the filesystem when the -a option of the mount command is used);
  - nouser(make the filesystem not mountable by a standard user);
  - async (perform I/O operations on the filesystem asynchronously).
- Fifth field - Should the filesystem be dumped? The fifth field in each entry can be either 0 or 1. The value is used by the dump backup program (if installed) to know what filesystem should be dumped.
- Sixth field - `Fsck` order. The sixth field is used to establish the order by which another utility, fsck, should check filesystems on boot. The value of  1 must always be used for the root filesystem; for all the others we can use 2. If this value is not provided it defaults to 0, and the filesystem will not be checked.