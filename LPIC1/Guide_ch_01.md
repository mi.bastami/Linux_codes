# Chapter_1: exploring linux command  line tools

## Different shells

- Bash
- Dash: scripts runs faster than Bash.
- KornShell
- tcsh
- Z shell

## default shell and `/bin/sh`

Terminals: `tty` and `pts`  
`tty` stands for teletypewriter and is the ancient black screen terminal in unix like systems. In the desktop environment of Linux, **the terminal window and applications** such as x-term and Konsole are examples of *virtual teletypes*. But these are *emulated* entirely in software. They are called **pseudo-teletypes (PTS)**.  
In Linux, there is a pseudo-teletype multiplexor which handles the connections from all of the terminal window pseudo-teletypes (PTS). The multiplexor is the master, and the PTS are the slaves. The multiplexor is addressed by the kernel through the device file located at `/dev/ptmx`. The `tty` command will print the name of the device file that your pseudo-teletype slave is using to interface to the master. And that, effectively, is the number of your terminal window.  
The response shows we are connected to the device file at /dev/pts/0. Our terminal window, which is a software emulation of a teletype (TTY), is interfaced to the pseudo-teletype multiplexor as a pseudo-teletype (PTS).

```zsh
tty
# /dev/pts/o
```

This file is a symbolic link to another shell. In CentOS it links to `bash` and in ubuntu it links to `Dash`/bin/sh .  

```sh
readlink /bin/sh
# dash

# default shell
echo $SHELL

# check version
echo $BASH_VERSION
echo $ZSH_VERSION

# kernel name
uname   # linux
uname -r    # kernel revision (4.15.0-46-generic)
uname -a    #all info
#Linux Ubuntu1804 4.15.0-46-generic #49-Ubuntu SMP Wed Feb 6
#09:33:07 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux
```

## Quoting

Bash meta characters that needs to be qouted:

```bash
# metacharacters: * ? [ ] ' " \ $ ; & ( ) | ^ < >
echo Is Schrodinger\'s cat alive or dead\?
echo Is "Schrodinger's" cat alive or "dead?"
```

## Navigating directory structures

Files on a Linux system are stored within a single directory structure, called a **virtual directory**. The virtual directory contains files from all the computer’s storage devices and merges
them into a single directory structure. This structure has a single base directory called the **root directory**, often simply called **root**.

```zsh
pwd
# /home/milad
# one dir up from wd
cd ..

# cd to current working directory
cd
cd ~
cd $HOME

# return to recent wd
cd -
```

## Understanding Internal and External Commands

Within a shell, some commands that you type at the command line are part of (internal
to) the shell program. These internal commands are sometimes called **built-in commands**. Other commands are external programs, because they are not part of the shell.
You can tell whether a command is an internal or external program via the `type` com-
mand.  
A command may be available **both internally and externally** to the shell. In
this case, it is important to know their differences, because they may pro-
duce slightly different results or require different optio

```zsh
type echo
# echo is a shell builtin

type pwd
# pwd is a shell builtin

type uname
# uname is /usr/bin/uname  (external program)
```

## Environmental variables

- **BASH_VERSION:** Current Bash shell instance’s version number (Chapter 1)
- **SHELL**: address of current shell.
- **EDITOR:** Default editor used by some shell commands (Chapter 1)
- **GROUPS:** User account’s group memberships (Chapter 7)
- **HISTFILE:** Name of the user’s shell command history file (Chapter 1)
- **HISTSIZE:** Maximum number of commands stored in history file (Chapter 1)
- **HOME:** Current user’s home directory name (Chapter 1)
- **HOSTNAME:** Current system’s host name (Chapter 8)
- **LANG:** Locale category for the shell (Chapter 6)
- **LC_*:** Various locale settings that override LANG (Chapter 6)
- **LC_ALL:** Locale category for the shell that overrides LANG (Chapter 6)
- **LD_LIBRARY_PATH:** Colon-separated list of library directories to search prior to looking through the standard library directories (Chapter 2)
- **PATH:** Colon-separated list of directories to search for commands (Chapter 1)
- **PS1:** Primary shell command-line interface prompt string (Chapter 1)
- **PS2:** Secondary shell command-line interface prompt string
- **PWD:** User account’s current working directory (Chapter 1)
- **SHLVL:** Current shell level (Chapter 1)
- **TZ:** User’s time zone, if different from system’s time zone (Chapter 6)
- **UID:** User account’s user identification number (Chapter 7)
- **VISUAL:** Default screen-based editor used by some shell commands (Chapter 1)

## find a list of environmental variables

- `set`: to see a list of active ENV vars
- `env`: list of ENV vars and locally defind vars.
- `printenv SHELL`: display variables.

## `SHLVL`: determine if it is a subshell

You can determine whether your process is currently in a subshell by looking at the data stored in the **SHLVL environment variable**. A 1 indicates you are not in a subshell, because
subshells have higher numbers. Thus, if SHLVL contains a number higher than 1, this indicates you’re in a subshell.

## changing ENV vars

The following setting will not survive entering into a subshell:

```zsh
PS1="my new promt:
```

To preserve an environment variable’s setting, you need to employ the export com-
mand.

```zsh
export PS1="KeepPrompt: "

# or
PS1="KeepPrompt: "
export PS1

# EDITOR is null by defalut
export EDITOR=vim
unset EDITOR  # back it to null

## If the variable had a different definition before you modified it, it is
#best to change it back to its original setting instead of using unset on the
#variable
```

## getting help

- Using `man CMD`: man uses `less` by default. use `pageUP or pageDown` to navigate.
- `/` is used to search for a command or keyword in man pages. e.g. press `man uname` and search for `-r` by `/-r`.
- `man -k passwd`: use -k to search for a keyword.
- `apropos passwd`: apropos also searches the manual pages and descriptions.
- `man man`: a manual of using man.
- `man man.7`: manual of man section 7.
- `man -s 5 passwd` or `man 5 passwd` or `man -S 5 passwd` or `man passwd.5`: go the section 5 of passwd man page.
- `sudo mandb`: update man database.

## history

- `history`: a history of previously run commands, each with a number (larger number more  recent). works in bash and zsh.
- `!<number>`: refers to the command in the history list. In bash it execute it but in zsh it retiev the command.
- `!!`: most recent command (both bash & zsh)
- The **history list** is preserved between login sessions in the file designated by the `$HISTFILE` environment variable. `~/.zsh_history` or `~/..bash_history`
- The **history file** will not have commands you have used during your current login session. These commands are stored only in the history list.

- `history -a`: appends the current history list commands to the end of the history file.
- `history -n`: appends the history file commands from the current Bash shell session to the current history list.
- `history -r`: -r overwrites the current history list commands with the commands stored in the history file.
-To clear history: first, `history -c` to clear current history list. then, `history -w` to overwrite the blanked list to the histfile.

## default text rditors

You can change your account’s standard editor via the EDITOR and VISUAL environment
variables. The `EDITOR` variable was originally for line-based editors, such as the old ed utility. The `VISUAL` variable is for **screen-based editors** (text editors that take up the whole screen, such as **nano**, **emacs**, and **vim**).

Change your standard editor to your desired editor by typing, for example, `export EDITOR=nano` at the command line. Do the same for the VISUAL environment variable. Even better, add these lines to an environment file.

- In `nano` documentation: `M-K` means metacharacter (ctrl or esc) + keyword
- `vi` vs `vim`: The vi editor was a Unix text editor, and when it was rewritten as an open source tool, it was improved. Thus, vim stands for “vi improved.”
- In ubuntu, `vim` is not installed by default. Instead, `/bin/vi` is a symbolic link to `/bin/vim.basic` or `/bin/vim.tiny`.

### vim

`vimtutor` is a bultin vim tutorial.
vim modes:

- **Command Mode (normal mode):**  This is the mode vim uses when you first enter the buffer area; it is sometimes called normal mode. Here you enter keystrokes to enact commands.
- **Insert Mode:** Insert mode is also called edit or entry mode. Enter: pressing I, exit the mode: ESC
- **Ex Mode** This mode is sometimes also called colon commands. For example, to leave the vim editor and not save any changes you type `:q` and press the Enter key.

Usefull commands:

- `?` forward search, `/` bakward search. press `n` to go to the next item.

**vim navigation commands:**  
| Keystroke(s) | Description                                         |
| :----------- | :-------------------------------------------------- |
| w            | Move cursor forward one word to front of next word. |
| e            | Move cursor to end of current word.                 |
| b            | Move cursor backward one word.                      |
| ^            | Move cursor to beginning of line.                   |
| $            | Move cursor to end of line.                         |
| gg           | Move cursor to the file’s first line.              |
| G            | Move cursor to the file’s last line.               |
| n G          | Move cursor to file line number n .                 |
| Ctrl+B       | Scroll up almost one full screen.                   |
| Ctrl+F       | Scroll down almost one full screen.                 |
| Ctrl+U       | Scroll up half of a screen.                         |
| Ctrl+D       | Scroll down half of a screen.                       |
| Ctrl+Y       | Scroll up one line.                                 |
| Ctrl+E       | Scroll down one line.                               |
  
**Vim edit cammands:**

| Keystroke(s) | Description                                                 |
| :----------- | :---------------------------------------------------------- |
| a            | Insert text after cursor.                                   |
| A            | Insert text at end of text line.                            |
| dd           | Delete current line.                                        |
| dw           | Delete current word.                                        |
| i            | Insert text before cursor.                                  |
| I            | Insert text before beginning of text line.                  |
| o            | Open a new text line below cursor, and move to insert mode. |
| O            | Open a new text line above cursor, and move to insert mode. |
| p            | Paste copied text after cursor.                             |
| P            | Paste copied (yanked) text before cursor.                   |
| yw           | Yank (copy) current word.                                   |
| yy           | Yank (copy) current line.                                   |

- `d3w`: delete 3 words.
- `d3d`: delete 3 lines. (similar to `3dd`)
- `y$`: copy (yank) text from cursor to the end of line. go to position and press `p` to paste.
  
**Commonly used EX mode commands:**

| :! command  | Execute shell command and display results, but don’t quit editor.   |
| :---------- | :------------------------------------------------------------------- |
| :r! command | Execute shell command and include the results in editor buffer area. |
| :r file     | Read file contents and include them in editor buffer area.           |


**Commands to save results:**

| Mode | Keystrokes | Description                                                        |
| :--- | :--------- | :----------------------------------------------------------------- |
| Ex   | :x         | Write buffer to file and quit editor.                              |
| Ex   | :wq        | Write buffer to file and quit editor.                              |
| Ex   | :wq!       | Write buffer to file and quit editor (overrides protection).       |
| Ex   | :w         | Write buffer to file and stay in editor.                           |
| Ex   | :w!        | Write buffer to file and stay in editor (overrides protection).    |
| Ex   | :q         | Quit editor without writing buffer to file.                        |
| EX   | :q!        | Quit editor without writing buffer to file (overrides protection). |
| cmd  | ZZ         | Write buffer to file and quit editor.                              |


## File-Combining Commands: `cat`

- `cat`
- `cat numbers.txt random.txt`:  comcatanating files
- `zcat` and `bzcat` and `xzcat` for compressed files.
- `bat`: should be installed separately. It is **cat with wings**.
- `fd`: fromauthor of bat. faster and better find.
- `batextras`: more utilities for bat like `batman` for colurfull manuals.
- `ripgrep` or `rg` in short is a faster grep.

| -A  | --show-all         | Equivalent to using the option -vET combination.                                   |
| :-- | :----------------- | :--------------------------------------------------------------------------------- |
| -E  | --show-ends        | Display a $ when a newline linefeed is encountered.                                |
| -n  | --number           | Number all text file lines and display that number in the output.                  |
| -s  | --squeeze-blank    | Do not display repeated blank empty text file lines.                               |
| -T  | --show-tabs        | Display a ^I when a tab character is encountered.                                  |
| -v  | --show-nonprinting | Display nonprinting characters when encountered using either ^ and/or M- notation. |

## File-Transforming Commands: uncovering with `od` & `hexdump`

It allows you to display a file’s contents in octal (base 8), hexadecimal (base 16), decimal (base 10), and ASCII. The fi rst column of the od command’s output is an index number for each displayed line. For example, in Listing 1.31, the line beginning with 0000040 indicates that the third line starts at octal 40 (decimal 32) bytes in the fi le.


```zsh
# cat fourtytwo.txt
42
fourty two
quarante deux
zweiundvierzig
forti to
#od fourtytwo.txt
0000000 031064 063012 072557 072162 020171 073564 005157 072561
0000020 071141 067141 062564 062040 072545 005170 073572 064545
0000040 067165 073144 062551 075162 063551 063012 071157 064564
0000060 072040 005157
0000064

#od -cb fourtytwo.txt
# hexdump -b -c
#show each character along with its octal byte location.

od -An -c input.txt
# in character format without byte offset info (first column)
```

## File-Transforming Commands: splitting with `split`

Discussed in Bash.md.

## File  formatting commands: numbering  lines with `nl`

Another useful file-formatting command is the nl utility (number line utility). This little
command allows you to number lines in a text file in powerful ways. It even allows you to use **regular expressions** (covered later in this chapter) to designate which lines to number.  
If you do not use any options with the nl utility, it will **number only non-blank text lines**. 

```zsh
#nl ContainsBlankLines.txt
1 Alpha
2 Tango

3 Bravo
4 Echo

5 Foxtrot

#-ba swithch to number all lines
#nl -ba ContainsBlankLines.txt
```

## File viewing commands: `less` & `tail`

When launched with +F, less will behave pretty much the same as `tail -f`

```zsh
less +F /var/log/messages

# from line 42 to the the end
tail -n +42 /etc/passwd
# usefull for log files (--follow)
tail -f
```

>Some log files have been replaced on various Linux distributions, and now
the messages are kept in a **journal file** managed by `journald`. To watch
messages being added to the journal file, use the `journalctl --follow`
command.

## File-Summarizing:  `wc`

An interesting wc option for **troubleshooting configuration files** is the `-L` switch. Generally speaking, line length for a configuration file will be under 150 bytes, though there are exceptions. Thus, if you have just edited a configuration file and that service is no longer working, check the file’s longest line length. A longer than usual line length indicates you might have accidently merged two configuration file lines.

## File summarizing: `cut`

**Text File Records**: A text file record is a single-file line that ends in a newline linefeed,
which is the A`SCII character LF`. You can see if your text file uses this end-of-line character via the `cat -E` command. It will display every newline linefeed as a `$`. If your text file
records end in the `ASCII character NUL`, you can also use cut on them, but you must use the `-z` option.  
**Text File Record Delimiter:** For some of the cut command options to be properly used,
fields must exist within each text file record. These fields are not database-style fields but
instead data that is separated by some delimiter. A delimiter is one or more characters that create a boundary between different data items within a record. A single space can be a delimiter. The password file, `/etc/passwd`, uses colons ( :) to separate data items within a
record.

## Discovering Repeated Lines with `uniq`

The uniq utility will find repeated text lines only if they come right after one another. Data sould be sorted first.

## MD5 and SHA algorithms

SHA is more secure. sha512sum is used for hash salted passwords in the `/etc/shadow`
file on Linux

```zsh
# ls -1 /usr/bin/sha???sum
/usr/bin/sha224sum
/usr/bin/sha256sum
/usr/bin/sha384sum
/usr/bin/sha512sum  # most secure
```

## Stream Editing with `sed`

```zsh
sed `s/cake/donute/` cake.txt
sed 's/cake/donout/g' cake.txt  # substitude cake in each line globally
sed '/Christine/d' cake.txt     # delete all line containing crhristine
sed -r '/^Christine/d' cake.txt # (-r: regex-extended) delete lies starting with Christin
sed '4cI am a new line' cake.txt change entire line 4
sed -e 's/cake/donut/g ; s/like/love/g'  # multiple expression
sed -e 's/cake/donut/g' -e 's/like/love/g' # multiple expression
```