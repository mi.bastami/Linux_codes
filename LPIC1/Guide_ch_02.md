# Managing Software and Processes

## Package managment vs Repository managment

Tracking software packages on a Linux system is called **package management**. Linux
implements package management by using a database to track the installed packages
on the system. The p**ackage management database** keeps track of not only what pack-
ages are installed but also the exact files and file locations required for each application.

Two common package managment:

* Red Hat package management (RPM)
* Debian package management (Apt)

Each package management system uses a different method of tracking application pack-
ages and files, but they both track **similar information:**

* App files & folders
* version
* dependencies

To handle dependencies, each Linux distribution has its own central clearinghouse of
packages, called a **repository**. The repository contains software packages that have been tested and known to install and work correctly in the distribution environment. There are also additional tools for working with package repositories. For Red Hat based distros it's `yum`.

## RPM

The Red Hat Linux distribution, along with other Red Hat–based distros such as **Fedora**
and **CentOS**, use RPM. In addition, there are other distributions that are not Red Hat based, such as **openSUSE** and OpenMandriva Lx, that employ RPM as well.
  
RPM package files have an .rpm fi leextension and follow this naming format:  
>PACKAGE-NAME-VERSION-RELEASE.ARCHITECTURE.rpm

```zs
docker-1.13.1-94.gitb2f74b2.el7.centos.x86_64.rpm
emacs-24.3-22.el7.x86_64.rpm
openssh-7.4p1-16.el7.x86_64.rpm
zsh-5.0.2-31.el7.x86_64.rpm
```

**RELEASE** The RELEASE is also called the build number . It represents a smaller program modification than does the version number. In addition, due to the rise of continuous software delivery models, you often find **version control system** (VCS) numbers listed in the release number after a dot. Examples include 22 and `94.gitb2f74b2`.  
Some distros include the **distribution version** in the build number. For example, you find `el7` (Red Hat Enterprise Linux v7) or `fc29` (Fedora, formerly called Fedora Core, v29)
after a dot.

**ARCHITECTURE** This is a designation of the CPU architecture for which the software
package was optimized. Typically you’ll see `x86_64` listed for 64-bit processors. Sometimes `noarch` is used, which indicates the package is architecturally neutral. Older CPU architecture designations include i386 (x86), ppc (PowerPC), and i586 and i686 (Pentium).  
There are two types of RPM packages: source and binary. A source RPM has src as its ARCHITECTURE in the RPM filename.

### downloading RPM files

If you want to obtain copies of RPM files on a Red Hat–based distro such as CentOS or Fedora:

```zsh
yumdownloader emacs
# download emacs rpm to current directory
```

In openSUSE

```zsh
zypper install -d package-name
# download rpm to  /var/cache/zypp/packages/
```

### `rpm` program

Does not know dependency. Only works when you have rpm file (should be downloade manually).
>rpm ACTION [OPTION] PACKAGE-FILE
  
To use the rpm command, you must have the .rpm package file downloaded onto your system. While you can use the `-i` action to install packages, it’s more common to use the `-U` action, which installs the new package or upgrades the package if it’s already installed.  
Adding the `-vh` option is a popular combination that shows the progress of an update
and what it’s doing.

```zsh
# -q for query installed packages
# retireive a list of all installed pkgs that use rpm
rpm -qa
# detailed information about installed pkg
rpm -qi termcap
# full name of pkg (or detailed info)
rpm -qa termcap
# Determining an RPM package’s dependencies
rpm -qR termcap
# Determining configuration filenames that belong to an RPM package
rpm -qc termcap
# checking dependency of an rpm file
rpm -qRp zsh-5.0.2-31.el7.x86_64.rpm
# check to which pkg de a file belongs
rpm -q --whatprovides /usr/bin/zsh
# erasing a pkg
rpm -e zsh
# converting rpm to cpio archive (to extract files from rpm file)
rpm2cpio emacs-24.3-22.el7.x86_64.rpm > emacs.cpio
# extract cpio archive to wd (-i copy-in -d make direcroreis)
cpio -idv < emacs.cpio
# output
./usr/bin/emacs-24.3
./usr/share/applications/emacs.desktop
./usr/share/applications/emacsclient.desktop
./usr/share/icons/hicolor/128x128/apps/emacs.png
./usr/share/icons/hicolor/16x16/apps/emacs.png
./usr/share/icons/hicolor/24x24/apps/emacs.png
./usr/share/icons/hicolor/32x32/apps/emacs.png
./usr/share/icons/hicolor/48x48/apps/emacs.png
./usr/share/icons/hicolor/scalable/apps/emacs.svg
```

### Verifying a RPM pkg

Each file that has a discrepancy is listed.  the /bin/zsh file has had both its owner and group changed, and the modification time stamp differs from the one in the package database. The /etc/zlogin fi le is
a zsh package configuration file (indicated by c), and its modification time stamp has also been changed.  The /etc/zprofile confi guration file is missing.

```zsh
# validate a pkg
rpm -V zsh
# output:
.....UGT. /bin/zsh      #  owner and group changed, modification time stamp differs
.......T. c /etc/zlogin
missing c /etc/zprofile
```

Verify action response codes for the rpm command

| Code    | Description                                  |
| :------ | :------------------------------------------- |
| ?       | Unable to perform verification tests         |
| 5       | Digest number has changed                    |
| c       | File is a configuration file for the package |
| D       | Device number (major or minor) has changed   |
| G       | Group ownership has changed                  |
| L       | Link path has changed                        |
| missing | Missing file                                 |
| M       | Mode (permission or file type) has changed   |
| P       | Capabilities have changed                    |
| S       | Size of file has changed                     |
| T       | Time stamp (modification) has changed        |
| U       | User ownership has changed                   |

## `yum` for Red Hat repository

List of repositories stored in `/etc/yum.repos.d/` directory that contains several `.repo` files. Each file in the yum.repos.d folder contains information on a repository, such as its
URL address and the location of additional package files within the repository.
  
Grouplist: One nice feature of yum is the ability to group packages together for distribution. Instead of having to download all of the packages needed for a specific environment (such as for a web server that uses the Apache, MySQL, and PHP servers), you can download the package group that bundles the packages together. Employ the `yum grouplist`
command to see a list of the various package groups available, and use yum groupinstall group-package-name for an even easier way to get packages installed on your system.

```zsh
yum install emacs
# list of available package lists
yum grouplist
# install pkg group
yum grouplist <group-NAme>

rpm -V emacs # detect a missing file
yum reinstall emacs # reinstall the package
# removing a pkg
yum remove emacs
#-y: ayumatically answer to yes to all questions
# update packages and kernel to the latest version available and resolvable
yum -y update
# equivalent to (in fedora 22+)
dnf -y upgrade
```

`dnf` is an improved alternative of yum which is installed in new versions of fedora.

### installing new repositories

1. editing `\etc\yum.conf` or adding a repo manually to `/etc/yum.repos.d`: not recomended.
2. Adding repository through yum or rpm: recomended.

## `ZYpp` in openSUSE

openSUSE uses .rpm system but does not use yum or dnf. Instead it uses zypp (`libzypp`) and `zypper` command.

| Command       | Description                                                                                                                                         |
| :------------ | :-------------------------------------------------------------------------------------------------------------------------------------------------- |
| help          | Displays overall general help information or help on a specified command                                                                            |
| install       | Installs the specified package                                                                                                                      |
| info          | Displays information about the specified package                                                                                                    |
| list-updates  | Displays all available package updates for installed packages from the repository                                                                   |
| lr            | Displays repository information                                                                                                                     |
| packages      | Lists all available packages or lists available packages from a specified repository                                                                |
| what-provides | Shows to what package a file belongs                                                                                                                |
| refresh       | Refreshes a repository’s information                                                                                                               |
| remove        | Removes a package from the system                                                                                                                   |
| search        | Searches for the specified package(s)                                                                                                               |
| update        | Updates the specified package(s) or if no package is specified, updates all currently installed packages to the latest version(s) in the repository |
| verify        | Verifies that installed packages have their needed dependencies satisfied                                                                           |
|               |                                                                                                                                                     |

```zsh
sudo zypper install emacs
zypper info emacs # pkg info
# Determining to which package a file belongs
zypper what-provides /usr/bin/emacs
# seaching for a pkg
zypper se nmap
sudo zypper remove emacs
```

# Debian Packages

>PACKAGE-NAME-VERSION-RELEASE_ARCHITECTURE.deb
  
This filenaming convention for .deb packages is very similar to the .rpm fi le format.
However, in the ARCHITECTURE, you typically find `amd64`, denoting it was optimized for the *AMD64/Intel64* CPU architecture. Sometimes `all` is used, indicating the package is architecturally neutral.

```
docker_1.5-1build1_amd64.deb
emacs_47.0_all.deb
openssh-client_1%3a7.6p1-4ubuntu0.3_amd64.deb
vim_2%3a8.0.1453-1ubuntu1_amd64.deb
zsh_5.4.2-3ubuntu3.1_amd64.deb
```

To download .deb file of a pkg

```zsh
sudo apt-get download vim
```

## The dpkg Command Set

The core tool to use for handling .deb fi les is the dpkg program, which is a command-line
utility that has options for installing, updating, and removing .deb package files on your Linux system.

| Short | Long             | Description                                                         |
| :---- | :--------------- | :------------------------------------------------------------------ |
| -c    | --contents       | Displays the contents of a package file                             |
| -C    | --audit          | Searches for broken installed packages and suggests how to fix them |
| N/A   | --configure      | Reconfigures an installed package                                   |
| N/A   | --get-selections | Displays currently installed packages                               |
| -i    | --install        | Installs the package; if package is already installed, upgrades it  |
| -I    | --info           | Displays information about an uninstalled package file              |
| -l    | --list           | Lists all installed packages matching a specified pattern           |
| -L    | --listfiles      | Lists the installed files associated with a package                 |
| -p    | --print-avail    | Displays information about an installed package                     |
| -P    | --purge          | Removes an installed package, including configuration files         |
| -r    | --remove         | Removes an installed package but leaves the configuration files     |
| -s    | --status         | Displays the status of the specified package                        |
| -S    | --search         | Locates the package that owns the specified files                   |

```zsh
# .deb info
dpkg -I zsh_5.4.2-3ubuntu3.1_amd64.deb
# installing a .deb file (or upgrade old version)
sudo dpkg -i zsh_5.4.2-3ubuntu3.1_amd64.deb
# view coontent of .bed file
dpkg -c # --content
# pkg status
dpkg -s zsh
# list all installed pkgs (first column: status)
# iU status means installed Unpacked (e.g. missing dep)
dpkg -l
#  The -r action removes the package but keeps any configuration and data fi les associated with the package installed.
# best for reinstalling and not reconfiguring
dpkg -r zsh
# purge (remove the entire package)
dpkg -P zsh
```

## The APT suite

The Advanced Package Tool (APT) suite is used for working with Debian repositories. This includes:

1. the `apt-cache` program: that provides information about the package database, and
2. the `apt-get` program that does the work of installing, updating, and removing packages.

The APT suite of tools relies on the `/etc/apt/sources.list` file to identify the locations of where to look for repositories.

`apt` tool (is an improved version, This utility should not be confused with the APT suite or the Python wrapper used on Linux Mint by the same name.). It is so popular and is included in LPIC1.

### Using apt-cache

Here are a few useful command options in the apt-cache program for displaying informa-
tion about packages:

- depends: Displays the dependencies required for the package
- pkgnames: Shows all the packages installed on the system
- search: Displays the name of packages matching the specified item
- showpkg: Lists information about the specified package
- stats: Displays package statistics for the system
- unmet: Shows any unmet dependencies for all installed packages or the specified installed package

```zsh
# check for an installed debian pkg
apt-cache pkgnames | grep ^nano

# search for a pkg
apt-cache search zsh

# pkg info (installed or not)
apt-cache showpkg zsh
```

### Using apt-get

| Action          | Description                                                               |
| :-------------- | :------------------------------------------------------------------------ |
| autoclean       | Removes information about packages that are no longer in the repository   |
| check           | Checks the package management database for inconsistencies                |
| clean           | Cleans up the database and any temporary download files                   |
| dist-upgrade    | Upgrades all packages, but monitors for package dependencies              |
| dselect-upgrade | Completes any package changes left undone                                 |
| install         | Installs or updates a package and updates the package management database |
| remove          | Removes a package from the package management database                    |
| source          | Retrieves the source code package for the specified package               |
| update          | Retrieves updated information about packages in the repository            |
| upgrade         | Upgrades all installed packages to newest versions                        |

The `dist-upgrade` action provides a great way to keep your entire Debian-
based system up-to-date with the packages released to the distribution
repository. Running that command will ensure that your packages have
all the security and bug fixes installed and will not break packages due to
unmet dependencies. However, that also means that you fully trust the
distribution developers to put only tested packages in the repository. Occa-
sionally a package may make its way into the repository before being fully
tested and cause issues.

## Adding deb and ppa repository

An APT repository is a network server or a local directory containing deb packages and metadata files that are readable by the APT tools.  
In Ubuntu and all other Debian based distributions, the apt software repositories are defined in the `/etc/apt/sources.list` file or in separate files under the `/etc/apt/sources.list.d/` directory.  
The names of the repository files inside the `/etc/apt/sources.list.d/` directory must end with `.list`.  
The general syntax of the `/etc/apt/sources.list` file takes the following format:

>deb http://repo.tld/ubuntu distro component...

- The first entry in the line defines the type of the archive. The archive type can be either **deb** or **deb-src**. Deb implies that the repository contains .deb packages while deb-src implies source packages.
- The second entry is the **repository URL**.
- The third entry specifies the **distribution code name**, such as beaver, xenial and so on.
- The last entries are the **repository components** or **categories**. The default Ubuntu repositories are split into four components - main, restricted, universe and multiverse. Generally, third-party repositories have only one category.
  
Most repositories are providing a **public key** (gpg key) to authenticate downloaded packages which need to be downloaded and imported.

### `add-apt-repository`

It is a Python script that allows you to add an APT repository to either `/etc/apt/sources.list` or to a separate file in the `/etc/apt/sources.list.d` directory and is included in `software-properties-common` package.  
Repository can be:

1. Regular repository (e.g. deb. key should be imported manualy)

    ```zsh
    add-apt-repository [options] repository
    # first import key
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
    # add repository
    sudo add-apt-repository 'deb [arch=amd64] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse'
    # install pkg
    sudo apt install mongodb-org
    # remove repository
    sudo add-apt-repository --remove 'deb [arch=amd64] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse'

    sudo add-apt-repository ppa:alexlarsson/flatpak
    ```

2. **Personal Package Archives (PPA)**: keys are imported automatically
    >`ppa:<user>/<ppa-name>`
    Personal Package Archives (PPA) is a service that allows users to upload Ubuntu source packages that are built and published with **Launchpad** as an apt repository.
    
    When adding a PPA repository the add-apt-repository command creates a new file under the `/etc/apt/sources.list.d/` directory.

    ```zsh
    sudo add-apt-repository ppa:jonathonf/ffmpeg-4
    ```

### Manually Adding Repositories

Manually edit the /etc/apt/sources.list file and add the apt repository line to the file.

```zsh
echo "deb https://apache.bintray.com/couchdb-deb $(lsb_release -cs) main" | sudo tee -a /etc/apt/sources.list
# manually import key
curl -L https://couchdb.apache.org/repo/bintray-pubkey.asc | sudo apt-key add -
```


## Reconfiguring Packages: `dpkg-reconfigure` and `debconf-show`

1. It’s a good idea to employ the `debconf-show` utility, too. This tool allows you to view the package’s configuration.
  
2. If the package required configuration when it was installed, you are in luck! Instead of purging the package and reinstalling it, you can employ the handy `dpkg-reconfigure` tool. It would be worthwhile to run the `debconf-show` command and record the settings before and after running the dpkg-reconfigure utility.

```zsh
sudo debconf-show cups
# cupsys/backend: lpd, socket, usb, snmp, dnssd
# cupsys/raw-print: true
sudo dpkg-reconfigure cups
```

## `snapd` daemon and `snappy`

Is a **new package management system** that employs this new and exciting method is `Snappy` for the *Ubuntu* distribution. It uses the `.snap` file extension. The packages are called snap packages , and using Snappy requires installation of the `snapd` daemon and program is `snap`.

```zsh
sudo apt-get install snapd
sudo snap install multipass
```

## `Flatpak`

Another interesting trend in package management includes not only what
is in the package but how the package’s application is executed. Using
virtualization concepts (covered in Chapter 5), `Flatpak` combines package
management, software deployment, and **application sandboxing** (isolated
in a separate environment) all together in one utility. It provides all the
needed package dependencies as well as a sandbox for application execu-
tion. Thus, you can run the application in a confined **virtualized environment**, protecting the rest of your system from any application effects.

# Libraries

## Managing Shared Libraries

A system library is a collection of items, such as program *functions*. The benefit of splitting functions into separate library files is that multiple applications
that use the same functions can share the same library fi les. These fi les full of functions make it easier to distribute applications.  
  
Linux supports two different flavors of libraries:

1. **static libraries** (also called *statically linked libraries* ) that are copied into an application when it is compiled. 
2. **shared libraries** (also called **dynamic libraries** ) where the library functions are copied into memory and bound to the application when the program is launched. This is called **loading a library**.  **Dynamic linked libraries (DLLs)** in windows are similar to linux shared libraries.  
  
naming convention:
> libLIBRARYNAME.so.VERSION

## Locating Library Files

When a program is using a shared function, the system will search for the function’s library
file in a specific order; looking in directories stored within the

1. LD_LIBRARY_PATH environment variable
2. Program’s PATH environment variable
3. /etc/ld.so.conf.d/ folder
4. /etc/ld.so.conf file
5. /lib*/ and /usr/lib*/ folders
  
Be aware that the order of #3 and #4 may be fl ip-flopped on your system. This is
because the `/etc/ld.so.conf` file loads configuration files from the `/etc/ld.so.conf.d/` folder.  
If another library is located in the `/etc/ld.so.conf` file and it is listed above the
include operation, then the system will search that library directory before the fi les in the `/etc/ld.so.conf.d/` folder.


```zsh
# cat /etc/ld.so.conf
include ld.so.conf.d/*.conf
# ls -1 /etc/ld.so.conf.d/
dyninst-x86_64.conf
kernel-3.10.0-862.9.1.el7.x86_64.conf
kernel-3.10.0-862.el7.x86_64.conf
kernel-3.10.0-957.10.1.el7.x86_64.conf
libiscsi-x86_64.conf
mariadb-x86_64.conf

# cat /etc/ld.so.conf.d/mariadb-x86_64.conf
/usr/lib64/mysql
# ls /usr/lib64/mysql
libmysqlclient.so.18 libmysqlclient.so.18.0.0 plugin
```

It is important to know that the `/lib*/` folders, such as `/lib/` and `/lib64/`,
are for libraries needed by **system utilities** that reside in the `/bin/` and `/sbin/` directories, whereas the  `/usr/lib*/` folders, such as `/usr/lib/` and
`/usr/lib64/`, are for libraries needed by **additional software**, such as data-
base utilities like MariaDB.

## dynamic loading & dynamic linker

When a program is started, the `dynamic linker` (also called the `dynamic linker/loader`) is
responsible for finding the program’s needed library functions. After they are located, the
dynamic linker will copy them into memory and bind them to the program.
Historically, the dynamic linker executable has a name like `ld.so` and `ld-linux.so*`,
but its actual name and location on your Linux distribution may vary. You can employ the
`locate` utility (covered in more detail in Chapter 4) to find its actual name and location.

```zsh
# locate ld-linux
/usr/lib64/ld-linux-x86-64.so.2
/usr/share/man/man8/ld-linux.8.gz
/usr/share/man/man8/ld-linux.so.8.gz
# Manually load & execute a program & its libraries
/usr/lib64/ld-linux-x86-64.so.2 /usr/bin/echo "Hello World"
```

## Library Management: library cache file

The l**ibrary cache** is a catalog of library directories and all the various libraries contained
within them. The system reads this cache file to quickly find needed libraries when it is
loading programs. This makes it much faster for loading libraries than searching through
all the possible directory locations for a particular required library file.
When new libraries or library directories are added to the system, this library cache file
must be updated. However, it is not a simple text file you can just edit. Instead, you have to
employ the `ldconfig` command.
Fortunately, when you are installing software via one of the package managers, the
ldconfig command is typically run **automatically**. Thus, the library cache is updated
without any needed intervention from you. Unfortunately, you’ll have to **manually run the ldconfig** command for any applications you are **developing yourself**.
  
Manually adding an under developement library:

1. modify the LD_LIBRARY_PATH environment variable `export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/devops/library/`
2. After testing, put the  library in appropriate place e.g. `/usr/lib/`
3. make a configuration file in `/etc/ld.so.conf.d` pointing to the directory of library.
4. updating library cache by manually running `ldconfig`.

```zsh
# see what libraries are cataloged
ldconfig -v 2> /dev/null | grep libmysqlclient
# output: libmysqlclient.so.18 -> libmysqlclient.so.18.0.0
```

## library Management: shared library dependencies

The ldd utility output shows the echo program requires two external library fi les: the
standard linux-vdso.so.1 and libc.so.6 fi les. The ldd utility also shows where those
files are found on the Linux system, which can be helpful when troubleshooting issues with applications involving their library files. Sometimes **a library is dependent on another library**. So when you are troubleshooting a missing library file, you may need to use the ldd command on the libraries listed for the application in order to get to the root of the problem.

```zsh
whereis echo
ldd /bin/echo
#    linux-vdso.so.1 => (0x00007ffd3bd64000)
#    libc.so.6 => /lib64/libc.so.6 (0x00007f7c39eff000)
#    /lib64/ld-linux-x86-64.so.2 (0x00007f7c3a2cc000)
ldd libc.so.6
```

# Managing Processes:

## Examining Process Lists

When a Linux system first boots, it starts a special process called the `init` process.
The init process is the core of the Linux system; it runs scripts that start all of the other processes running on the system

### Viewing Processes with `ps`

`ps` has three switch flavors:

1. unix style: `-f`
2. BSD  style: `f`
3. GNNU style: `--file`

- By **default**, the ps program shows only the processes that are running in the **current user shell** (i.e. same effective user ID [EUID]  and same TTY (terminal))
- `ps a` vs `ps -a`: in BSD style only CMD name is printed, in unix style cammand arguments are also shown.

```zsh
# see every process on the system using standard syntax:
    ps -e    # all ps (same as -A)
    ps -ef   # full-format (UID, PPID, STIME)
    ps -eF   # full column
    ps -ely  # long, without flag (S, UID, NI, PRI)
# ps tree
    ps -ejH
    ps -e --forest
    ps -f --forest -C sshd # forest for specific command
    ps -ef --forest | grep -v grep | grep sshd
# To see every process with a user-defined format:
    ps -eo pid,tid,class,rtprio,ni,pri,psr,pcpu,stat,wchan:14,comm
    ps axo stat,euid,ruid,tty,tpgid,sess,pgrp,ppid,pid,pcpu,comm
    ps -Ao pid,tt,user,fname,tmout,f,wchan
# To see every process running as root (real & effective ID) in user format:
    ps -U root -u root u
# To see every process on the system using BSD syntax:
ps aux  (%CPU %MEM, STAT)
# Print only the name of PID 42 (= without title):
    ps -q 42 -o comm=
# select by CMD name
    ps -C zsh
# effective group ID (EGID)
    ps -g GIDList
# real group ID (EGID)
    ps -G GIDList
    ps -fG 48
    ps -fG apache
# every ps except
    ps -N <pid>
# ps with pid
    ps -p 1,2
# ppid
    ps -f --ppid 1154
# ps in state of running
    ps -r
# ps from ttylist (terminals)
    ps -t ttylist
    ps -t 1,3
# ps from current terminal
    ps -T
# ps from effective user (username or EUID)
    ps -u milad
# real username or RUID
    ps _-U
# Usually this is better to get all ps from a user
    ps -u Christine -U Christine
# repeat header lines, one per page of output
    ps --headers
# no headers
    ps --no-headers

# ps & grep
# will show grep ps in output
ps aux | grep firefox
ps -C firefox
# to prevent
ps aux | grep -v | grep firefox
ps aux | grep '[f]irfox'

# list all format specifiers
ps L
# use above info for custom output)
ps -eo pid,ppid,user,cmd
ps -p 1154 -o pid,ppid,fgroup,ni,lstart,etime

# display parrent & child prrocesses
ps -C sshd
# Find all PIDs of all instances of a process (= removes header)
ps -C httpd -o pid=
#find high mem consuming ps
ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem | head
# monitor ps with watch
watch -n 1 'ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem | head'

## PID of current shell
echo $$
## PPID of current shell
echo $PPID
## getting pids
pgrep -u milad top

# pstree:
## show process's group ID
pstree -g
## show PID
pstree -p
# highlight specific PID
pstree -H <PID>
# only the parent and child info for a specific process
ps -s <pid>
```

### Understanding Process States

Also notice in the `-ef` output that some process command names are shown in **brackets**.
That indicates processes that are currently swapped out from physical memory into **virtual memory** on the hard drive.  
Processes that are swapped into virtual memory are called **sleeping**. Often the Linux kernel
places a process into sleep mode while the process is **waiting for an event**.  
When the event triggers, the kernel sends the process a signal. If the process is in **interruptible sleep mode (S)**, it will receive the signal immediately and wake up. If the process is in
**uninterruptible sleep mode (D)**, it only wakes up based on an external event, such as hardware
becoming available. It will save any other signals sent while it was sleeping and act on them
once it wakes up.
If a process has ended but its parent process hasn’t acknowledged the termination signal
because it’s sleeping, the process is considered a **zombie (Z)**. It’s stuck in a limbo state between
running and terminating until the parent process acknowledges the termination signal.


**PROCESS STATE CODES:**  
Here are the different values that the s, stat and state output specifiers (header "STAT" or "S") will
       display to describe the state of a process:

               D    uninterruptible sleep (usually IO)
               R    running or runnable (on run queue)
               S    interruptible sleep (waiting for an event to complete)
               T    stopped by job control signal
               t    stopped by debugger during the tracing
               W    paging (not valid since the 2.6.xx kernel)
               X    dead (should never be seen)
               Z    defunct ("zombie") process, terminated but not reaped by its parent

For BSD formats and when the stat keyword is used, additional characters may be displayed:

               <    high-priority (not nice to other users)
               N    low-priority (nice to other users)
               L    has pages locked into memory (for real-time and custom IO)
               s    is a session leader
               l    is multi-threaded (using CLONE_THREAD, like NPTL pthreads do)
               +    is in the foreground process group

### Headers

- UID: The user responsible for running the process
- PID: The process ID of the process
- PPID: The process ID of the parent process (if the process was started by another process)
- C: The processor utilization over the lifetime of the process
- STIME: The system time when the process was started
- TTY: The terminal device from which the process was started
- TIME: The cumulative CPU time required to run the process
- CMD: The name of the program that was started in the process


| %CPU           | How much of the CPU the process is using               |
| :------------- | :----------------------------------------------------- |
| %MEM           | How much memory the process is using                   |
| ADDR           | Memory address of the process                          |
| C or CP        | CPU usage and scheduling information                   |
| COMMAND*       | Name of the process, including arguments, if any       |
| NI             | nice value                                             |
| F              | Flags                                                  |
| PID            | Process ID number                                      |
| PPID           | ID number of the process's parent process              |
| PRI            | Priority of the process                                |
| RSS            | Real memory usage                                      |
| S or STAT      | Process status code                                    |
| START or STIME | Time when the process started                          |
| SZ             | Virtual memory usage                                   |
| TIME           | Total CPU usage                                        |
| TT or TTY      | Terminal associated with the process                   |
| UID or USER    | Username of the process's owner                        |
| WCHAN          | Memory address of the event the process is waiting for |

## `top`, `watch` `free` and `uptime`

`htop` and `glance` are 3rd party alternatives for top. **htop** is awsome (interactive)
- `free -h`: look at memory usage  ( same as what top would retrieve)
- `uptime`: system load (same as what top would retrieve)
- `watch uptime`: with watch utility you can run a command every two seconds to monitor ir process. You can monitor a directory’s changes in real time
and more.

```zsh
# To display the top 15 processes sorted by memory use in descending order (batch mode)
# -o : used to specify fields for sorting processes
# -b : batch mode
        top -b -o +%MEM | head -n 22
# To watch for mail, you might do
        watch -n 60 from
# To watch the contents of a directory change, you could use (-d highlight output changes)
        watch -d ls -l
# If you're only interested in files owned by user joe, you might use
        watch -d 'ls -l | fgrep joe'
# To see the effects of quoting, try these out
        watch echo $$
        watch echo '$$'
        watch echo "'"'$$'"'"
# To see the effect of precision time keeping, try adding -p to
        watch -n 10 sleep 1
# You can watch for your administrator to install the latest kernel with
        watch uname -r
```

| Command | Description                                                                                       |
| :------ | :------------------------------------------------------------------------------------------------ |
| 1       | Toggles the single CPU and Symmetric Multiprocessor (SMP) state                                   |
| b       | Toggles the bolding of important numbers in the tables                                            |
| I       | Toggles Irix/Solaris mode                                                                         |
| z       | Configures color for the table                                                                    |
| l       | Toggles display of the load average information line                                              |
| t       | Toggles display of the CPU information line                                                       |
| m       | Toggles display of the MEM and SWAP information lines                                             |
| f       | Adds or removes different information columns                                                     |
| o       | Changes the display order of information columns                                                  |
| F or O  | Selects a field on which to sort the processes (%CPU by default)                                  |
| < or >  | Moves the sort field one column left (<) or right (>)                                             |
| R       | Toggles normal or reverse sort order                                                              |
| h       | Toggles showing of threads                                                                        |
| c       | Toggles showing of the command name or the full command line (includ-ing parameters) of processes |
| i       | Toggles showing of idle processes                                                                 |
| S       | Toggles showing of the cumulative CPU time or relative CPU time                                   |
| x       | Toggles highlighting of the sort field                                                            |
| y       | Toggles highlighting of running tasks                                                             |
| z       | Toggles color and mono mode                                                                       |
| u       | Shows processes for a specific user                                                               |
| n or #  | Sets the number of processes to display                                                           |
| k       | Kills a specific process (only if process owner or if root user)                                  |
| r       | Changes the priority (renice) of a specific process (only if process owner or if root user)       |
| d or s  | Changes the update interval (default three seconds)                                               |
| W       | Writes current settings to a configuration file                                                   |
| q       | Exits the top command                                                                             |

## Sending Signals To Processes

To send a signal to a process, use the `kill`, `pkill` or `pgrep` commands. But programs can only respond to signals if they are programmed to recognize those signals. A root user can kill System-level-process and the process of any user.  
The generally accepted procedure is to first try the `TERM` signal. If the
process ignores that, try the `INT` or `HUP` signal. If the program recognizes
these signals, it will try to gracefully stop doing what it was doing before
shutting down. The most forceful signal is the `KILL` signal. When a process
receives this signal, it immediately stops running. Use this as a last resort,
as it can lead to corrupted files.

```zsh
# view a list of all signals
# use /bin/kill to prevent using shells kill (zsh)
/bin/kill -L
# convert number to name abd vice versa
kill -l 11
```
And most signals are for internal use by the system, or for programmers when they write code. The following are signals which are useful to a system user:

- **SIGHUP 1** – sent to a process when its controlling terminal is closed.
- **SIGINT 2** – sent to a process by its controlling terminal when a user interrupts the process by pressing [Ctrl+C].
- **SIGQUIT 3** – sent to a process if the user sends a quit signal [Ctrl+D].
- **SIGKILL 9** – this signal immediately terminates (kills) a process and the process **will not perform any clean-up operations**.
- **TERM (SIGTERM) 15** – Terminates if possible. this is a program termination signal (kill, pkill and killall will send this by **default**).
- **SIGTSTP 20** – sent to a process by its controlling terminal to request it to stop (terminal stop); initiated by the user pressing [Ctrl+Z].

```zsh
pidof firefox
kill 9 2687
kill -KILL 2687
kill -SIGKILL 2687

# To kill an application using its name, use pkill or killall
pkill firefox
killall firefox # kill all instances & childs
```

The `pkill` and `pgrep` commands have a variety of searches they can perform to locate the appropriate processes. Thy have similar syntx. The `pkill` command is a powerful way to send processes’ signals using selection criteria
other than their PID numbers or commands they are running. You can choose by **user-name**, **user ID (UID)**, **terminal** with which the process is associated, and so on. In addition,
the pkill command allows you to use **wildcard** characters `pgrep` command’s syntax.

```zsh
#Example 1: Find the process ID of the named daemon:

        pgrep -u root named

#Example 2: Make syslog reread its configuration file:

        pkill -HUP syslogd

#Example 3: Give detailed information on all xterm processes:
# -f: match pattern in full comand line not only process name.
        ps -fp $(pgrep -d, -x xterm)

#Example 4: Make all netscape processes run nicer:

        renice +4 $(pgrep netscape)

```

## Changing Linux Process Priority

On the Linux system, all active processes have a `priority` and certain `nice` value. Processes with higher priority will normally get more CPU time than lower priority processes.  
However, a system user with root privileges can influence this with the `nice` (NI field from top) and `renice` commands.  
Use the nice command to set a nice value for a process. Keep in mind that **normal users** can attribute a nice value from **zero to 20** to processes they own.
Only the **root** user can use **negative nice values**.  
Nice value — Nice values are user-space values that we can use to control the priority of a process. The nice value range is -20 to +19 where **-20 is highest**, 0 default and **+19 is lowest**.  
To renice the priority of a process, use the renice command as follows  

```zsh
#  start an application with a nondefault niceness level setting
nice -n VALUE COMMAND
renice PRIORITY [-p PIDS] [-u USERS] [-g GROUPS]
renice +8  2687
renice +8  2103
```

## Background & foreground jobs

The `job` utility allows you
to see any processes that belong to you that are running in background mode. However,
it displays only the job number. If you need the job’s PID, you have to issue the `jobs -l` command.

```zsh
sleep 3000 &
jobs -l
# [1]  + 10274 running    sleep 3000
# job ID : 1 and PID: 10274

# the plus sign ( +) next to the new background job’s number. It
# denotes the last job added to the background job stack. The minus sign ( -) 
# indicates that this particular job is the second-to-last process, which was added to the job stack.

#[1]    10274 running    sleep 3000
#[2]  - 11318 running    sleep 3200
#[3]  + 11324 running    sleep 2000
fg %2
kill %2
# by default send stdout and stderr to $HOME/nohup.out
nohup bash CriticalBackups.sh &
```

## Check for openfiles: `lsof`

**Be careful of stopping processes that may have open files**. Files can be
damaged and unrepairable if the process is abruptly stopped. It’s usually a
good idea to run the lsof command first to see a list of the open files and
their processes.  

`lsof` is a command meaning "list open files", which is used in many Unix-like systems to report a list of all open files and the processes that opened them. Open files in the system include disk files, named pipes, network sockets and devices opened by all processes. One use for this command is when a disk cannot be unmounted because (unspecified) files are in use. The listing of open files can be consulted (suitably filtered if necessary) to identify the process that is using the files.

```zsh
# lsof /var
COMMAND     PID     USER   FD   TYPE DEVICE SIZE/OFF   NODE NAME
syslogd     350     root    5w  VREG  222,5        0 440818 /var/adm/messages
syslogd     350     root    6w  VREG  222,5   339098   6248 /var/log/syslog
cron        353     root  cwd   VDIR  222,5      512 254550 /var -- atjobs

# To view the port associated with a daemon:
# lsof -i -n -P | grep sendmail
sendmail  31649    root    4u  IPv4 521738       TCP *:25 (LISTEN)
# unix sockets
lsof -U
# files pend by a cmmand or multiple commands
lsof -c chrome
# exclude chrome
lsof -c "^chrome"
# using  regex for searching command name (ignore case)
lsof -c /regex_string/i
# MULTIPLE COMMAND
lsof -c ssh -c init
# To find Processes running on Specific Port:
lsof -i TCP:22
# user specific opened filess
lsof -u milad
# exclude root user
lsof -i -u^root
# To list opened files under a directory:
lsof +D /var/log/
# To list processes which opened a specific file:
lsof /var/log/syslog
```

**OUTPUT DESCRIPTION:**
TYPE – of files and it’s identification.

- DIR – Directory
- REG – Regular file
- CHR – Character special file.
- FIFO – First In First Out

**FD** – stands for File descriptor and may seen some of the values as:

- cwd current working directory
- rtd root directory
- txt program text (code and data)
- mem memory-mapped file

Also in FD column numbers like `1u` is actual file descriptor and followed by u,r,w of it’s mode as:

- r for read access.
- w for write access.
- u for read and write access.