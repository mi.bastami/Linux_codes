# Accessing the Genome Browser Programmatically

## Getting Sequences

### downloading UCSC commandline tools

```zsh
mkdir utilities && cd utilities
rsync -aP rsync://hgdownload.soe.ucsc.edu/genome/admin/exe/linux.x86_64/ ./
chmod +x ./*
```

### extract a sequence from UCSC server

The best choice is to use the **twoBitToFa** command.

```zsh
twoBitToFa http://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/hg38.2bit:chr1:100100-100200 stdout
 # >chr1:100100-100200
 # gcctagtacagactctccctgcagatgaaattatatgggatgctaaatta
 # taatgagaacaatgtttggtgagccaaaactacaacaagggaagctaatt
```

### extract sequence for a list of coordinates

```zsh
cat input.bed
 # chr1 4150100 4150200 seq1
 # chr1 4150300 4150400 seq2
twoBitToFa http://hgdownload.soe.ucsc.edu/goldenPath/mm10/bigZips/mm10.2bit -bed=input.bed stdout
 # >seq1
 # gcatcccagtcctgatactggaaaattcatttagtgacaagcgagggcca
 # cttgggattctctcacccccatatttaggagaccttattagggtcacctt
 # >seq2
 # tatccccttccctccccaccagatactacaattcacatcatactctgtcc
 # cccagtctacccataaaatctattctatttacctctccaaacgaagatct
```

### The most efficient way to get sequence from UCSC

In browser, first you must make a custom track of the region(s) you would like sequence for, and then use the “output format: sequence” option with your custom track selected as the primary track.

In command line, downloading the **2bit** file for your organism of interest and then using the twoBitToFa command on it like so:

```zsh
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/hg38.2bit
twoBitToFa hg38.2bit:chr1:100100-100200 stdout
 # >chr1:100100-100200
 # gcctagtacagactctccctgcagatgaaattatatgggatgctaaatta
 # taatgagaacaatgtttggtgagccaaaactacaacaagggaagctaatt
```

### get the sequence & count A, C, G, T: `twoBitToFa` and `faCount`

```zsh
twoBitToFa http://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/hg38.2bit:chr1:100100-100200 stdout | faCount stdin
 # #seq    len     A       C       G       T       N       cpg
 # chr1:100100-100200      100     37      17      21      25      0       0
 # total   100     37      17      21      25      0       0
```

## Using the Public MySQL Server and gbdb System (non-sequence data)

The easiest way to get started is via the Table Browser. Choose the assembly and track of interest and click the “describe table schema” button, which will show the MySQL database name, the primary table name, the fields of the table and their descriptions. If the track is stored not in MySQL but as a binary file (like bigBed or bigWig) in /gbdb, it will show a file name, e.g. "Big Bed File: /gbdb/dm6/ncbiRefSeq/ncbiRefSeqOther.bb"

### Accessing the public MySQL server


#### download some transcripts from the new NCBI RefSeq Genes track

```zsh
mysql -h genome-mysql.soe.ucsc.edu -ugenome -A -e "select * from ncbiRefSeq limit 2" hg38
```

If you are interested in a particular enhancer region, for instance “chr1:166,167,154-166,167,602”, and want to find the nearest genes within a 10kb range

```zsh
chrom="chr1"
chromStart="166167154"
chromEnd="166167602"
mysql -h genome-mysql.soe.ucsc.edu -ugenome -A -e "select \
   e.chrom, e.txStart, e.txEnd, e.strand, e.name, j.name as geneSymbol from ncbiRefSeqCurated e,\
   ncbiRefSeqLink j where e.name = j.id AND e.chrom='${chrom}' AND \
      ((e.txStart >= ${chromStart} - 10000 AND e.txStart <= ${chromEnd} + 10000) OR \ (e.txEnd >= ${chromStart} - 10000 AND e.txEnd <= ${chromEnd} + 10000)) \
order by e.txEnd desc " hg38
```