<!-- TOC -->

- [Chapter 2: setting up and managing a bioinformatics project](#chapter-2-setting-up-and-managing-a-bioinformatics-project)
  - [shell wild cards and expansion](#shell-wild-cards-and-expansion)
  - [brace expansion `text-{1,2,3}`](#brace-expansion-text-123)
  - [Pandoc: convert between formats: *markdown to HTML*](#pandoc-convert-between-formats-markdown-to-html)
- [Chapter 3: Remedial Unix shell](#chapter-3-remedial-unix-shell)
  - [`csvtk`: toolkit for csv file manipulation](#csvtk-toolkit-for-csv-file-manipulation)
  - [Streams and Redirection](#streams-and-redirection)
    - [concatanating multiple files](#concatanating-multiple-files)
    - [redirection](#redirection)
    - [a tip regarding `ls -lrt`*](#a-tip-regarding-ls--lrt)
    - [redirectong both std out & std err: logfiles](#redirectong-both-std-out--std-err-logfiles)
    - [redirecting to pseudodevice `/dev/null/`](#redirecting-to-pseudodevice-devnull)
    - [using tail -f to monitor redirected std err](#using-tail--f-to-monitor-redirected-std-err)
  - [using pipes](#using-pipes)
    - [Combining pipes and redirection](#combining-pipes-and-redirection)
    - [Redirecting stderr to stdout `&>`, `&>>`,`2>&1`](#redirecting-stderr-to-stdout--21)
    - [a tee joint in pipe](#a-tee-joint-in-pipe)
  - [Managing and interacting with processes](#managing-and-interacting-with-processes)
    - [Background processes](#background-processes)
    - [background processes and **hangeup**](#background-processes-and-hangeup)
    - [puting a foreground command to Background](#puting-a-foreground-command-to-background)
    - [kill a foregrounded process using `CTRL+C`](#kill-a-foregrounded-process-using-ctrlc)
  - [Exit status](#exit-status)
  - [Command substitution `$(CMD)`](#command-substitution-cmd)
- [Chapter 4: Working with remote machines (`SSH`)](#chapter-4-working-with-remote-machines-ssh)
  - [SSH config file: store your frequent hosts](#ssh-config-file-store-your-frequent-hosts)
  - [SSH keys](#ssh-keys)
  - [Maintaining long-running jobs with `nohup` and `tmux`](#maintaining-long-running-jobs-with-nohup-and-tmux)
    - [Detaching and Attaching Sessions](#detaching-and-attaching-sessions)
    - [Working with Window Panes](#working-with-window-panes)
    - [CTRL-B cheat sheet](#ctrl-b-cheat-sheet)
- [Chapter-6: Bioinformatics data](#chapter-6-bioinformatics-data)
  - [Downloading Data](#downloading-data)
    - [`wget`: downloading Data](#wget-downloading-data)
    - [`curl`](#curl)
    - [`Rsync` and `Secure Copy` (SCP)](#rsync-and-secure-copy-scp)
  - [Data integrity](#data-integrity)
  - [Looking at differences between data: `diff`](#looking-at-differences-between-data-diff)
  - [Compressing Data](#compressing-data)
- [Chapter 7: Unix Data Tools](#chapter-7-unix-data-tools)
  - [line seperator characters](#line-seperator-characters)
  - [`head` and `tail`](#head-and-tail)
  - [`less`](#less)
  - [Plain text summary information with `wc`, `ls`, `awk`](#plain-text-summary-information-with-wc-ls-awk)
  - [`cut` working with columns](#cut-working-with-columns)
  - [`column`](#column)
  - [`GNU` or `BSD` flavors](#gnu-or-bsd-flavors)
  - [`grep`](#grep)
  - [decoding plain text data with `hexdump`](#decoding-plain-text-data-with-hexdump)
  - [Sorting text files with `sort`](#sorting-text-files-with-sort)
  - [finding unique values with `uniq`](#finding-unique-values-with-uniq)
  - [`join`](#join)
  - [Union, Intersection and Difference of files: 'sort', 'uniq' and 'join'](#union-intersection-and-difference-of-files-sort-uniq-and-join)
    - [For sorted files](#for-sorted-files)
    - [For unsorted Files](#for-unsorted-files)
  - [Subshells](#subshells)
  - [Named Pipes and process substitution](#named-pipes-and-process-substitution)
- [Chapter 8: R](#chapter-8-r)
  - [split-apply-combine](#split-apply-combine)
    - [split-apply-combine pattern: ```split() lapply() do.call(rbind, list)```](#split-apply-combine-pattern-split-lapply-docallrbind-list)
    - [split-apply-combine pattern: ```aggregate() tapply() by()```](#split-apply-combine-pattern-aggregate-tapply-by)
    - [split-apply-combine: ``dplyr``](#split-apply-combine-dplyr)
  - [string prrocessing](#string-prrocessing)
  - [Runinig R scripts from command line: `Rscript R r`](#runinig-r-scripts-from-command-line-rscript-r-r)
  - [Dependency management: `packrat`](#dependency-management-packrat)
  - [Workflows for Loading and Combining Multiple Files](#workflows-for-loading-and-combining-multiple-files)
  - [exporting dataframe to a compressed file](#exporting-dataframe-to-a-compressed-file)
- [Chapter 9: Genomic Ranges](#chapter-9-genomic-ranges)
  - [Chagning coordinate systems](#chagning-coordinate-systems)
  - [BEDTOOLS](#bedtools)
    - [Sorting bed files](#sorting-bed-files)
  - [intersecting files](#intersecting-files)
  - [resizing intervals: slop](#resizing-intervals-slop)
  - [getting flanks (promoters)](#getting-flanks-promoters)
  - [getting sequence for intervals](#getting-sequence-for-intervals)
  - [Coverage using BEDTOOLS](#coverage-using-bedtools)
  - [bedtools complement](#bedtools-complement)
  - [bedtools closest](#bedtools-closest)
  - [bedtools merge](#bedtools-merge)
    - [Count the number of overlapping intervals](#count-the-number-of-overlapping-intervals)
- [Chapter 11: alignments](#chapter-11-alignments)
  - [coordinate system of various formats](#coordinate-system-of-various-formats)
  - [Termonology (SAM 1.6)](#termonology-sam-16)
    - [Linear alignment](#linear-alignment)
    - [Chimeric alignment](#chimeric-alignment)
    - [Multiple mapping](#multiple-mapping)
    - [Alternate locus](#alternate-locus)
    - [Genome patch](#genome-patch)
    - [Assembly (<https://www.ncbi.nlm.nih.gov/grc/help/definitions/>)](#assembly-httpswwwncbinlmnihgovgrchelpdefinitions)
  - [Assembly units](#assembly-units)
    - [Haploid Assembly](#haploid-assembly)
    - [primary assembly](#primary-assembly)
    - [Unlocalized Sequence](#unlocalized-sequence)
    - [Unplaced Sequence](#unplaced-sequence)
    - [Contig](#contig)
    - [Scaffold](#scaffold)
  - [tips regarding SAM file](#tips-regarding-sam-file)
    - [bitwise FLAGS](#bitwise-flags)
    - [alignment fields](#alignment-fields)
  - [Converting SAM-BAM](#converting-sam-bam)
  - [Samtools Sort and Index](#samtools-sort-and-index)
  - [Extracting and Filtering Alignments with samtools view](#extracting-and-filtering-alignments-with-samtools-view)
    - [Extracting alignments from a region with samtools view](#extracting-alignments-from-a-region-with-samtools-view)
    - [Filtering alignments with samtools view](#filtering-alignments-with-samtools-view)
  - [Visualizing Alignments](#visualizing-alignments)
    - [Visualizing Alignments with `samtools tview`](#visualizing-alignments-with-samtools-tview)
    - [visualyzing alignments using `IGV`](#visualyzing-alignments-using-igv)
  - [Pileups with samtools pileup, Variant Calling, and Base Alignment Quality](#pileups-with-samtools-pileup-variant-calling-and-base-alignment-quality)
- [Chapter-13: Out of memory approches](#chapter-13-out-of-memory-approches)
  - [Tabix](#tabix)
  - [SQLite](#sqlite)
  - [RSQLite](#rsqlite)
    - [sqliteCopyDatabase: Copy a SQLite database](#sqlitecopydatabase-copy-a-sqlite-database)
    - [Regular expression `initRegExp`](#regular-expression-initregexp)
    - [Begin/commit/rollback SQL transactions](#begincommitrollback-sql-transactions)
    - [Self-contained SQL transactions `dbWithTransaction`](#self-contained-sql-transactions-dbwithtransaction)
    - [Statements](#statements)
    - [Batched queries](#batched-queries)
  - [RSQLite & bioconductor](#rsqlite--bioconductor)
  - [RSQLite & dplyr](#rsqlite--dplyr)
    - [more about SQLite indices](#more-about-sqlite-indices)
      - [ANALYZE](#analyze)
      - [Multi column indices  `AND`](#multi-column-indices-and)
      - [Covering Indices](#covering-indices)
      - [OR-by-UNION technique](#or-by-union-technique)
      - [sorting by index or covering index](#sorting-by-index-or-covering-index)
      - [Searching And Sorting](#searching-and-sorting)
      - [rowid vs INTEGER PRIMARY KEY](#rowid-vs-integer-primary-key)
    - [Foreign Key Constraints](#foreign-key-constraints)

<!-- /TOC -->

# Chapter 2: setting up and managing a bioinformatics project

## shell wild cards and expansion

    - asterisk  matches zero or more characters (ignores hidden files)
    - ? matches one characters (ignore hidden files)
    - [A-Z]  alphanumeric range
    - [0-9]  alphanumeric range

**examples:**

    - ls zmays[AB]_R1.fastq
    - ls zmays*
    - ls zmays?_R1.fastq

## brace expansion `text-{1,2,3}`

expand the comma separated values in curle braces. There should be no space between values. The -p argument let mkdir to create any subdirecory (here data) if needed. brace expansion will expand irrespective to whether file or directory exist. but wildcards expand to existing files.

       mkdir -p zmays/{data/seqs,analysis/scripts}

## Pandoc: convert between formats: *markdown to HTML*

    sudo apt-get install pandoc
    pandoc --from markdown --to html README.md > README.html

# Chapter 3: Remedial Unix shell

You may try to use **zsh** which more advance and contain atucomplete that comes handy in bioinformatics.

## `csvtk`: toolkit for csv file manipulation

## Streams and Redirection

Data streaming or text streaming: is a phylosophy in unix and allows us to handle large files without puting them all into memory. Data streaming in Unix contains:

- std in (file descriptor integer: 0)
- std out (file descriptor integer: 1)
- std err (file descriptor integer: 2)

### concatanating multiple files

    cat file1.fasta file 2.fasta > out.fasta

### redirection

      > std out. overrite existing content of a file
      >> std out. append to the end of a file
      2> std err. overrites
      2>> std err. append
      < std in
    std in: somethimes we use <. somethime we use pipe (|). some programs need a dash for indicating using std input and others takes an argument for this.

### a tip regarding `ls -lrt`*

- l: listing
- r reverse order
- t: order by time
- a: list all (including hidden ones)
- h: human readable sizes (1k, 1M, ...)
- `ls -ltrha` is commonly used

### redirectong both std out & std err: logfiles

file1 exist and file2 doesnot exists (producing error)
    ls -l file1 file2 >listing 2>listing.err

### redirecting to pseudodevice `/dev/null/`

Unwanted output can be redirected to `/dev/null`. Outputs redirected here will be disappered.

### using tail -f to monitor redirected std err

When redirecting both std out and std err, a long time running command may be finished with no message. To keep yourself updated with what is going on use either:
    `tail stderr.txt`: print the last 10 lines of the stderr file.
or use `tail -f` which follows the file being updated. lines added to the file will be printed. This can be canceled using `Control+c`, but the file continue being updated.
    `tail -f stderrr.txt`

## using pipes

There is an example for using pipe: a linux one-liner to take a fasta file and looks for non-nucleotide characters.  

    grep -v "^>" file .fasta | grep --colour -i "[^ATCG]"

- first remove header line
- then print lines that doesnot match A, T, C or G
- colour: print the matching in colour
- i: ignore case (T is the same as t)
- ^ inside [] means everything that is not one of the carachters in []

### Combining pipes and redirection

handling stderr and stdout of two processes

    program1 input.txt 2> program1.stderr | program2 2> program2.stderr

### Redirecting stderr to stdout `&>`, `&>>`,`2>&1`

A use case of this approach is when you want to look for "error" both in std out and std out. pipes only works on std out not std err. First merge stderr and std output

    program1 2>&1 | grep "error"

`&>` is equivalent to `2>&1`; `>&` is also used, but the preferred method is `&>`, because `&>>` means appending but the other forms doesnot have appending.

### a tee joint in pipe

We occasionly need to write intermediate files to disk for debuging purposes or to have the output of long term running programs and also use pipe benefits. The tee program works like a plumber tee joint: the output of program1 both wrritten to intermediate.file and redirected as input to program2.

    program1 input.txt | tee intermediate.file | program2 >results.txt

## Managing and interacting with processes

Basics of manipulatidng processes.

### Background processes

By default shell runs commands in **foreground**. we can run a command in background and making shell available for running other commands by appending `&` to the end of the command. This with return a **process ID** or **PID**.

    program1 input > output.txt &

  **to see all background jobs:**

- `jobs`  without args the status of **all active jobs** are diplayed and gives a **job ID** which is different from PID.
- `jobs -l` adds PID to other displated info.

**to bring a job to foreground `fg`**

- `fg` bring the most recent process to foreground
- `fg %<num>` replace <num> with job ID from jobs output (e.g. `fg %1`)

### background processes and **hangeup**

when proccesses from a terminal are send to background and we close the terminal, it sends a `hangeup` signal or `SIGHUP` to all processes started from that terminal and kill the process. To prevent this we should use the tool `nohup` or run the command from within `Tmux`. Chaapter 4 for details.

### puting a foreground command to Background

we need to **suspend** or pause the proccess using `CTRL + z` and then put it to background using `bg` which is the same as `fg`.

    bg %<num>

### kill a foregrounded process using `CTRL+C`

The process is then non-recoverable. Other usefull process management commands `top; ps, kill`

## Exit status

**exit status 0**: command was successfully ran. **nonzero exit status:** command failed. Not all programs handle errors correctly. you should never trust your tools! It is possible that a command return an error and also a zero exit status. To check the exit status of the last command use shell variable `$?`

    program1 input.txt > output.text
    echo $?
    0
**operators that implement exit status:**

- `&&` will run the next command only if the previous command return a **zero exit** status (secessful)
- `||` will run the next command onlt if the previous command return a **non-zero exit** status (failure)
- `true` would return exit success
- `false` would return exit fail

```Shell
program1 input > intermediate.txt && program2 intermediate.txt > results.txt
program1 input > intermediate.txt || echo "warninng: an error occured"
true; echo $?  # return 0
false; echo $? # return 1
true && echo "first cmd was suceess"  # return the message
true || "first command was suceess"   # return nothing
false && echo "first cmd was ok"      # return nothing
false || echo "first cmd was success" # return the message
```

**to run command sequentially use`;`**
This is irrespective of the exit status.

    false; true; false; echo "none of previous mattered"

## Command substitution `$(CMD)`

Allows to run a command inline and return the output as string and use this string in another command.

    echo "There are $(grep -c "^>" input.fasta) in my FASTA file"

This will make directories with current date. +%F is a standard and usefull format (2015-04-13). When directories are sorted by name using `ls -l`, directories are also sorted chronologically.

    mkdir results-$(date +%F)

# Chapter 4: Working with remote machines (`SSH`)

SSH: secure shell. You can connect to a host by `ssh hostname` or `ssh hostIP` address. If your local username differs with the remote username you should specify username in the `user@domain` format. The default port is 22. The `-p` flag defines the port number. The `-v` or `-vv` or `-vvv` flags define the verbose and it level.

    ssh biocluster.myuniversity.edu
    ssh -p 50432 cdarwin@biocluster.myuniversity.edu
    ssh 192.169.273

## SSH config file: store your frequent hosts

An SSH config file may be created to store frequently connected hosts. Hosts in this file can be used with SSH and two usefull programs: `rsync` and `scp` (Chapter 6).
You can easily creat the file at `~/.ssh/config`. Each entry takes the following form:

```
host bio-serv
     HostName 192.168.237.42
     User cdarwin
     Port 50432
```

Then you can connect to host using `ssh bio-serv`. In terminal you can ask for the host name by `hostname` commaand. If you have multiple account at server check the current account by `whoami`.

## SSH keys

An alternative to entering passwd in each session in to use a public/private key pair (just like the one we used with git that was created with gpg program). Generate a ssh key using `ssh-keygen`:

    ssh-keygen -b 2048

  This creates a private akey at `~/.ssh/id-rsa` and public key at `~/.ssh/id-rsa.pub`. To use passwd less authentication:

  1. first login to host using passwd.
  2. change to ~/.ssh
  3. append the public key to remote `~./ssh/autherized_keys` by copying and pasting or using `ssh-copy-id
  4. login again. if will ask for key's passwd.
  5. `ssh-agent` which can be started with `eval ssh-agent` allows to use ssh keys without enteing it's password each time.
  6. `ssh-add` will add the keys to ssh-agent
  7. for the next login no passwd is reguired.

## Maintaining long-running jobs with `nohup` and `tmux`

In local machine, by closing terminal the proccess will be terminated by SIGHUP or hangup signal. The same is ture abot remote connections. When you get disconnected or network temporarily lost, the process will be lost.
**Using `nohup` local terminal:**
nohup will return the PID which is the only way to terminate the process in this case.

```Shell
nohup program1 input >output &
```

- **Using `Tmux` or GNU `Screen` for remote machines**
*Terminal multiplexers* like Tmux (well developed) or Screen (popular alternative), create a session with multiple windows. Sessions are stable to hangeups and connection loses. You need to just relogin to host (ssh) and reattach the session to the current terminal. `~/.tmux.conf` is the configuration file of Tmux. Download a copy from book's github and paste it in the relevant directory it adds nice features to Tmux.

- **working with `Tmux`:**
With Tmux, you can create multiple Sessions (one for each project), each session can have multiple windows open (e.g. one for shell, one for text documentation). All these windows are within Tmux. It is not the same as tabs and multiple windows in regular terminals. You can start Tmux by creating nex sessions either in local machine or in remote host (after ssh to the remote).

  tmux nex-session -s <SessionName>

- **Interacting with Tmux:** though shortcuts, by default (Control+B) + (release Control B) + press another key. We changes the Ctrl+b to CTRL+a in configuration file.
- **attaching detaching sessions:**
    detach: (Control+a) + d # detach session
    tmux list-sessions      # see the list of running  Sessions
    tmux attach or tmux attach-session
    tmux attach -t <SessionName>
- **create a window in current session:** (CTRL+b) + c
- (ctrl-b)+N: next window; +P: previous window; +0-9: specific window;
- (CTRL-b)+?: list of shortcuts
- (ctrl-b)+w: choose window from a list.

### Detaching and Attaching Sessions

If you press **Ctrl+B, and then D**, you will detach the **session**. It will continue to run in the background, but you won’t be able to see or interact with it. We use the session name to attach to a background session, and then restore it to an interactive one.  
  
To attach a detached session, we’ll use the self-explanatory `attach-session` command with the -t (target session) option. Any long-running or continual processes you launched before detaching the session will still be running in the background (unless they’ve finished) when you attach the session. Use **ctrl-b-s** to see a **list of sessions**, then **Right Arrow**, the windows for the highlighted session are displayed. If you select a new session, your current one detaches, and the one you selected is attached.

```zsh
# start tmux
tmux

# start a named session in tmux (session name: geek-1)
tmux new -s geek-1

# rename an existing session
tmux rename-session -t 0 database

# ctrl-b-d to dettach the session
# to attach session (-t target session)
tmux attach-session -t geek-1

# detach it again to create a new session (ctrl-b-d)
# create a new session
tmux new -s geek-2

# ctrl-b-s to see a list of sessions
```

### Working with Window Panes

- If you press **Ctrl+B, and then double quotation marks (“”)**, you split the window **horizontally** into two panes. This only affects the current window.

- These are two independent command lines, not two views in one window; they are distinct and **separate shells**.

- ctrl-b and then arrows to hop in another pane.

- ctrl-b-x to kill a pane.  
  
If you press **Ctrl+B, and then the percentage sign (%)** it splits the current pane **vertically**.

- Press Ctrl+B, and then Q to make tmux briefly flash the number of each pane

### CTRL-B cheat sheet

Session Commands

- S: List sessions.
- $: Rename current session.
- D: Detach current session.
- Ctrl+B, and then ?: Display Help page in tmux.
  
Window Commands

- C: Create a new window.
- ,: Rename the current window.
- W: List the windows.
- N: Move to the next window.
- P: Move to the previous window.
- 0 to 9: Move to the window number specified.

Pane Commands

- %: Create a horizontal split.
- “: Create a vertical split.
- H or Left Arrow: Move to the pane on the left.
- I or Right Arrow: Move to the pane on the right.
- J or Down Arrow: Move to the pane below.
- K or Up Arrow: Move to the pane above.
- Q: Briefly show pane numbers.
- O: Move through panes in order. Each press takes you to the next, until you loop through all of them.
- }: Swap the position of the current pane with the next.
- {: Swap the position of the current pane with the previous.
- X: Close the current pane.


# Chapter-6: Bioinformatics data

## Downloading Data

### `wget`: downloading Data

- For quickly downloading files.
- works for both `ftp` and `http`.
- set username with `--user=` and passwd with `--ask-password`
- `-r` or `--recruisive`: downloading `recruisively`
  - `-l` or `--level`: default:5.
  - we should set ome limits.
  -`wget --accept "*.gtf --no-directories --recruisive --no-parent <url>"`
  `--no-parent`: prevent downloadinghigher directories
  `--accept` or `-A`: download specific filenames.

### `curl`

- For quickly downloading files.
- Capable of using more protocols: `sftp; SCP`.
- by default, prints to std out. Either redirect the output or use `-o <filename>`.
- If `-o` is specified without filename argument, the remote filename will be used.
- `-L` or `--location`: **Can follow page redirects**.
- is implemented in `RCurl` and `pycurl`

### `Rsync` and `Secure Copy` (SCP)

- `Rsynch` is appropriate for hevy-duty tasks. e.g. **synchronizing** entire large directory.
- When a copy in already exist or partially exists, Rsynch will download only **differences between file version** in a compressed form: its **faster**.
- Rsynch has an **Archive option** which preserves file attributes (owner, modifications, timestamps, permissions)), making it deall for **network backup**.
- `rsynch source destination`. Either dource or destination can be a remote host in a format `user@host:directory/to/files`
- `rsynch -avz -e ssh zea_mays/data/ 192.168.237.42:/home/data`
  - `-a`: archiving; `-v`: verbose; `-z`: compression;
  - `-e ssh`: we rae going to connect through ssh. if ssh host aliases were used, this could be replaced by host's aliase.
  - `data/` in source means copy contents, but `data` means copy entire directory (directory itself & its contents)
- rsynch transfers files if they **do not exist or changed**. Use `rsynch` again to check if worked fine.
- Use **exit status** in scripts to check for problems in transferring files.

**Secure Copy (scp)**:
It is used in cases where we need to sust quickly copy a file over ssh. We can use scp using `scp source destination`. Destination is the same as what was used for rsynch.

## Data integrity

Checksum is a compresseed summary of the data and will change if even a bit of data changes. Using `checksums` for evaluating data integrity:

1. To verify file transfered correctly and is not changes or corrupted.
2. To keep track of data versions.
3. Reproducibility: associate a particular analysis to a specific data with checksums.
**SHA and MD5 Checksums**

- `SHA-1` is newer but `md5` is more common.
- `shasum`program or `sha1sum` for SHA-1. `md5sum` for MD5 (in some servers `chsum` or `sum` programs).
- `shasum *.fastq >chechsum.sha` create checksum file for all fastq file.
- `shasum -c checksum.sha` to validate checksums. Returns a nonzero exit status if any file fail.

## Looking at differences between data: `diff`

- `diff -u gene-1.bed gene-2.bed`
- the `-u` option enables unified format for output wich gives more context relative to diff's default output.
- Hunks (i..e changed chunks) are seperated with `@@ .. .. @@`
- changes (- or +) are relative to the current file (i.e. ---).
- The output of diff can be redirected to a file, **patch file**. Patch files are instructions on how to update plain text files and can be used with `patch` unix program to apply changes.
- diff can be computationally expensive on large datasets.

## Compressing Data

- `gzip` and `bzip2` are two most common compression program.
- `gzip` is faster, `bzip2` compress more.
- `gzip` for most bioinformatics jobs, `bzip2` for long-term storages.
**Some usefull applications are:**
- `trimmer in.fastq.gz | gzip > out.fastq`
- `gzip in.fastq`: compress file  in place (replacing the uncompressed file)
- `gunzip in.fastq.gz`: uncompress file  in place (replacing the compressed file)
- `gzip -c in.fastq > infastq.gz`: compress file to std out
- `gunzip -c in.fastq.gz >in.fastq`: uncompress file to std out
- `gzip -c in2.fastq >> in.fastq.gz`: compressing and appending to an existing gz file. Files are **Concatanated** and are no longer seperated.
- `cat in.fastq in2.fastq | gzip >in.fastq.gz`: compressing multiple file together. Files are **Concatanated** and are no longer seperated.
- use `tar` for compressing multiple files and keep them seperated.
- `tar -cvf in.fastq.tar in1.fastq in2.fastq`
**An important advantage of gzip a nd bzip2 is that unix and bioinfo tools can work directly with compressed files.**
- `zcat` instead of `cat`.
- `zdiff` instead of `diff`
- `zgrep` instead of `grep`
- `zless` instead of `less`
- If the program connot handle compressed files, use `zcat in.fastq.gz | program`.

# Chapter 7: Unix Data Tools

## line seperator characters

In windows, line seperator is usually `\r\n`, where `\r` is **cariage return**. Some files may also be seperated by a signgle carriage return. In inux and Mac, line seperator is `\n`: **linefeed character**. CSV format usually use `\r\n` as line seperator as suggested by **RFC-4180 guidelines**.

## `head` and `tail`

- `head -n 10`: show the first 10 lines.
- `tail -n 10`: show the last 10 lines.
- `tail -n +2`: truncate the first row. Show from row number 2 to the end.
-`(head -n 3; tail -n 3) < file.bed`: show both head and tail of a file.
- to write a function for the above command and put it into ~/.bashrc and source it:
  - `i(){(head -n 3; tail -n 3) < "$1" | column -t;}` put it into `~/.bashrc`
  - `source ~/.bashrc` to load the function.
  - `i` stands for inspection.
  - `column -t` will print a tabular output.
  - see `man column`.
- `pipeline | head -n 5`: use `head` to peek at data rsulting from a pipeline:
  - `grep 'gene_id "ENSG00000025907"' Mus-muscu;us.gtf | head -n 1`
  - usefull for checking if each step of pipeline is working correctly.
  - When printing the required lines, `head` exits, shell catches this signal i.e. `SIGPIPE` and stops the entire pipe.
  - for example in `grep 'string' huge_file.txt | program1 | program2 | head -n 5`, after head print out five lines and exits, the grep and other programs wont continue searching and running.
  - A program may explicitly, catches and ignore this signal (not required in bioinfo works).

## `less`

Is a terminal pager and more recent than `more`. run it using `less file.fastq`.
Usefull shortcuts:

- `h`: getting help;
- `space`: go down a page; `b`: go up a page.
- `j or k`: forward and backward a line.
- `q`: quite.
- `g`: first line; `G` last line.
- `/ <pattern>` search downward for strings.
- `? <pattern>` search upward for strings.
**Using `less` for debugging pipelines:**
Just pipe the output of the command you want to debug to `less` and delete everything after.
**Using `less` to iteratively construct pipelines:**
Pipelines should be contructed stepwise. If the full pipeline is `step 1 input.txt | step2 | step3 > output.txt`, we shlould first evaluate the output of each step and then add the next step. This can be done with `less`:

1. `step1 input.txt | less`  inspect the results
2. `step1 input.txt | step2 | less`
3. `step1 input.txt | step2 | step3 | less`
A good fiture of pipe is that when the output of a program is piped to `less`, pipes blockes the programm when less has a full scrin of data (i.e. when pipe if full). This enables us to quickly evaluate the pipeline prrocessing large data.

## Plain text summary information with `wc`, `ls`, `awk`

- `wc` by default gives number of words, lines, characters
- `wc file1 file2 file3` works with multiple files
- `wc -l` only number of lines
- lines may differ with rows: when there are some empty line, wc would count them as lines, but the actual number of rows are different.
- `awk -F "\t" '{print NF; exit}' file.bed` Get the number of columns based on first row.
- `grep -v "^#"  file.gtf | awk -F "\t" '{print NF; exit}'` Get the number of columns by first excluding comment lines.

## `cut` working with columns

- By defalut, the delimiter is tab `\t`.
- `cut -f1` cut and retain the first column
- `cut -f1-3` cut columns 1 to 3
- `cut -f1,4,5` cut columns 1, 4 and 5.
- cannot reorder columns with cut.
- `cut -d, -f2,3` specify the dilimiter a comma.

## `column`

- By defalut, the delimiter is tab `\t`
- `-s` specifies delimiter
- `column -s"," -t file.csv`
- `column -t` treat the data as table.

## `GNU` or `BSD` flavors

Some unix tools like `grep cut sort` has two flavores: **GNU or BSD**. BSD is used in OSX and freeBSD, GNU is used in linux. GNU-based flavore of tools are under developement, have better documentations and are more robust and powerfull. GNU tools can be instlled with:

```Shell
sudo apt install linuxbrew-wrapper
brew install coreutils
```

## `grep`

- `grep -w "bioinfo" file.txt`: by default, grep includes partial matches (i.e. bioinformatics, bioinfo, bioinformatician). `-w` restricts matches to words (surronded by spaces). Therefore, bioinformatics will not be in the output.
- `--color==auto`: color matches.
- providing context for matches: `-A` lines after each match; `B`: lines before; `-C`: after and before;
  - `grep -B1 "ATCGA" file.fastq`: include header (one line before) the matches.
- grep uses **BRE** (POSIX basic regular expression) and supports **ERE** (extended regular expression) with `-E` option.
- `egrep` in many systems refers to extended grep (i.e. grep with -E option)
- `grep -E "(olfr413|olfr411)"`: use pipe with `-E` to match any of the patterns.
- `grep -o "olfr.*"` extract only the matching part not the entire line.
- `grep -E -o 'gene_id "\w+"' file.gtf | cut -f2 -d" " | sed 's/"//g' | sort | uniq > mm_gene_id.txt`: capture alll unique gene names in the last fileld of a gtf file.
- `grep -c`: count the number of occurance
- `grep -m 3`: gives the First 3 matched lines
- `grep -m 3 -v`: gives the First 3 unmatched lines
- `grep -L`: only prints name of files that do not have any match. The reverse in `grep -l`

**Options:**

- Matcher Selection:
  - `-E`: ERE (extended);
  - `-G`: BRE (basic): default
  - `-P`: powerfull
  - `-F`: fixed string
- Matching Control:
  - `-i`: ignore case; `-v`: invert selection; `-w`; word.
  - `-x`: matching the whole line (^(pattern)$)
  - `-f filename`: giving a file with a pattern in each line.
- General Output Control:
  - `L` & `-l`
  - `-o`
  - `-c`
  - `--color`
  - `-m`
  - `-n`: also print line number

## decoding plain text data with `hexdump`

Two commonly used character encoding schemes: **ASCII** and **UTF8**. ASCII uses 7 bits to store 128 different values. Modern systems use 8 bits (one byte) to store ASCII characters. ASCII doesnot have special characters and therefore is usefull for bioinformatics analysis. Most data downloaded from famous bioinformatic databases are in ASCII format. **UTF8** is a superset of ASCII (i.e. ascii with specila charater support). Files generated by hand (copy  & paste) may have hiden special characters (i.e. being UTF-8), and therefore giving errors during analysis.

- `file`: this command gives the infered encoding of a file.
- `hexdump -c filename`: return the heximal value of each character. Usefull for insecting file for non-ascii characters.
- `alias nonascii="LC_CTYPE=C grep --color='auto' -n -P '[\80-\xFF]'"`. Add this alias to `~/.bashrc`. this will search for non-ascii characters and print the line with line number.

## Sorting text files with `sort`

- Sort uses a **merge sort** algorithm with sort intermediate sorted files writed on disk and allows us to sort files larger than our memory size.
- By default sort is **unstable** which means that when two lines are identical in terms of all specified sorting keys (-k), they are sorted by whole line (last-resort comparison) and therefore the order of such lines may be different from the original file. To disble this feature, use `-s` option to make sort stable.
- Default sorting is to sort alphanumerically. Sort doesnot sort by numbers in text (chr2, chr10, chr11, chr22). To make sort use a clever alphanumeric algorithm use `-V` for all columns or `k1,1V` for a specific column.
- The locale specified by the environment affects sort order.  Set **LC_ALL=C** to get the traditional sort order that uses native byte values.
-`sort file1 file2 >file12.txt`; Concatanate sorting. This will first concatanate all lines and then sort them as they were from a single file.
- **KEYDEF**  is F[.C][OPTS][,F[.C][OPTS]]: for start,end position;
  - F is field number; .C is character position in the field.
  - `-k2,`: default end position is end of file. column 2 to last column as sorting key.
  - `-k2.2,2.9` sorting key is from column2-character2 to column2-character9
  - OPTS is one or more single-let‐ter ordering options  [bdfgiMhnRrV],  which  override  global  ordering options  for  that key.
- `sort -kstart,end`: specifying start and end column of each sorting key. use `-kstart,start` to specify a specific column.
- `sort -k1,1V k2,2nr`: sort first column clever alphanumerically and then sort by second column reverse numerically.
- `sort -t","`: specift delimiter.
- `sort -n`: sort all columns numerically.
- `sort -k1,1 -k2,2n -c; echo $?`: To validate if a file is sorted by first and second column. Zero exit status mean file is sorted.
- `sort -k1,1 -k4,4n -S2G file.gtf`: `-S` option specifies the fixed-sized memmory buffer. 2G: 2gigabytes; 2M; 2K; `-S 50%` 50% of memory; A larger buffer means less intermediate files  wrriten to disk and more speed.
- `sort -k1,1 -k4,4n --parallel 4`: use 4 cores; use it for extremely large files otherwise may slow down the process.
-`sort -u` or `--unique`: without -c, output only the first of an equal run.

## finding unique values with `uniq`

- `uniq` **does not** return unique values. It only removes consecutive duplicate lines and keeping the first instance.
- To find unique values across all lines: `sort filename | uniq`
- `sort | uniq -i`: case insensitive
- `sort | uniq -c filename`:  give counts as well as unique values.
- Summarizing columns of categorical data:`grep -v '^#' file.gtf | cur -f3,7 | sort | uniq -c`: uniq also works with multiple columns. This gives how many features in column 3 are on each strand (column7)
- count number of duplicated lines: `sort test.bed | uniq -d | wc -l`. `-d` option of uniq return only duplicates.

## `join`

- Both files should be first sorted on the column we want to join by.
- `join -1 <file-1-field> -2 <file-2-field> file1 file2`
- `join -1 1 -2 1 file1.sorted.bed file2.sorted.bed`
- `join -j1 <(sort -k1b,1 file1.unsorted) <(sort -k1b,1 file2.unsorted)`: using **process substitution**: <(cmd)
- `-a FILENUM`: print unpairable lines coming from file FILENUM, where FILENUM is 1 or 2, corresponding to FILE1 or FILE2. `join -1 1 -2 2 -a 1 file.1 file.2`: only includes unpaired lines from file 1; Use `-a 1 -a 2`: to include unpaired lines from both file (will mix lines from both files!!).
- `join -j 1 file1  file2` is equivalent to `join -1 1 -2 1 file1 file2`
- `-v FILENUM`: like -a FILENUM, but suppress joined output lines.
- `join -j1 -v1 file1 file2`: return only unpaired lines of first file.
- `join -v1 -v2`: unpaired lines from both files
- dealing with unpaired lines:
- `join -a 1 -a 2 file1 file2` prints all lines (common and unpaired) from both files.  Without '-o auto' it is not easy to discern which fields originate from which file.
- `join -a 1 a 2 -o auto -e X file1 file2` is usefull in dealing with unpaired lines;
- `-i`: --ignore-case
- `-t CHAR`: use CHAR as input and output field separator
- Either FILE1 or FILE2 (but not both) can be '-'
- `-o FIELD-LIST`: construct each output line according to the format in FIELD-LIST.  Each element in FIELD-LIST is either the single character '0' or has the form M.N where the file number, M, is '1' or '2' and N is a positive field number. A field specification of '0' denotes the join field. The elements in FIELD-LIST are separated by commas or blanks. `join -o 1.2,2.2`
- recommended sorting option is `sort -k 1b,1`
- To avoid any locale-related issues, it is recommended to use the 'C'
locale for both commands:

      LC_ALL=C sort -k 1b,1 file1 > file1.sorted
      LC_ALL=C sort -k 1b,1 file2 > file2.sorted
      LC_ALL=C join file1.sorted file2.sorted > file3

- Both 'sort' and 'join' operate of whitespace-delimited fields.  To specify a different delimiter, use '-t' in _both_.
- To specify a tab (ASCII 0x09) character instead of whitespace, use `-t$'\t'` in both.
- `--header`: first line as header.
- To sort a file with a header line, use GNU 'sed -u'. The following example sort the files but keeps the first line of each file in place:

      ( sed -u 1q ; sort -k2b,2 ) < file1 > file1.sorted
      ( sed -u 1q ; sort -k2b,2 ) < file2 > file2.sorted
      join --header -o auto -e NA -a1 -a2 file1.sorted file2.sorted > file3

## Union, Intersection and Difference of files: 'sort', 'uniq' and 'join'

`sort` without `-k` and `join -t''` both consider entire lines as the key. Therefore, All examples below operate on entire lines and not on specific fields.

### For sorted files

    join -t'' -a1 -a2 file1 file2  # Union of sorted files
    join -t'' file1 file2          # Intersection of sorted files
    join -t'' -v2 file1 file2      # Difference of sorted files
    join -t'' -v1 -v2 file1 file2  # Symmetric Difference of sorted files

In each of above examples, if `-t` is set to the seperator, and joining field (i.e `-1` & `-2` or `-j`) is given, then ***union, intersection, difference and symetric difference** would be based on a joining field but the whole line (like above).

    # Union of sorted files (common rows + unjoined in file1 and file2)
    join -j1 -a1 -a2 file1 file2
    join -j1 -a1 -a2 -o auto -e NA file1 file2
    # Intersection of sorted files (only joined lines from both files)
    join -j1 file1 file2
    # Difference of sorted files (unjoined rows in file2)
    join -j1 -v2 file1 file2
    # Symmetric Difference of sorted files (unjoined rows of both files)
    join -j1 -v1 -v2 file1 file2
    join -j1 -v1 -v2 -o auto -e NA file1 file2

### For unsorted Files

    # Union of unsorted files
    sort -u file1 file2

    # Intersection of unsorted files
    #(work if only each file does not contain duplicated line)
    sort file1 file2 | uniq -d

    # Difference of unsorted files
    # only lines of file2 that are not in file1
    sort file1 file1 file2 | uniq

    # Difference of unsorted files
    # only lines of file1 that are not in file2
    sort file1 file2 file2 | uniq

    # Symmetric Difference of unsorted files
    sort file1 file2 | uniq -u

## Subshells

First a qucik refresher on **sequential commands** (those connected with `&& || ;`) and **piped commands**. Sequential commands run one after the another and their output is not streamed to the other command in the sequence. The difference between `&&` and `;` is that `&&` cares about the exit status of the previous command but `;` does not care.
**Subshells** allow us to execute sequential commands together in a seperate shell process. This is usefull primarily for *grouping* sequential commands such that their output is **a single stream**.
An example usage is: we want to sort a GTF file with a metadata header: we use a subsell to group sequential commands. The first command print the header lines to stdout and seconnd command sort aa lines except headers. Because we used subshel, therefore all commands have a single stream and the result would be printing header lines and sorted lines after header.

```shell
(zgrep "^#" file.gtf.gz; \
 zgrep -v "^#" file.gtf.gz | sort k1,1 k4,4n) | \
 gzip > file.sorted.gtf.gz
```

- Identify the number of subshells with `echo $BASH_SUBSHELL`.
- Whenever you run a shell script, it creates a new process called subshell and your script will get executed using a subshell. The shell script we ran had its own environment (including its own directory where commands were being run) and that environment went away once the script finished running.
- If you run your script with `.` command, it **"source"** the contents of that file into the current shell. And  source and the dot operator being synonyms. Files such as this are often used to incorporate setup commands such as adding things to ones environment variables. **Sourcing a script doesn't call a separate process.** It is like typing all of the commands in the parent process by hand; its environment is preserved after the script ends.
- A subshell does not inherit a variable's setting. Use the **export** command to export variables and functions to subshell:

  ```shell
  WWWJAIL=/apache.jail
  export WWWJAIL
  die() { echo "$@"; exit 2; }
  export -f die
  # now call script that will access die() and $WWWJAIL
  /etc/nixcraft/setupjail -d cyberciti.com
  ```

- However, environment variables (such as $HOME, $MAIL etc) are passed to subshell.

There are several constructs that create a subshell:

- **Subshell for grouping**: `( … )` does nothing but create a subshell and wait for it to terminate). Contrast with `{ … }` which groups commands purely for syntactic purposes and does not create a subshell.
- **Background**: `… &` creates a subshell and does not wait for it to terminate.
- **Pipeline**: `… | …` creates two subshells, one for the left-hand side and one for the right-hand side, and waits for both to terminate. The shell creates a pipe and connects the left-hand side's standard output to the write end of the pipe and the right-hand side's standard input to the read end. In some shells (ksh88, ksh93, zsh, bash with the lastpipe option set and effective), the right-hand side runs in the original , so the pipeline construct only creates one subshell.
- **Command substitution**: `$(…)` (also spelled ``…``) creates a subshell with its standard output set to a pipe, collects the output in the parent and expands to that output, minus its trailing newlines. (And the output may be further subject to splitting and globbing, but that's another story.)
- **Process substitution**: `<(…)` creates a subshell with its standard output set to a pipe and expands to the name of the pipe. The parent (or some other process) may open the pipe to communicate with the subshell. `>(…)` does the same but with the pipe on standard input.
- **Coprocess**: `coproc …` creates a subshell and does not wait for it to terminate. The subshell's standard input and output are each set to a pipe with the parent being connected to the other end of each pipe.

## Named Pipes and process substitution

Some programs won’t interface with the Unix pipes we’ve come to love and depend on. For example, certain bioinformatics tools read in multiple input files and write to multiple output files:

```Shell
processing_tool --in1 in1.fq --in2 in2.fq --out1 out2.fq --out2.fq
```

The imaginary program processing_tool requires two separate input files, and produces two separate output files. Because each file needs to be provided separately, we can’t pipe the previous processing step’s results through process ing_tool ’s standard in. there’s a more serious problem: we would have to write and read four intermediate files to disk to use this program.\
A **named pipe**, also known as a **FIFO** (First In First Out, a concept in computer science), is a special sort of file. **Regular pipes are anonymous**—they don’t have a name, and only persist while both processes are running. Named pipes behave like files, and are persistent on your filesystem. We can create a named pipe with the program mkfifo : `mkfifo fqin; ls -l fqin`. This is indeed a special type of file: the p before the file permissions is for pipe.\
With anonymous pipes, there's one reader and one writer, but that's not required with named pipes—any number of readers and writers may use the pipe.\
Although the syntax is similar to shell redirection to a file, **we’re not actually writing anything to our disk**. Named pipes provide all of the computational benefits of pipes with the flexibility of interfacing with files.\
However, creating and removing these file-like named pipes is a bit tedious. Pro grammers like syntactic shortcuts, so there’s a way to use named pipes without having to explicitly create them. This is called **process substitution**, or sometimes known as **anonymous named pipes**. These allow you to invoke a process, and have its standard output go directly to a named pipe. However, your shell treats this process substitution block like a file, so you can use it in commands as you would a regular file or named pipe: `cat <(echo "hello, process substitution")`.
![Naed pipes](/images/2019/08/named_pipes.png)

```Shell
# capturing input stream
program --in1 <(makein raw1.txt) --in2 <(makein raw2.txt) \
--out1 out1.txt --out2 out2.txt

# capturing output stream
program --in1 in1.txt --in2 in2.txt \
--out1 >(gzip > out1.txt.gz) --out2 >(gzip > out2.txt.gz)
```

# Chapter 8: R

## split-apply-combine

### split-apply-combine pattern: ```split() lapply() do.call(rbind, list)```

```R
myList <- split(df, factor)  # split to a list
myList.res <- lapply(myList, FUN) # apply FUN to the list
do.call(rbind, myList.res) # combine using do.call
```

### split-apply-combine pattern: ```aggregate() tapply() by()```

```R
aggregate(data, factors, FUN)
aggregate(df[,c(2,3)], list(df[,1]), summary)
tapply(data, factor, FUN)

models <- with(data, by(data, factor, function(x) lm(response~predictor, data = x)))
sapply(models, coef)
```

### split-apply-combine: ``dplyr``

```R
df %>% group_by(col1, col2) %>% summarize(avg = mean(col2), n_per_group = n(col3), distinct = n_distinct(col4), nth=nth(col5, 3))
```

## string prrocessing

`grep()` is a pattern compatible version of `match()`. `grep()` uses POSIX extended regular expressions by default. but set `perl = TRUE` for perl compatible RE or `fix=TRUE` for fixed character matching.

```R
## return position of match in x
re_sites <- c("CTGCAG", "CGATCG", "CAGCTG", "CCCACA")
grep("CAG", re_sites)
grep("CT[CG]", re_sites)

chrs <- c("chrom6", "chr2", "chr6", "chr4", "chr1", "chr16", " chrom8")
grep("[^\\d]6", chrs, perl=TRUE)
hrs[grep("[^\\d]6", chrs, perl=TRUE)]

```

`regexpr()` return where in each element of x it finded a pattern and set -1 for no match. regexpr() also returns the length of the matching string using attributes

```R
regexpr("[^\\d]6", chrs, perl=TRUE)

#tidy chromosome names in chrs
## first extract chromosome number
pos <- regexpr("\\d+", chrs, perl=TRUE)
## use substr to retrieve chromosome numbers
substr(chrs, pos, pos + attributes(pos)$match.length)
```

`sub(pattern, replacement, x)`: substitute strings for other strings. eplaces the first occurrence of pattern with
replacement for each element in character vector x

## Runinig R scripts from command line: `Rscript R r`

Rscript is newer and is used for batch processing Rscripts. Use `Rscript --help` in bash or `?Rscript` in Rstudio. `R` is relatively old. `littler` package in R is a new and improved version for running R scripts and is used with `r`. It can read from stdin.

```R
Rscript --default-packages=CSV-list-of-packages-tobe-loaded -e 'R expression to be evaluated' -e 'another R expression'

Rscript --default-packages=methods <file.R> >out.txt #by default method is not loaded

Rscript file.R args

Rscript --vanilla file.R #Rscript saves ENV of last script that was run. vanilla arg prevents to use the previous environment.

# run Rscript (infile) and store results in outfile (or infile.Rout if not specified).
R CMD BATCH [options] infile [outfile]

## args.R -- a simple script to show command line args
args <- commandArgs(TRUE)
print(args)

## run this with:
Rscript --vanilla args.R arg1 arg2 arg3
```

## Dependency management: `packrat`

Packrat enhances your project directory by storing your package dependencies inside it, rather than relying on your personal R library that is shared across all of your other R sessions. We call this directory your private package library (or just private library). When you start an R session in a packrat project directory, R will only look for packages in your private library; and anytime you install or remove a package, those changes will be made to your private library.

Unfortunately, private libraries don’t travel well; like all R libraries, their contents are compiled for your specific machine architecture, operating system, and R version. Packrat lets you snapshot the state of your private library, which saves to your project directory whatever information packrat needs to be able to recreate that same private library on another machine. The process of installing packages to a private library from a snapshot is called restoring.

## Workflows for Loading and Combining Multiple Files

```R
# loading multiple bed files (one for each chromosome)
list.files("hotspots", pattern="hotspots.*\\.bed")
hs_files <- list.files("hotspots", pattern="hotspots.*\\.bed", full.names=TRUE) # get full path

bedcols <- c("chr", "start", "end")
loadFile <- function(x) read.delim(x, header=FALSE, col.names=bedcols)
hs <- lapply(hs_files, loadFile)
names(hs) <- list.files("hotspots", pattern="hotspots.*\\.bed") #setting names of files to each element's name
hsd <- do.call(rbind, hs)


loadFile <- function(x) {
 # read in a BED file, extract the chromosome name from the file,
 # and add it as a column
 df <- read.delim(x, header=FALSE, col.names=bedcols)
 df$chr_name <- sub("hotspots_([^\\.]+)\\.bed", "\\1", basename(x))
 df$file <- x
 df
}
hs <- lapply(hs_files, loadFile)


# for large files which cannot be commbined (memory issues):
loadAndSummarizeFile <- function(x) {
 df <- read.table(x, header=FALSE, col.names=bedcols)
 data.frame(chr=unique(df$chr), n=nrow(df), mean_len=mean(df$end - df$start))
}
chr_hs_summaries <- lapply(hs_files, loadAndSummarizeFile)
do.call(rbind, chr_hs_summaries)
```

## exporting dataframe to a compressed file

```R
# open a gzipped file connection
hs_gzf <- gzfile("hotspot_motifs.txt.gz")
write.table(mtfs, file=hs_gzf, quote=FALSE, sep="\t", row.names=FALSE,
col.names=TRUE)
```
  
Encoding and saving objects to disk in a way that allows them to be restored as the original object is known as **serialization**.

# Chapter 9: Genomic Ranges

## Chagning coordinate systems

- **CrossMap** is a command-line tool that converts many data formats (BED, GFF/
GTF, SAM/BAM, Wiggle, VCF) between coordinate systems of different assem‐
bly versions.

- **NCBI Genome Remapping Service** is a web-based tool supporting a variety of
genomes and formats.

- **LiftOver** is also a web-based tool for converting between genomes hosted on the
UCSC Genome Browser’s site.

## BEDTOOLS

### Sorting bed files

```zsh
bedtools sort -i <bed/gcf/vcf> #sort by chr then start
sort -k1,1 -k2,2n # equivalent to above but faster
bedtools sort -h  # opening help for sortBed
bedtools sort -i <bed/gcf/vcf> -sizeA #first by chr then by feature size ascending
```

## intersecting files

```zsh
#return intersected intervals with names from file a
bedtools intersect [OPTIONS] -a <bed/gff/vcf/bam> -b <bed/gff/vcf/bam>

#Report those entries in A that overlap NO entries in B. Like “grep -v”
bedtools intersect [OPTIONS] -a <bed/gff/vcf/bam> -b <bed/gff/vcf/bam>

# Read BED A from STDIN
bedtools intersect -a genes.bed -b LINES.bed | \
  bedtools intersect -a stdin -b SINEs.bed -v

# wo (write overlap): for each A count number of overlap in B; -f what fraction of each A should overlap
bedtools intersect -a cpg.bed -b exons.bed -wo -f 0.50
```

## resizing intervals: slop

Add requested base pairs of "slop" to each feature. It is equivalent to Bioconductor `GRanges + <int>`.

```zsh
bedtools slop [OPTIONS] -i <bed/gff/vcf> -g <genome> [-b <int> or (-l and -r)]

#making genome file (three ways)
echo -e "chr1\t1000" > genome.txt
bioawk -c fastx '{print $name"\t"length($seq)}' your_genome.fastq > genome.txt
mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
 "select chrom, size from hg19.chromInfo"  > hg19.genome # extracting from UCSC mySQL

bedtools slop -i file.bed -g genome.txt -b #both end
bedtools slop -i file.bed -g genome.txt -l 2 -r 3 # left and right
bedtools slop -i file.bed -g genome.txt -b -s #taking strand into acount
```

## getting flanks (promoters)

```zsh
# getting protein coding genes from a GTF file which contains all features (e.g. exons, cds, genes, ...)
bioawk -cgff '{if ($feature == "gene") print $0}' \
  Mus_musculus.GRCm38.75_chr1.gtf.gz | \
  grep 'gene_biotype "protein_coding";' > mm_GRCm38.75_protein_coding_genes.gtf

bedtools flank -i mm_GRCm38.75_protein_coding_genes.gtf \
  -g Mus_musculus.GRCm38_genome.txt \
  -l 3000 -r 0 -s > mm_GRCm38_3kb_promoters.gtf
```

## getting sequence for intervals

```zsh
#unzipp the sequence file (here chr1 of mm10)
gunzip Mus_musculus.GRCm38.75.dna_rm.toplevel_chr1.fa.gz

# fi=input fasta (like above); -bed=intervals (here in gtf from a step above); fo: name for fasta output file
bedtools getfasta -fi Mus_musculus.GRCm38.75.dna_rm.toplevel_chr1.fa \
-bed mm_GRCm38_3kb_promoters.gtf -fo mm_GRCm38_3kb_promoters.fasta
```

## Coverage using BEDTOOLS

```zsh
#first group ranges on same chromosome together using sort
sort -k1,1 ranges.bed > ranges.sorted.bed
bedtools genomecov -i rangessorted.bed -g genome.txt
# the output columns  are: The columns are depth, how many bases covered at this depth, total number of bases per chromosome, and proportion of bases covered at this depth.

#retrive per base coverage
bedtools genomecov -i rangessorted.bed -g genome.txt -d
```

**BedGraph** is similar to the run-length encoding we encountered earlier, except runs of the same depth are stored as the ranges. We can tell genomecov
to output data in BedGraph format using:  

```zsh
bedtools genomecov -i ranges-cov.bed -g cov.txt -bg
```

Here is a figure describing genomecov:

![genomecov][logo]

[logo]: http://bedtools.readthedocs.org/en/latest/_images/genomecov-glyph.png "bedtools genomecov"

## bedtools complement

Similar to `gaps()` and `setdiff()` in bioconductor.

## bedtools closest

Similar to `nearest()` in bioconductor.

## bedtools merge

similar to `reduce()` in bioconductor. It merges overlapping ranges into a single range.

### Count the number of overlapping intervals

The -c option allows one to specify a column or columns in the input that you wish to summarize. The -o option defines the operation(s) that you wish to apply to each column listed for the -c option. For example, to count the number of overlapping intervals that led to each of the new “merged” intervals, one will “count” the first column (though the second, third, fourth, etc. would work just fine as well).  

```zsh
bedtools merge -i exons.bed -c 1 -o count
```

With the -d (distance) option, one can also merge intervals that do not overlap, yet are close to one another. For example, to merge features that are no more than 1000bp apart. Negative d value are for bp overlaps.

```zsh
bedtools merge -i exons.bed -d 1000 -c 1 -o count
```

Count column 1 and collapse column 4:

```zsh
bedtools merge -i exons.bed -d 90 -c 1,4 -o count,collapse
```

# Chapter 11: alignments

## coordinate system of various formats

**1-based coordinate system:** A coordinate system where the first base of a sequence is one. In this coordinate system, a region is specified by a closed interval. For example, the region between the 3rd and the 7th bases inclusive is [3, 7]. The **SAM**, **VCF**, **GFF** and **Wiggle** formats are using the 1-based
coordinate system.

**0-based coordinate system:** A coordinate system where the first base of a sequence is zero. In this coordinate system, a region is specified by a half-closed-half-open interval. For example, the region between the 3rd and the 7th bases inclusive is (2, 7]. The **BAM**, **BCFv2**, **BED**, and **PSL** formats are using the 0-based coordinate system.

## Termonology (SAM 1.6)

### Linear alignment

An alignment of a read to a single reference sequence that may include insertions,
deletions, skips and clipping, but may not include direction changes (i.e., one portion of the alignment
on forward strand and another portion of alignment on reverse strand). A linear alignment can be
represented in a single SAM record.

### Chimeric alignment

An alignment of a read that cannot be represented as a linear alignment. A chimeric
alignment is represented as a set of linear alignments that do not have large overlaps. Typically, one
of the linear alignments in a chimeric alignment is considered the “representative” alignment, and the
others are called “supplementary” and are distinguished by the supplementary alignment flag. All the
SAM records in a chimeric alignment have the same QNAME and the same values for 0x40 and 0x80
flags. The decision regarding which linear alignment is representative is arbitrary.  
Chimeric alignments are primarily caused by structural variations, gene fusions, misassemblies, RNA-seq or experimental protocols. They are more frequent given longer reads. For a chimeric alignment, the linear alignments constituting the alignment
are largely non-overlapping; each linear alignment may have high mapping quality and is informative in SNP/INDEL calling.
In contrast, multiple mappings are caused primarily by repeats. They are less frequent given longer reads. If a read has multiple
mappings, all these mappings are almost entirely overlapping with each other; except the single-best optimal mapping, all the
other mappings get mapping quality `<Q3` and are ignored by most SNP/INDEL callers.

### Multiple mapping

The correct placement of a read may be ambiguous, e.g., due to repeats. In this case,
there may be multiple read alignments for the same read. One of these alignments is considered
primary. All the other alignments have the secondary alignment flag set in the SAM records that
represent them. All the SAM records have the same QNAME and the same values for 0x40 and 0x80
flags. Typically the alignment designated primary is the best alignment, but the decision may be
arbitrary.

### Alternate locus

A sequence that provides an alternate representation of a locus found in a largely haploid assembly. These sequences don't represent a complete chromosome sequence although there is no hard limit on the size of the alternate locus; currently these are **less than 1 Mb**. Previously these sequences have been referred to as "**partial chromosomes**", "**alternate alleles**", and "**alternate haplotypes**". However, these terms are confusing because they contain terms that have biological implications. Diploid assemblies (which by definition are from a single individual) should not have alternate loci representations. Multiple scaffolds from different loci that are considered to be part of the same haplotype should be grouped into alternate locus groups (e.g. mouse 129/Sv group). Note: an alternate locus group was previously considered an alternate partial assembly.

### Genome patch

A contig sequence that is released outside of the full assembly release cycle. These sequences are meant to add information to the assembly **without disrupting the stable coordinate system**. There are two types of patches, **FIX and NOVEL**. FIX patches are released to correct an error in the assembly and will be removed when the new full assembly is released. NOVEL sequences are sequences that were not in the last full assembly release and will be retained with the next full assembly release. A genome patch is a scaffold sequence that is part of a minor genome release.

### Assembly (<https://www.ncbi.nlm.nih.gov/grc/help/definitions/>)

a set of ***chromosomes***, ***unlocalized and unplaced (random) sequences*** and ***alternate loci*** used to represent an organism's genome. Most current assemblies are a haploid representation of an organism's genome, although some loci may be represented more than once (see Alternate locus, above). This representation may be obtained from a single individual (e.g. chimp or mouse) or multiple individuals (e.g. human reference assembly). Except in the case of organisms which have been bred to homozygosity, the haploid assembly does not typically represent a single haplotype, but rather a mixture of haplotypes. As sequencing technology evolves, it is anticipated that diploid sequences representing an individual's genome will become available.

## Assembly units

Collections of sequences used to define discrete parts of an assembly. For example, the **Primary assembly** is considered one sequence unit. **Alternate-loci grouped together** by a common name (e.g. 129/Sv in mouse) would be considered a separate assembly-unit. In many cases, the assembly-units are what many people previously considered ‘assemblies'.

### Haploid Assembly

The collection of Chromosome assemblies, unlocalized and unlocalized sequences and alternate loci that represent an organism's genome. Any locus may be represented 0, 1 or >1 time (e.g. alternate locus), but entire chromosomes are only represented 0 or 1 times.

### primary assembly

Relevant for haploid assemblies only. The primary assemblies represents the collection of assembled chromosomes, unlocalized and unplaced sequences that, when combined, should represent a **non-redundant haploid genome**. This **excludes any of the alternate locus groups**.

### Unlocalized Sequence

A sequence found in an assembly that is associated with a specific chromosome but cannot be ordered or oriented on that chromosome.

### Unplaced Sequence

A sequence found in an assembly that is not associated with any chromosome.

### Contig

a contiguous sequence generated from determining the non-redundant path along an order set of component sequences. A contig should contain **no gaps**(Figure 1) but often the terms contig and scaffold are used interchangeably.

![Contig][fig]

[fig]: https://www.ncbi.nlm.nih.gov/core/assets/grc-web-docs/img/overlap_figure.png "Contig"

### Scaffold

an ordered and oriented set of contigs. A scaffold will contain gaps, but there is typically some evidence to support the contig order, orientation and gap size estimates.

## tips regarding SAM file

- All mapped segments in alignment lines are represented on the **forward genomic strand**. For segments
that have been mapped to the reverse strand, the recorded SEQ is reverse complemented from the original
unmapped sequence and CIGAR, QUAL, and strand-sensitive optional fields are reversed and thus recorded
consistently with the sequence bases as represented.

### bitwise FLAGS

- **Bit 0x10** indicates whether SEQ has been reverse complemented and QUAL reversed. When
bit 0x4 is unset, this corresponds to the strand to which the segment has been mapped: bit 0x10
unset indicates the forward strand, while set indicates the reverse strand. When 0x4 is set,
this indicates whether the unmapped read is stored in its original orientation as it came off the
sequencing machine.

-**Bits 0x40 and 0x80** reflect the read ordering within each template inherent in the sequencing
technology used.13 If 0x40 and 0x80 are both set, the read is part of a linear template, but it is
neither the first nor the last read. If both 0x40 and 0x80 are unset, the index of the read in the
template is unknown. This may happen for a non-linear template or when this information is lost
during data processing.
For example, in **Illumina paired-end sequencing**, first (0x40) corresponds to the **R1 ‘forward’** read and last (0x80) to the
**R2 ‘reverse’** read. (Despite the terminology, this is unrelated to the segments’ orientations when they are mapped: either,
neither, or both may have their reverse flag bits (0x10) set after mapping.)

### alignment fields

- **MAPQ**: MAPping Quality. It equals −10 log10 Pr{mapping position is wrong}, rounded to the nearest
integer. A value 255 indicates that the mapping quality is not available.

- **TLEN**: signed observed Template LENgth. If all segments are mapped to the same reference sequence,
the absolute value of TLEN equals the distance between the mapped end of the template and the
mapped start of the template, inclusively (i.e., end − start + 1).14 Note that mapped base is defined
to be one that aligns to the reference as described by CIGAR, hence excludes soft-clipped bases. The
TLEN field is positive for the leftmost segment of the template, negative for the rightmost, and the
sign for any middle segment is undefined. If segments cover the same coordinates then the choice of
which is leftmost and rightmost is arbitrary, but the two ends must still have differing signs. It is set
as 0 for a single-segment template or when the information is unavailable (e.g., when the first or last
segment of a multi-segment template is unmapped or when the two are mapped to different reference
sequences).  
The intention of this field is to indicate where the other end of the template has been aligned without
needing to read the remainder of the SAM file. Unfortunately there has been no clear consensus on
the definitions of the template mapped start and end. Thus the exact definitions are implementationdefined.
Thus a segment aligning in the forward direction at base 100 for length 50 and a segment aligning in the reverse direction
at base 200 for length 50 indicate the template covers bases 100 to 249 and has length 150.

## Converting SAM-BAM

Usually we only need to convert BAM to SAM when manually inspecting files. In
general, it’s better to store files in BAM format, as it’s more space efficient, compatible
with all samtools subcommands, and faster to process (because tools can directly
read in binary values rather than require parsing SAM strings).

```zsh
# BAM to SAM
# -h includes the header in SAM (without header it is useless)
samtools view -h celegans.bam > celegans_copy.sam

# SAM to BAM
samtools view -b file.sam >file.bam
```

## Samtools Sort and Index

Position-sorted BAM files are the starting point for most later processing steps such
as SNP calling and extracting alignments from specific regions. Additionally, sorted
BAM files are much more disk-space efficient than unsorted BAM files.  

`samtools sort` uses  a *merge sort* algorithm similar to the unix sort.

```zsh
# sort by alignment position (samtools sort will append the .bam extension for you)
samtools sort celegans_unsorted.bam celegans_sorted

#option -m to increase the memory, and -@ to specify how many threads to use
samtools sort -m 4G -@ 2 celegans_unsorted.bam celegans_sorted

# The BAM file must be sorted first, and we cannot index SAM files.
# This creates a file named celegans_sorted.bam.bai
samtools index celegans_sorted.bam
```

## Extracting and Filtering Alignments with samtools view

### Extracting alignments from a region with samtools view

With a position-sorted and indexed BAM file, we can extract specific regions of an
alignment with samtools view.

```zsh
samtools view NA12891_CEU_sample.bam 1:215906469-215906652 | head -n 3

# write these alignments in BAM format to disk
samtools view -b NA12891_CEU_sample.bam 1:215906469-215906652 >
USH2A_sample_alns.bam

# extract regions from a BED file with the -L option
samtools view -L USH2A_exons.bed NA12891_CEU_sample.bam | head -n 3
```

### Filtering alignments with samtools view

samtools view also has options for filtering alignments based on **bitwise flags**, **mapping quality**, **read group**.

- `-f`, which only outputs reads with the specified flag(s), and `-F`, which only outputs reads without the specified flag(s)

output all reads that are unmapped*

```zsh
samtools flags unmap
# 0x4   4   UNMAP
samtools view -f 4 NA12891_CEU_sample.bam | head -n 3

# to verfy the output
samtools flags 69

```

 output reads with multiple bitwise flags set

 ```zsh
 samtools flags READ1,PROPER_PAIR
# 0x42 66 PROPER_PAIR,READ1

samtools view -f 66 NA12891_CEU_sample.bam | head -n 3

# extract all aligned reads
samtools flags UNMAP  #0x4 4 UNMAP
samtools view -F 4 NA12891_CEU_sample.bam | head -n 3
```

extract all reads that did not align in a proper pair. You might be tempted to approach this by filtering out all alignments that have the proper pair bit (0x2) set using `samtools view -F 2 NA12891_CEU_sample.bam`  
This would be incorrect! Both unmapped reads and unpaired reads will also be included in this output. Neither unmapped reads, nor unpaired reads will be in a proper pair and have this bit set.    
Instead, we want to make sure the unmapped (0x4) and proper paired bits are unset (so the read is aligned and paired), and the paired end bit is set (so the read is not in a proper pair). We do this by combining bits:

```zsh
samtools flags paired   # decimal = 1
samtools flags unmap,proper_pair # decimal =6
samtools view -F 6 -f 1 NA12891_CEU_sample.bam | head -n 3
 ```

One way to verify that these results make sense is to check the counts (note that this
may be very time consuming for large files). In this case, our total number of reads
that are mapped and paired should be equal to the sum of the number of reads that
are mapped, paired, and properly paired, and the number of reads that are mapped,
paired, and not properly paired:

```zsh
samtools view -F 6 NA12891_CEU_sample.bam | wc -l # total mapped and paired
# 233628
samtools view -F 7 NA12891_CEU_sample.bam | wc -l # total mapped, paired, proper paired
# 201101
samtools view -F 6 -f 1 NA12891_CEU_sample.bam | wc -l # total mapped, paired, and not proper paired
# 32527
echo "201101 + 32527" | bc
233628
```

## Visualizing Alignments

### Visualizing Alignments with `samtools tview`

samtools tview requires **position-sorted and indexed BAM** files as input, can also load the reference genome alongside alignments so the **reference sequence** can be used in comparisons.

```zsh
# opens at the begining of chromosome
samtools tview NA12891_CEU_sample.bam human_g1k_v37.fasta

# let’s go to a specific region with the option -p
samtools tview -p 1:215906469-215906652 NA12891_CEU_sample.bam human_g1k_v37.fasta
```

### visualyzing alignments using `IGV`

## Pileups with samtools pileup, Variant Calling, and Base Alignment Quality

The pileup format, a plain-text format that summarizes
reads’ bases at each chromosome position by stacking or “piling up” aligned reads.
The per-base summary of the alignment data created in a pileup can then be used to
identify variants (regions different from the reference), and determine sample indi‐
viduals’ genotypes. samtools’s mpileup subcommand creates pileups from BAM files,
and this tool is the first step in samtools-based variant calling pipelines.

```zsh
# disable Base Alignment Quality (BAQ) with --no-BAQ or -B
# samtools mpi leup called with the -v (VCF) or -g (BCF) arguments will generate genotype likelihoods
# for every site in the genome (or all sites within a region if one is specified). -u. for uncompressed results
 samtools mpileup -v --no-BAQ --region 1:215906528-215906567 \
  --fasta-ref human_g1k_v37.fasta NA12891_CEU_sample.bam \
  > NA12891_CEU_sample.vcf.gz

# BCFcall uses the estimated genotype likelihoods and other information to call certain sites as
# variant or not, and infer the genotypes of all individuals
#  -m uses the multiallelic caller (the other option is to use the original consensus caller with -c)
# -v for showing variant  sites only
 bcftools call -v -m NA12891_CEU_sample.vcf.gz > NA12891_CEU_sample_calls.vcf.gz
```

# Chapter-13: Out of memory approches

## Tabix

Three steps to use it:

1. sort by chromosome and start (handle header columns)

2. compress with bgzip

3. index with tabix (.tbi)

A nice feature of Tabix is that it works across an HTTP or FTP server. Once you’ve
sorted, bgzipped, and indexed a file with tabix, you can host the file on a shared
server so others in your group can work with it remotely.

```zsh
# we use a subshell to handle headers
(zgrep "^#" Mus_musculus.GRCm38.75.gtf.gz; \
 zgrep -v "^#" Mus_musculus.GRCm38.75.gtf.gz | sort -k1,1 -k4,4n) | \
 bgzip > Mus_musculus.GRCm38.75.gtf.bgz

tabix -p gff Mus_musculus.GRCm38.75.gtf.bgz
tabix Mus_musculus.GRCm38.75.gtf.bgz 16:23146536-23158028 | head -n3
tabix Mus_musculus.GRCm38.75.gtf.bgz 16:23146536-23158028 | \
  awk '$3 ~ /exon/ {print}'
```


## SQLite

A table can have only one primary key, which may consist of single or multiple fields. When multiple fields are used as a primary key, they are called a **composite key**.  

**SQL functions:**
| coalesce(a, b,c, ...)   | Return first non-NULL value in a, b, c, ... or NULL if all values are NULL                                       |
| :---------------------- | :--------------------------------------------------------------------------------------------------------------- |
| length(x)               | Returns number of characters in x                                                                                |
| lower(x)                | Return x in lowercase                                                                                            |
| upper(x)                | Return x in uppercase                                                                                            |
| replace(x, str,repl)    | Return x with all occurrences of str replaced with repl                                                          |
| round(x, digits)        | Round x to digits (default 0)                                                                                    |
| trim(x, chars),         | Trim off chars (spaces if chars is not specified) from both sides, left side, and right side of x, respectively. |
| ltrim(x, chars),        | Trim off chars (spaces if chars is not specified) from both sides, left side, and right side of x, respectively. |
| rtrim(x, chars)         | Trim off chars (spaces if chars is not specified) from both sides, left side, and right side of x, respectively. |
| substr(x, start,length) | Extract a substring from x starting from character start and is length characters long                           |


```sql
/* like for pattern matching (uses % like * in bash)*/

select author,position from gwascat where author like 'Ha%' limit 5;

/* `is` and `is not` same as `=` & `!=`, except using null: */
select author,position from gwascat where position is null limit 5;

/* pvalue has some NULL values:*/
SELECT chrom, position, trait, strongest_risk_snp, pvalue FROM gwascat ORDER BY pvalue LIMIT 5;
/* chrom         position    trait      strongest_risk_snp  pvalue
   ---------- ---------- ------------- ------------------ ----------
                          Brain imaging rs10932886
                          Brain imaging rs429358
                          Brain imaging rs7610017
                          Brain imaging rs6463843                    */
SELECT chrom, position, trait, strongest_risk_snp, pvalue FROM gwascat 
      where pvalue is not null ORDER BY pvalue LIMIT 5;

SELECT chrom, position, strongest_risk_snp, pvalue FROM gwascat
      WHERE chrom IN ("1", "2", "3") AND pvalue < 10e-11
      ORDER BY pvalue LIMIT 5;

/* || is concatanation operator (same as concat function in mySQL) */
SELECT lower(trait) AS trait,
      "chr" || chrom || ":" || position AS region FROM gwascat LIMIT 2;
/* trait                  region
-------------------- -------------
asthma and hay fever chr6:32658824
asthma and hay fever chr4:38798089  */

/* ifnull function: replace null value with NA */
SELECT ifnull(chrom, "NA") AS chrom, ifnull(position, "NA") AS position,
      strongest_risk_snp, ifnull(pvalue, "NA") AS pvalue FROM gwascat
      WHERE strongest_risk_snp = "rs429358";

/* Using count(*) will always count the rows, regardless of whether there are NULLs. 
calling count(colname) where colname is a particular column will return
the number of non-NULL values */
SELECT count(*) FROM gwascat;
SELECT count(pvalue) FROM gwascat;
/* get the number of null pvalues*/
select count(*) from gwascat where pvalue is null;

/* date is stored using  XKCD’s ISO 8601 format YYYY:MM:DD (2013-12-30), Because
this format cleverly arranges the date from largest period (year) to smallest (day),
sorting and comparing this date as text is equivalent to comparing the actual dates.
*/
select "2007" AS year, count(*) AS number_entries
     from gwascat WHERE date BETWEEN "2007-01-01" AND "2008-01-01";

/*  number of unique non-NULL RS IDs in the strongest_risk_snp column*/
SELECT count(DISTINCT strongest_risk_snp) AS unique_rs FROM gwascat;

/* group by */
SELECT chrom, count(*) FROM gwascat GROUP BY chrom;
SELECT chrom, count(*) as nhits FROM gwascat GROUP BY chrom
      ORDER BY nhits DESC;
select strongest_risk_snp, count(*) AS count
      FROM gwascat GROUP BY strongest_risk_snp
      ORDER BY count DESC LIMIT 5;
select strongest_risk_snp, strongest_risk_allele, count(*) AS count
      FROM gwascat GROUP BY strongest_risk_snp, strongest_risk_allele
      ORDER BY count DESC LIMIT 10;
/* average log10 p-value for all association studies grouped by year.
It’s important to note that filtering with WHERE applies to rows before grouping. If you
want to filter groups themselves on some condition, you need to use the HAVING
clause.*/
SELECT substr(date, 1, 4) AS year,
      round(avg(pvalue_mlog), 4) AS mean_log_pvalue,
      count(pvalue_mlog) AS n
      FROM gwascat GROUP BY year;

/*  report per-group averages when we have
more than a certain number of cases (because our -log10 p-value averages may not be
reliable with too few cases) */
SELECT substr(date, 1, 4) AS year,
      round(avg(pvalue_mlog), 4) AS mean_log_pvalue,
      count(pvalue_mlog) AS n
      FROM gwascat GROUP BY year
      HAVING count(pvalue_mlog) > 10;

/* average number of associations per study each year */
/* THIS IS THE INNER QUERY: first aggregation (over pubmedid) */

/* THIS IS THE OUTER QUERY: second aggregation (over years) */
SELECT year, avg(num_assoc)
      FROM (SELECT substr(date, 1, 4) AS year,
            author, count(*) AS num_assoc
            FROM gwascat GROUP BY pubmedid)
      GROUP BY year;

/* join tables */
/* forein key: a column in a table that is a primary key in another
 table and connects two table to eachother
join predicate: (on table1.id = table2.id)
*/
/* inner join */
SELECT * FROM assocs INNER JOIN studies ON assocs.study_id = studies.id;
SELECT studies.id, assocs.id, trait, year FROM assocs
      INNER JOIN studies ON assocs.study_id = studies.id;
/* count numbre of inner joined rows */
SELECT count(*) FROM assocs INNER JOIN studies ON assocs.study_id = studies.id;
/* There’s one record in assocs with a study_id not in the
studies.id column (in this case, because it’s NULL).*/
SELECT * FROM assocs WHERE study_id NOT IN (SELECT id FROM studies);
/*record in the studies table that is not linked to any associa‐
tion results in the assocs table*/
SELECT * FROM studies WHERE id NOT IN (SELECT study_id FROM assocs);

/* SQLite only supports a type of outer join known as a left outer join
SQLite doesn’t support full outer joins, but it’s possible to emulate
full outer joins in SQLite */
SELECT * FROM assocs LEFT OUTER JOIN studies
      ON assocs.study_id = studies.id;
/* creating and droping index for a  column */
CREATE INDEX snp_idx ON assocs(strongest_risk_snp);
DROP INDEX snp_idx;
/* Note that SQLite automatically indexes the primary key for each table—you won’t
have to index this yourself. In addition, SQLite will not index foreign keys for you,
and you should generally index foreign keys to improve the performance joins.*/

/* database dump */
sqlite3 variants.db ".dump" > dump.sql
sqlite3 variants-duplicate.db < dump.sql

/* importing tables from files
it will pick up headers from the first row */
sqlite3 csv.db
    .mode csv
    .import csv_file.csv mytable

/* importing tables from files to an existing table
the sqlite3 tool uses all the rows, including the first row,
in the CSV file as the actual data to import. */
    DROP TABLE IF EXISTS cities;
    CREATE TABLE cities(
      name TEXT NOT NULL,
      population INTEGER NOT NULL
    );
    .mode csv
    .import csv_file_without_header.csv table
```

Here is a visual illustration of SQL joins (note only inner join and left join is supported in SQLite):  

![](images/SQL_JOINS.jpg)

<http://www.codeproject.com/KB/database/Visual_SQL_Joins/Visual_SQL_JOINS_orig.jpg>

Following are commonly used **table constraints** available in SQLite:

- NOT NULL Constraint − Ensures that a column cannot have NULL value.
- 
- DEFAULT Constraint − Provides a default value for a column when none is specified.
- 
- UNIQUE Constraint − Ensures that all values in a column are different.
- 
- PRIMARY Key − Uniquely identifies each row/record in a database table.
- 
- CHECK Constraint − Ensures that all values in a column satisfies certain conditions.

```sql
CREATE TABLE COMPANY3(
   ID INT PRIMARY KEY     NOT NULL,
   NAME           TEXT    NOT NULL,
   AGE            INT     NOT NULL,
   ADDRESS        CHAR(50),
   SALARY         REAL    CHECK(SALARY > 0)
);
```

## RSQLite

### sqliteCopyDatabase: Copy a SQLite database

Copies a database connection to a file or to another database connection. It can be used to save an in-memory database (created using `dbname = ":memory:"` or `dbname = "file::memory:"`) to a file or to create an in-memory database a copy of another database.

```r
library(DBI)
# Copy the built in databaseDb() to an in-memory database
con <- dbConnect(RSQLite::SQLite(), ":memory:")
dbListTables(con)

db <- RSQLite::datasetsDb()
RSQLite::sqliteCopyDatabase(db, con)
dbDisconnect(db)
dbListTables(con)

dbDisconnect(con)
```

### Regular expression `initRegExp`

This loads a regular-expression matcher for posix extended regular expressions, as available through the SQLite source code repository (https://sqlite.org/src/raw?filename=ext/misc/regexp.c).
SQLite will then implement the "A regexp B" operator, where A is the string to be matched and B is the regular expression. Note this only affects the specified connection.

```r
library(DBI)
db <- RSQLite::datasetsDb()
RSQLite::initRegExp(db)

dbGetQuery(db, "SELECT * FROM mtcars WHERE carb REGEXP '[12]'")
dbDisconnect(db)
```

### Begin/commit/rollback SQL transactions

A transaction encapsulates several SQL statements in an atomic unit. It is initiated with dbBegin() and either made persistent with dbCommit() or undone with dbRollback(). In any case, the DBMS guarantees that either all or none of the statements have a permanent effect. This helps ensuring consistency of write operations to multiple tables.

```r
library(DBI)

con <- dbConnect(RSQLite::SQLite(), ":memory:")

dbWriteTable(con, "cash", data.frame(amount = 100))
dbWriteTable(con, "account", data.frame(amount = 2000))

# All operations are carried out as logical unit:
dbBegin(con)
withdrawal <- 300
dbExecute(con, "UPDATE cash SET amount = amount + ?", list(withdrawal))
dbExecute(con, "UPDATE account SET amount = amount - ?", list(withdrawal))
dbCommit(con)

dbReadTable(con, "cash")
dbReadTable(con, "account")

# Rolling back after detecting negative value on account:
dbBegin(con)
withdrawal <- 5000
dbExecute(con, "UPDATE cash SET amount = amount + ?", list(withdrawal))
dbExecute(con, "UPDATE account SET amount = amount - ?", list(withdrawal))
if (dbReadTable(con, "account")$amount >= 0) {
  dbCommit(con)
} else {
  dbRollback(con)
}

dbReadTable(con, "cash")
dbReadTable(con, "account")

dbDisconnect(con)
```

### Self-contained SQL transactions `dbWithTransaction`

Given that transactions are implemented, this function allows you to pass in code that is run in a transaction. The default method of `dbWithTransaction()` calls `dbBegin()` before executing the code, and dbCommit() after successful completion, or dbRollback() in case of an error. The advantage is that you don't have to remember to do `dbBegin()` and `dbCommit()` or `dbRollback()` – that is all taken care of. The special function `dbBreak()` allows an early exit with rollback, it can be called only inside `dbWithTransaction()`.

```r
con <- dbConnect(RSQLite::SQLite(), ":memory:")

dbWriteTable(con, "cash", data.frame(amount = 100))
dbWriteTable(con, "account", data.frame(amount = 2000))

# All operations are carried out as logical unit:
dbWithTransaction(
  con,
  {
    withdrawal <- 300
    dbExecute(con, "UPDATE cash SET amount = amount + ?", list(withdrawal))
    dbExecute(con, "UPDATE account SET amount = amount - ?", list(withdrawal))
  }
)

# The code is executed as if in the curent environment:
withdrawal

# The changes are committed to the database after successful execution:
dbReadTable(con, "cash")
dbReadTable(con, "account")

# Rolling back with dbBreak():
dbWithTransaction(
  con,
  {
    withdrawal <- 5000
    dbExecute(con, "UPDATE cash SET amount = amount + ?", list(withdrawal))
    dbExecute(con, "UPDATE account SET amount = amount - ?", list(withdrawal))
    if (dbReadTable(con, "account")$amount < 0) {
      dbBreak()
    }
  }
)

# These changes were not committed to the database:
dbReadTable(con, "cash")
dbReadTable(con, "account")

dbDisconnect(con)
```

### Statements

DBI has new functions dbSendStatement() and dbExecute(), which are the counterparts of dbSendQuery() and dbGetQuery() for SQL statements that do not return a tabular result, such as inserting records into a table, updating a table, or setting engine parameters. It is good practice, although currently not enforced, to use the new functions when you don’t expect a result.

```r
dbExecute(mydb, 'DELETE FROM iris WHERE "Sepal.Length" < 4')
#> [1] 0
rs <- dbSendStatement(mydb, 'DELETE FROM iris WHERE "Sepal.Length" < :x')
dbBind(rs, params = list(x = 4.5))
dbGetRowsAffected(rs)
#> [1] 4
dbClearResult(rs)
```

### Batched queries

If you run a query and the results don’t fit in memory, you can use dbSendQuery(), dbFetch() and dbClearResults() to retrieve the results in batches. By default dbFetch() will retrieve all available rows: use n to set the maximum number of rows to return.

```r
rs <- dbSendQuery(mydb, 'SELECT * FROM mtcars')
while (!dbHasCompleted(rs)) {
  df <- dbFetch(rs, n = 10)
  print(nrow(df))
}
#> [1] 10
#> [1] 10
#> [1] 10
#> [1] 2
dbClearResult(rs)

# with multiple  parameters
rs <- dbSendQuery(mydb, 'SELECT * FROM iris WHERE "Sepal.Length" < :x')
dbBind(rs, params = list(x = 4.5))
nrow(dbFetch(rs))
#> [1] 4
dbBind(rs, params = list(x = 4))
nrow(dbFetch(rs))
#> [1] 0
dbClearResult(rs)
```

## RSQLite & bioconductor

```r
library(org.Hs.eg.db)
# make a sql connection from db
hs_con <- org.Hs.eg_dbconn()
# get schema for all tables
org.Hs.eg_dbschema()
# get info about a table
dbGetQuery(hs_con, "PRAGMA table_info('gene_info');")
```

## RSQLite & dplyr

```r
library(dplyr, warn.conflicts = FALSE)

con <- DBI::dbConnect(RSQLite::SQLite(), dbname = ":memory:")
# copy an obj to db
copy_to(con, mtcars)

# retrieve a table from db
mtcars2 <- tbl(con, "mtcars")

# lazily generates query
summary <- mtcars2 %>%
  group_by(cyl) %>%
  summarise(mpg = mean(mpg, na.rm = TRUE)) %>%
  arrange(desc(mpg))

# see query
summary %>% show_query()
#> <SQL>
#> SELECT `cyl`, AVG(`mpg`) AS `mpg`
#> FROM `mtcars`
#> GROUP BY `cyl`
#> ORDER BY `mpg` DESC

# execute query and retrieve results
summary %>% collect()
#> # A tibble: 3 x 2
#>     cyl   mpg
#>   <dbl> <dbl>
#> 1     4  26.7
#> 2     6  19.7
#> 3     8  15.1
```

Another example

```r
con <- DBI::dbConnect(RSQLite::SQLite(), dbname = ":memory:")

copy_to(con, nycflights13::flights, "flights",
  temporary = FALSE,
  indexes = list(
    c("year", "month", "day"),
    "carrier",
    "tailnum",
    "dest"
  )
)

# Now that we’ve copied the data, we can use tbl() to take a reference to it:
flights_db <- tbl(con, "flights")

# dplyr focusses on SELECT statements (i.e. it can only make select statements)

flights_db %>% select(year:day, dep_delay, arr_delay)
#> # Source:   lazy query [?? x 5]
#> # Database: sqlite 3.30.1 [:memory:]
#>    year month   day dep_delay arr_delay
#>   <int> <int> <int>     <dbl>     <dbl>
#> 1  2013     1     1         2        11
#> 2  2013     1     1         4        20
#> 3  2013     1     1         2        33
#> 4  2013     1     1        -1       -18
#> 5  2013     1     1        -6       -25
#> 6  2013     1     1        -4        12
#> # … with more rows

flights_db %>%
  group_by(dest) %>%
  summarise(delay = mean(dep_time))
#> Warning: Missing values are always removed in SQL.
#> Use `mean(x, na.rm = TRUE)` to silence this warning
#> This warning is displayed only once per session.
#> # Source:   lazy query [?? x 2]
#> # Database: sqlite 3.30.1 [:memory:]
#>   dest  delay
#>   <chr> <dbl>
#> 1 ABQ   2006.
#> 2 ACK   1033.
#> 3 ALB   1627.
#> 4 ANC   1635.
#> 5 ATL   1293.
#> 6 AUS   1521.
#> # … with more ro
```

The most important difference between ordinary data frames and remote database queries is that your R code is translated into SQL and executed in the database on the remote server, not in R on your local machine. When working with databases, dplyr tries to be as lazy as possible:

It never pulls data into R unless you explicitly ask for it. It delays doing any work until the last possible moment: it collects together everything you want to do and then sends it to the database in one step.  
  
Surprisingly, this sequence of operations never touches the database. It’s not until you ask for the data (e.g. by printing tailnum_delay) that dplyr generates the SQL and requests the results from the database. Even then it tries to do as little work as possible and only pulls down a few rows.

```r
tailnum_delay_db
#> # Source:     lazy query [?? x 3]
#> # Database:   sqlite 3.30.1 [:memory:]
#> # Ordered by: desc(delay)
#>   tailnum delay     n
#>   <chr>   <dbl> <int>
#> 1 N11119   30.3   148
#> 2 N16919   29.9   251
#> 3 N14998   27.9   230
#> 4 N15910   27.6   280
#> 5 N13123   26.0   121
#> 6 N11192   25.9   154
#> # … with more rows

tailnum_delay_db %>% show_query()
#> <SQL>
#> SELECT *
#> FROM (SELECT *
#> FROM (SELECT `tailnum`, AVG(`arr_delay`) AS `delay`, COUNT() AS `n`
#> FROM `flights`
#> GROUP BY `tailnum`)
#> ORDER BY `delay` DESC)
#> WHERE (`n` > 100.0)
```

you’ll iterate a few times before you figure out what data you need from the database. Once you’ve figured it out, use collect() to pull all the data down into a local tibble.

```r
tailnum_delay <- tailnum_delay_db %>% collect()
tailnum_delay
#> # A tibble: 1,201 x 3
#>   tailnum delay     n
#>   <chr>   <dbl> <int>
#> 1 N11119   30.3   148
#> 2 N16919   29.9   251
#> 3 N14998   27.9   230
#> 4 N15910   27.6   280
#> 5 N13123   26.0   121
#> 6 N11192   25.9   154
#> # … with 1,195 more rows
```

You can also ask the database how it plans to execute the query with `explain()`. The output is database dependent, and can be esoteric, but learning a bit about it can be very useful because it helps you understand if the database can execute the query efficiently, or if you need to create new indices.

### more about SQLite indices

#### ANALYZE

The ANALYZE command gathers statistics about tables and indices and stores the collected information in internal tables of the database where the query optimizer can access the information and use it to help make better query planning choices. Applications with long-lived databases that use complex queries should consider running the following commands just prior to closing each database connection. Tables with names of the form `sqlite_statN` where N is an integer. Such tables store database statistics gathered by the ANALYZE command and used by the query planner to help determine the best algorithm to use for each query.

```sql
PRAGMA analysis_limit=400;
PRAGMA optimize;
```

#### Multi column indices  `AND`

Multi-column indices only work if the constraint terms in the WHERE clause of the query are connected by AND. If you two seperate index for a table SQLite either use one arbitarar or use the most appropriate one if `ANALYZE` command was perforrmed. For better performance on **AND-connected where clauses** create multicolumn indices from column involved in the query (instead of creating seperte index for each column).

```sql
 create index idx1 on table_name(column1);
 create index idx2 on table_name(column2);
 /* if useing idx3 remove previous indices (idx1 & idx2); Hence, a good rule of thumb is that your database schema should never contain two indices
 where one index is a prefix of the other. Drop the index with fewer columns.
 SQLite will still be able to do efficient lookups with the longer index.*/
 create index idx3 on table_name(column1,column2);
```

#### Covering Indices

When indices include neccesary query column and also the equested output column. Hence, by adding extra "output" columns onto the end of an index, one can avoid having to reference the original table and thereby cut the number of binary searches for a query in half.

#### OR-by-UNION technique

When the `where clause` includes `OR`, SQLite uses the indices to compute all of the rowids first and then combines them with a *union* operation before starting to do rowid lookups on the original table. For this to work, **all columns** in the OR statement should has seperate indices, otherwise a full table scan is performed.

#### sorting by index or covering index

Using `sort by column`, when the column is indexed: depending on the content of *where clause* and *size of the table*, query planner decides whether to perform sort as an extra step or use an existing index column. Usually using index column needs less temporary storage and is preffered (but may not neccesarily leads a faster result).  
Hoever, soing by covering index can be much efficient. In this case the query columns and output columns are  all multi-column indexed.

#### Searching And Sorting

```sql
SELECT price FROM table_name WHERE column_1='Orange' ORDER BY column_2;
```

If the table has a **multi-column index** (including column_1 and column-2), the query is efficient and the sort is not neccessary.

#### rowid vs INTEGER PRIMARY KEY

`rowid` is a 64-bit signed key that uniquely identifies the row within its table and is stored as a **B-Tree structure**. This means that retrieving or sorting records by rowid is fast. Searching for a record with a specific rowid, or for all records with rowids within a specified range is around **twice as fast as** a similar search made by specifying any other PRIMARY KEY or indexed value.  
if a rowid table has a primary key that consists of a single column and the declared type of that column is "INTEGER" in any mixture of upper and lower case, then the column becomes an alias for the rowid. Such a column is usually referred to as an "integer primary key" (and is really efficient). 

```sql
/* three table declarations all cause the column "x" to be an alias for the rowid (an integer primary key): */
CREATE TABLE t(x INTEGER PRIMARY KEY ASC, y, z);
CREATE TABLE t(x INTEGER, y, z, PRIMARY KEY(x ASC));
CREATE TABLE t(x INTEGER, y, z, PRIMARY KEY(x DESC));
/* in this case x is not an alias to rowid */
CREATE TABLE t(x INTEGER PRIMARY KEY DESC, y, z);
```

You can refer to the rowid by `rowid` alias or the cilumn name of the INTEGER PRIMARY KEY. An INSERT statement *may* provide a value (i.e. its optional) to use as the rowid for each row inserted.

### Foreign Key Constraints

SQL foreign key constraints are used to enforce "exists" relationships between tables. For example, consider a database schema created using the following SQL commands. The applications using this database are entitled to assume that for each row in the track table there exists a corresponding row in the artist table.

```sql
PRAGMA foreign_keys = ON;

CREATE TABLE artist(
  artistid    INTEGER PRIMARY KEY, 
  artistname  TEXT
);
CREATE TABLE track(
  trackid     INTEGER,
  trackname   TEXT, 
  trackartist INTEGER     -- Must map to an artist.artistid!
);

/* solution */

CREATE TABLE track(
  trackid     INTEGER, 
  trackname   TEXT, 
  trackartist INTEGER,
  FOREIGN KEY(trackartist) REFERENCES artist(artistid)
);
```

The following SQLite command-line session illustrates the effect of the foreign key constraint added to the track table:

```
sqlite> SELECT * FROM artist;
artistid  artistname       
--------  -----------------
1         Dean Martin      
2         Frank Sinatra    

sqlite> SELECT * FROM track;
trackid  trackname          trackartist
-------  -----------------  -----------
11       That's Amore       1  
12       Christmas Blues    1  
13       My Way             2  

sqlite> -- This fails because the value inserted into the trackartist column (3)
sqlite> -- does not correspond to row in the artist table.
sqlite> INSERT INTO track VALUES(14, 'Mr. Bojangles', 3);
SQL error: foreign key constraint failed

sqlite> -- This succeeds because a NULL is inserted into trackartist. A
sqlite> -- corresponding row in the artist table is not required in this case.
sqlite> INSERT INTO track VALUES(14, 'Mr. Bojangles', NULL);

sqlite> -- Trying to modify the trackartist field of the record after it has 
sqlite> -- been inserted does not work either, since the new value of trackartist (3)
sqlite> -- Still does not correspond to any row in the artist table.
sqlite> UPDATE track SET trackartist = 3 WHERE trackname = 'Mr. Bojangles';
SQL error: foreign key constraint failed

sqlite> -- Insert the required row into the artist table. It is then possible to
sqlite> -- update the inserted row to set trackartist to 3 (since a corresponding
sqlite> -- row in the artist table now exists).
sqlite> INSERT INTO artist VALUES(3, 'Sammy Davis Jr.');
sqlite> UPDATE track SET trackartist = 3 WHERE trackname = 'Mr. Bojangles';

sqlite> -- Now that "Sammy Davis Jr." (artistid = 3) has been added to the database,
sqlite> -- it is possible to INSERT new tracks using this artist without violating
sqlite> -- the foreign key constraint:
sqlite> INSERT INTO track VALUES(15, 'Boogie Woogie', 3);
```

As you would expect, it is not possible to manipulate the database to a state that violates the foreign key constraint by deleting or updating rows in the artist table either:

```
sqlite> -- Attempting to delete the artist record for "Frank Sinatra" fails, since
sqlite> -- the track table contains a row that refer to it.
sqlite> DELETE FROM artist WHERE artistname = 'Frank Sinatra';
SQL error: foreign key constraint failed

sqlite> -- Delete all the records from the track table that refer to the artist
sqlite> -- "Frank Sinatra". Only then is it possible to delete the artist.
sqlite> DELETE FROM track WHERE trackname = 'My Way';
sqlite> DELETE FROM artist WHERE artistname = 'Frank Sinatra';

sqlite> -- Try to update the artistid of a row in the artist table while there
sqlite> -- exists records in the track table that refer to it. 
sqlite> UPDATE artist SET artistid=4 WHERE artistname = 'Dean Martin';
SQL error: foreign key constraint failed

sqlite> -- Once all the records that refer to a row in the artist table have
sqlite> -- been deleted, it is possible to modify the artistid of the row.
sqlite> DELETE FROM track WHERE trackname IN('That''s Amore', 'Christmas Blues');
sqlite> UPDATE artist SET artistid=4 WHERE artistname = 'Dean Martin';
```

**SQLite uses the following terminology:**

- The **parent table** is the table that a foreign key constraint refers to. The parent table in the example in this section is the artist table. Some books and articles refer to this as the **referenced table**, which is arguably more correct, but tends to lead to confusion.
- 
- The **child table** is the table that a foreign key constraint is applied to and the table that contains the REFERENCES clause. The example in this section uses the track table as the child table. Other books and articles refer to this as the **referencing table**.
- 
- The **parent key** is the column or set of columns in the parent table that the foreign key constraint refers to. This is normally, but not always, the primary key of the parent table. The parent key must be a named column or columns in the parent table, not the rowid.
- 
- The **child key** is the column or set of columns in the child table that are constrained by the foreign key constraint and which hold the REFERENCES clause.